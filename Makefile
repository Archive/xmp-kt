# Extended Module Player toplevel Makefile
# $Id: Makefile,v 1.1.1.2 1998/11/15 00:11:43 petery Exp $

# DIST		distribution package name
# DFILES	standard distribution files 
# DDIRS		standard distribution directories

XCFLAGS	= -Iloaders/include #-Ifrontends/include
TEST_XM	=
DIST	= xmp-kt-$(VERSION)
RPMSPEC	= etc/xmp.spec
RPMRC	= etc/rpmrc
MODULES	= cobalt.mod
DFILES	= README INSTALL configure configure.in Makefile Makefile.rules.in \
	  xmp-kt.spec $(MODULES)
DDIRS	= lib doc etc src patches scripts
CFILES	=
DCFILES	= Makefile.rules.old config.log config.status config.cache

all: xmp

xmp:
	cd src; $(MAKE)

include Makefile.rules

distclean::
	rm -f Makefile.rules

configure: configure.in
	autoconf

check: test

dust:
	@echo "<cough, cough>"

test: xmp
	time -v player/xmp -vvv $(TEST_XM)

uninstall:
	rm -f $(BIN_DIR)/xmp
	rm -f $(BIN_DIR)/xxmp
	rm -f $(MAN_DIR)/xmp.1
	rm -f $(MAN_DIR)/xxmp.1
	rm -f /etc/xmprc


# Build the complete distribution directory with source, binary and RPM
# packages. gzipped files are expanded and recompressed using bzip2.

release: chkoldver dist diff bindist
	rm -Rf $(VERSION)
	mkdir $(VERSION)
	cp $(DIST).tar.gz $(VERSION)
	mv *-$(VERSION)_$(PLATFORM).gz $(VERSION)
	[ "$(OLDVER)" != "none" ] && \
	    mv xmp-$(OLDVER)-$(VERSION).diff.gz $(VERSION); \
	if [ -f ../$(DIST)*src.rpm ]; then \
	    rm -f ../$(DIST)*src.rpm; \
	    mv ../$(DIST)*rpm $(VERSION); \
	elif [ -f $(DIST)*rpm ]; then \
	    rm -f $(DIST)*src.rpm; \
	    mv $(DIST)*rpm $(VERSION); \
	fi
	cd $(VERSION); for i in *.tar.gz; do \
	    gzip -dc $$i | bzip2 > `basename $$i .gz`.bz2; \
	done; \
	ls [0-9a-z]*|../scripts/makeindex $(VERSION) > index.html 
	$(MAKE) -C doc html
	sync
	du -bs $(VERSION)


# Extra targets:
# 'dist' prepares a distribution package
# 'mark' marks the last RCS revision with the package version number
# 'whatsout' lists the locked files
# 'diff' creates a diff file
# 'rpm' generates a RPM package

dist: doc Makefile
	rm -f etc/xmp.spec
	rm -Rf $(DIST) $(DIST).tar.gz
	mkdir $(DIST)
	cp -dp $(DFILES) $(DIST)
	$(MAKE) DISTDIR=$(DIST) subdist
	cat Makefile.rules.in | \
	sed "s/$(DATE)/`date`/" > $(DIST)/Makefile.rules.in
	mv -f Makefile.rules.in Makefile.rules.old
	cp $(DIST)/Makefile.rules.in .
	chmod -R u+w $(DIST)/*
	tar cvf - $(DIST) | gzip -9c > $(DIST).tar.gz
	rm -Rf $(DIST)
	./config.status
	touch -r Makefile.rules.old Makefile.rules.in Makefile.rules
	sync

bindist:
	$(MAKE) _bindist1 CONFIGURE="./configure"

binpkg: bindist $(PORTS)

_bindist1:
	rm -Rf $(DIST)
	gzip -dc $(DIST).tar.gz | tar xvf -
	cd $(DIST); $(CONFIGURE); $(MAKE) _bindist2
	rm -Rf $(DIST)
	sync

_bindist2: xmp
	strip src/main/xmp src/main/xxmp
	cat src/main/xmp | gzip -9c > ../xmp-$(VERSION)_$(PLATFORM).gz
	cat src/main/xxmp | gzip -9c > ../xxmp-$(VERSION)_$(PLATFORM).gz

mark:
	ID=`echo $(VERSION) | sed 's/\./_/g'`; \
	rcs -nxmp_$$ID: RCS/*

chkoldver:
	@if [ "$(OLDVER)" = "" ]; then \
	    echo "parameter missing: OLDVER=<old_version>"; \
	    false; \
	fi

diff: chkoldver
	if [ "$(OLDVER)" != "none" ]; then \
	    @echo "Creating diff from $(OLDVER) to $(VERSION)"; \
	    rm -Rf xmp-$(OLDVER) xmp-$(VERSION); \
	    tar xzf xmp-$(OLDVER).tar.gz; \
	    tar xzf xmp-$(VERSION).tar.gz; \
	    diff -rud --new-file xmp-$(OLDVER) xmp-$(VERSION) | gzip -9c > \
		xmp-$(OLDVER)-$(VERSION).diff.gz; \
	    rm -Rf xmp-$(OLDVER) xmp-$(VERSION); \
	    sync; \
	fi

rpm:
	$(MAKE) -C etc xmp.spec
	sudo rpm --rcfile $(RPMRC) -ba $(RPMSPEC)
	sync

Makefile.rules: Makefile.rules.in
	./config.status

$(OBJS): Makefile

