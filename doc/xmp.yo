COMMENT(-- Start of kludge --)

DEFINESYMBOL(XXmanpagesynopsis)
DEFINESYMBOL(XXmanpagedescription)
DEFINESYMBOL(XXmanpagename)
DEFINESYMBOL(XXmanpageoptions)
DEFINESYMBOL(XXmanpagefiles)
DEFINESYMBOL(XXmanpageseealso)
DEFINESYMBOL(XXmanpagediagnostics)
DEFINESYMBOL(XXmanpagebugs)
DEFINESYMBOL(XXmanpageauthor)

COMMENT(-- End of kludge --)

mailto(claudio@pos.inf.ufpr.br)
manpage(XMP)
	(1)
	(Version 1.1.6)
	(Oct 1998)
	(Extended Module Player)

htmlcommand(<p><br>)
manpagesection(NAME)
htmlcommand(<ul>)
	xmp, xxmp - Extended Module Player

label(synopsis)
htmlcommand(</ul><p><br>)
manpagesection(SYNOPSIS)
htmlcommand(<ul>)
	bf(xmp)
	[link(bf(-8, --8bit))(8bit)]
	[link(bf(--big-endian))(bigendian)]
	[link(bf(-b, --bits))(bits) em(bits)]
	[link(bf(-c, --stdout))(stdout)]
	[link(bf(-D))(D) em(device-specific parameters)]
	[link(bf(-d, --driver))(driver) em(driver)]
	[link(bf(-e, --disable-envelope))(disableenvelope)]
	[link(bf(-f, --frequency))(frequency) em(rate)]
	[link(bf(-h, --help))(help)]
	[link(bf(-i, --interpolate))(interpolate)]
	[link(bf(-l, --loop))(loop)]
	[link(bf(--little-endian))(littleendian)]
	[link(bf(-M, --mute))(mute) em(channel-list)]
	[link(bf(-m, --mono))(mono)]
	[link(bf(--modrange))(modrange)]
	[link(bf(--nocmd))(nocmd)]
	[link(bf(--nopan))(nopan)]
	[link(bf(--ntsc))(ntsc)]
	[link(bf(-o, --output-file))(outputfile) em(filename)]
	[link(bf(-P, --pan))(pan) em(pan)]
	[link(bf(-p, --period))(period) em({linear|amiga})]
	[link(bf(-R --random))(random)]
	[link(bf(-r --reverse))(reverse)]
	[link(bf(--realtime))(realtime)]
	[link(bf(-S, --solo))(solo) em(channel-list)]
	[link(bf(-s, --start))(start) em(pos)]
	[link(bf(--stereo))(stereo)]
	[link(bf(-T, --tempo))(tempo) em(bpm)]
	[link(bf(-t, --time))(time) em(time)]
	[link(bf(-u, --unsigned))(unsigned)]
	[link(bf(-V, --version))(version)]
	[link(bf(-v, --verbose))(verbose)]
	em(modules)

htmlcommand(</ul><p><br>)
manpagesection(DESCRIPTION)
htmlcommand(<ul>)
	bf(xmp) is a tracked music module player. The current version
	recognizes the following module formats: Fasttracker II (XM),
	1 to 32 channel Noise/Fast/Protracker and variants (MOD,NST,WOW),
	Scream Tracker 3 (S3M), Scream Tracker 2 (STM), Composer 669
	(669), Ultra Tracker (ULT), Poly Tracker (PTM), Oktalyzer (OKT),
	Kris Tracker (KRIS), Multitracker (MTM), UNIC Tracker (UNIC),
	Laxity Tracker (LAX), ProRunner 1 and 2 (PRU1,PRU2), AC1D Packer
	(AC1D), Pha Packer/Propacker 1.0 (PP10), XANN Packer (XANN), Aley
	Keptr (ALM), Farandole Composer (FAR), Amusic Adlib Tracker (AMD)
	and Reality Adlib Tracker (RAD).

	bf(xmp) can play through several output devices including the Open
	Sound System sequencer, linear and ulaw PCM audio devices or mixing
	to a file.

htmlcommand(</ul><p><br>)
manpagesection(OPTIONS)
htmlcommand(<ul>)
startdit()
label(8bit)
dit(bf(-8, --8bit))
	Convert 16 bit samples to 8 bit. You may want to use this mode to
	save memory in the sound device (AWE cards will ignore this mode and
	always work with 16 bit samples).
label(bigendian)
dit(bf(--big-endian))
	Tell the software mixer to work with big-endian samples when using
	the software mixer (default is the machine byte ordering).
label(bits)
dit(bf(-b, --bits) em(bits))
	Set the software mixer resolution (8 or 16 bits). If ommited,
	The audio device will be opened at the highest resolution available.
label(stdout)
dit(bf(-c, --stdout))
	Mix the module to stdout.
label(D)
dit(bf(-D) em(device-specific parameter))
	Pass a configuration parameter to the device driver. See the
	link(bf(DEVICE DRIVER PARAMETERS))(devparm) section below for a
	list of known parameters. 
label(driver)
dit(bf(-d, --driver) em(driver))
	Select the output driver. If not specified, bf(xmp) will try to
	probe each available driver.
label(disableenvelope)
dit(bf(-e, --disable-envelope))
	Disable envelope processing.
label(frequency)
dit(bf(-f, --frequency) em(rate))
	Set the software mixer sampling rate in hertz.
label(help)
dit(bf(-h, --help))
	Show a short summary of command-line options.
label(interpolate)
dit(bf(-i, --interpolate))
	Enable or disable the software mixer interpolation routines.
label(loop)
dit(bf(-l, --loop))
	Enable module looping and backward pattern jumps.
label(littleendian)
dit(bf(--little-endian))
	Tell the software mixer to work with big-endian samples when using
	the software mixer (default is the machine byte ordering).
label(mute)
dit(bf(-M, --mute) em(channel-list))
	Mute the specified channels. em(channel-list) is a comma-separated
	list of decimal channel ranges. Example: 0,2-4,8-16.
label(mono)
dit(bf(-m, --mono))
	Force mono output (default is stereo in stereo-capable devices).
label(modrange)
dit(bf(--modrange))
	Limit MOD files to the standard three octave range. If not specified,
	MOD files will play using the full XM octave range.
label(ntsc)
dit(bf(--ntsc))
	Force NTSC (60 Hz) timing for MOD files. Default is PAL (50 Hz).
label(nocmd)
dit(bf(--nocmd))
	Disable interactive commands.
label(nopan)
dit(bf(--nopan))
	Disable dynamic panning. This option can be used to prevent
	clicking when playing in AWE32 cards. Module formats that rely only
	in dynamic pan setting to produce stereo output (e.g. XM) will use
	the MOD channel panning (LRRL).
label(outputfile)
dit(bf(-o, --output-file) em(filename))
	Mix the module to a file. If '-' is given as the file name, the
	output will be sent to stdout.
label(pan)
dit(bf(-P, --pan) em(num))
	Set the percentual panning amplitude.
label(period)
dit(bf(-p, --period) em({linear|amiga}))
	Set the period mode to Amiga (exponential) or XM linear periods
	(period = 7680 - note<<6 - finetune>>1). If not specified, bf(xmp)
	will use the default period mode for the module being played.
label(random)
dit(bf(-R, --random))
	Play modules in random order.
label(realtime)
dit(bf(--realtime))
	Play modules in realtime priority (available for FreeBSD).
label(reverse)
dit(bf(-r, --reverse))
	Reverse the left/right stereo channels.
label(solo)
dit(bf(-S, --solo) em(channel-list))
	Play only the specified channels. em(channel-list) is a
	comma-separated list of decimal channel ranges. Example: 0,2-4,8-16.
label(start)
dit(bf(-s, --start) em(pos))
	Start playing the module from the position em(pos).
label(stereo)
dit(bf(--stereo))
	Force stereo output.
label(tempo)
dit(bf(-t, --tempo) em(bpm))
	Set the initial tempo in beats per minute (default is 125).
label(time)
dit(bf(-t, --time) em(time))
	Specifies the maximum playing time to em(time) seconds.
label(unsigned)
dit(bf(-u, --unsigned))
	Tell the software mixer to use unsigned samples when mixing to
	a file (default is signed).
label(version)
dit(bf(-V, --version))
	Print version information.
label(verbose)
dit(bf(-v, --verbose))
	Verbose mode (incremental). If specified more than once, the
	verbosity level will be increased. (no messages will be displayed
	when the player runs in background)
enddit()

label(devparm)
htmlcommand(</ul><p><br>)
manpagesection(DEVICE DRIVER PARAMETERS)
htmlcommand(<ul>)
	Use the option bf(-D) to send parameters directly to the device
	drivers. Multiple bf(-D) options can be specified in the command line.

OSS sequencer:
startdit()
label(chorus)
dit(bf(-D) em(chorus=mode,depth))
	Set the chorus mode and depth. Valid modes range from 0 to 7
	(CHORUS_1, CHORUS_2, CHORUS_3, CHORUS_4, FEEDBACK, FLANGER,
	SHORTDELAY, SHORTDELAY2) and depth from 0 to 100.
	Available only in AWE cards.
label(reverb)
dit(bf(-D) em(reverb=mode,depth))
	Set the reverb mode and depth. Valid modes range from 0 to 7
	(ROOM1, ROOM2, ROOM3, HALL1, HALL2, PLATE, DELAY, PANNINGDELAY)
	and depth from 0 to 100. Available only in AWE cards.
label(opl2)
dit(bf(-D) em(opl2))
	Open the sequencer in OPL2 FM mode instead of sample mode. This
	parameter is required to play modules with FM instruments (RAD,
	AMD, S3M).
label(seqdevice)
dit(bf(-D) em(dev=device_name))
	Set the sequencer device to open. Default is /dev/sequencer.
enddit()

OSS software mixing:
startdit()
label(frag)
dit(bf(-D) em(frag=num,size))
	Set the maximum number of fragments to em(num) and the size of
	each fragment to em(size) bytes (must be a power of two).
	The number and size of fragments set a tradeoff between the buffering
	latency and sensibility to system load. To get better synchronization,
	reduce the values. To avoid gaps in the sound playback, increase
	the values.
label(ossaudiodevice)
dit(bf(-D) em(dev=device_name))
	Set the audio device to open. Default is /dev/dsp.
label(nosync)
dit(bf(-D) em(nosync))
	Don't sync the OSS audio device between modules.
enddit()

HP-UX and Solaris audio:
startdit()
label(gain)
dit(bf(-Dgain=)em(gain))
	Set the audio gain. Valid values go from 0 to 255.
	The default value is 128.
label(port)
dit(bf(-Dport=)em({s|h|l}))
	Set the audio port. Valid arguments are em(s) for the internal
	speaker, em(h) for headphones and em(l) for line out. The default
	is the internal speaker.
label(buffer)
dit(bf(-Dbuffer=)em(size))
	Set the size in bytes of the audio buffer. The default value is 32 Kb.
enddit()

htmlcommand(</ul><p><br>)
manpagesection(INTERACTIVE COMMANDS)
htmlcommand(<ul>)
The following single key commands can be used when playing modules:
startdit()
label(cmd_q)
dit(bf(q))
	Stop the currently playing module and quit the player.
label(cmd_f)
dit(bf(f))
	Jump to the next pattern.
label(cmd_b)
dit(bf(b))
	Jump to the previous pattern.
label(cmd_n)
dit(bf(n))
	Jump to the next module.
label(cmd_p)
dit(bf(p))
	Jump to the previous module.
label(cmd_spc)
dit(bf(SPACE))
	Pause the module.
label(cmd_mute)
dit(bf(1), bf(2), bf(3), bf(4), bf(5), bf(6), bf(7), bf(8), bf(9), bf(0))
	Mute/unmute channels 1 to 10.
label(cmd_unmute)
dit(bf(!))
	Unmute all channels.
enddit()

Interactive mode can be disabled using the link(bf(--nocmd))(nocmd) command
line option.

htmlcommand(</ul><p><br>)
manpagesection(EXAMPLES)
htmlcommand(<ul>)
Play module muting channels 0 to 3 and 6:
startdit()
	dit()
	tt(xmp --mute=0-3,6 module.mod.gz)
enddit()

Play modules in /dev/dsp using the default device settings (unsigned 8bit,
8 kHz mono):
startdit()
	dit()
	tt(xmp -o/dev/dsp -f8000 -m -b8 -u module.lha)
enddit()

Play all XM modules in the /mod directory and all subdirectories in
random order, ignoring any configuration set in the xmprc file:
startdit()
	dit()
	tt(xmp --norc -R `find /mod -name "*.xm*" -print`)
enddit()

htmlcommand(</ul><p><br>)
manpagesection(BUGS)
htmlcommand(<ul>)
startdit()
it()	Primary and secondary effects share the same internal variables.
	In certain module formats (e.g. XM) the primary effect should not
	be affected by the secondary effect settings.
it()	Handling of multi-file modules is confuse.
it()	Instruments with big-endian 16 bit samples will not load correctly.
it()	Command line options bf(-M) and bf(-S) don't work correctly with xxmp.
enddit()

htmlcommand(</ul><p><br>)
manpagesection(FILES)
htmlcommand(<ul>)
	tt(/etc/xmprc, $HOME/.xmprc)

htmlcommand(</ul><p><br>)
manpagesection(AUTHOR)
htmlcommand(<ul>)
	Claudio Matsuoka <email(claudio@pos.inf.ufpr.br)> and Hipolito
	Carraro Jr <email(hipolito@brhs.com.br)>.
htmlcommand(</ul>)

