
Stable versions
---------------

1.1.6 (981019):
	- Bugfix: xxmp compilation in FreeBSD fixed by Adam Hodson
	  <A.Hodson-CSSE96@cs.bham.ac.uk>
	- Bugfix: Makefile fixed for bash 2 
	- Bugfix: S3M global volume setting removed (reported by
	  John v/d Kamp <blade_@dds.nl>)
	- Bugfix: S3M tempo/BPM effect fixed (reported by Joel Jordan
	  <scriber@usa.net>)
	- Bugfix: XM loader checks module version
	- Bugfix: XM loader fixed for DEC UNIX by Andrew Leahy
	  <alf@cit.nepean.uws.edu.au>
	- Bugfix: finalvol shifted right one bit to prevent volume overflow
	  with dh-pofot.xm (Party On Funk-o-tron)
	- Bugfix: File uncompression based on magic instead of file suffix
	- Bugfix: Loop detection and time estimation improved; --noback
	  option removed (reported by Scott Scriven <toykeeper@cheerful.com>)
	- Bugfix: Invalid values for module restart are ignored (reported by
	  John v/d Kamp <blade_@dds.nl>)
	- Bugfix: Don't play invalid samples and instruments
	- Bugfix: Fine effect processing changed to the Protracker standard
	  instead of FT2 (i.e. effects EB1-EE5 play fine vol slide five times)
	- Bugfix: OSS audio driver fragment setting fixed
	- Bugfix: Added test for file type before loading
	- Bugfix: MOD/XM tempo/BPM setting fixed (reported by Gabor Lenart
	  <lgb@hal2000.hal.vein.hu>)
	- Workaround: XM loader limits number of samples (needed to play
	  Jeronen Tel's "Pools of Poison")
	- Workaround: invalid sample number in instrument map is set to
	  0xff and ignored by the player (needed to play Jeronen Tel's
	  "Pools of Poison")
	- Workaround: Jump to previous order in order zero ignored.
	- Cleanup: Channel 1 to 10 mute/unmute keys changed
	- Cleanup: cfg.mode -1 bias removed
	- Cleanup: --ignoreff option removed
	- Cleanup: reserved & unsed fields removed from structures
	- New: S3M tremor effect implemented
	- New: XM keyoff effect implemented
	- New: Experimental (untested) SGI driver
	- New: Experimental (untested) OpenBSD driver
	- New: --nocmd option added by Mark R. Boyns <boyns@sdsu.edu>
	- New: Added support for XM 1.02, Ultra Tracker, ProRunner, Propacker,
	  Kris Tracker, Unic Tracker, Laxity, FC-M, XANN and AC1D modules
	- New: Added built-in uncompressors for Powerpacker and XPK-SQSH
	- New: Option for realtime priority in FreeBSD added by Douglas
	  Carmichael <dcarmich@mcs.com>
	- New: Support for 15 bpp in xxmp added by John v/d Kamp
	  <blade_@dds.nl>

1.1.5 (980321):
	- Bugfix: Bidirectional sample loop fixed (reported by Andy Eltsov)
	- Bugfix: Set pan effect bug fixed by Frederic Bujon
	- New driver: Solaris/Sparclinux driver for the AMD 7930 audio chip
	  (tested in Solaris 2.5.1 and Linux 2.0.33)
	- New driver: Support for the Enlightened Sound Daemon
	- Better SIGSTOP/SIGCONT handling

1.1.4.1 (980330):
	- New URL updated in docs

1.1.4 (980204):
	- Bugfix: Added missing error check in Solaris and HP-UX drivers
	- Bugfix: Fixed includes for FreeBSD
	- Bugfix: Fixed X setup in the configure script
	- Bugfix: Fixed X include path in Makefile.rules and src/main/Makefile
	- Bugfix: scan.c replaced by a new version from 1.2.0 development tree
	- Bugfix: HP-UX driver works (tested in a 9000/710 with HP-UX 9.05)
	- Misc doc updates

1.1.3 (980128):
	- Bugfix: xxmp color #000000 changed to #020202 (needed in Solaris)
	- Bugfix: interactive commands (`cmd' type changed to char)
	- Bugfix: interactive commands to unmute channels 6, 7 and 8
	- Bugfix: MTM loader works in big-endian machines
	- New feature: experimental HP-UX support added (not tested)
	- Panel background colors changed
	- New INSTALL file
	- Misc doc updates

1.1.2 (980105):
	- Bugfix: xxmp palette corruption
	- Bugfix: xxmp error messages
	- Misc doc updates

1.1.1 (980103):
	- Bugfix: coredump in Oktalyzer loader (resetting pattern and
	  sample counters)
	- Bugfix: coredump with Adlib instruments
	- Bugfix: xxmp window update (added missing XSync, xxmp shows
	  current pattern and row)
	- Bugfix: color palette in 16 bpp True Color
	- Bugfix: command line arguments -S and -M

1.1.0 (971224): "The Nightmare Before Christmas" release
	- Package license changed to GPL
	- Configuration made by GNU autoconf
	- Software mixer and /dev/dsp support
	- Compiles on FreeBSD 2.2 and Solaris 2.4
	- Command line options changed, long options added
	- Random play mode added
	- AWE reverb and chorus options added
	- Support for OPL2 FM synthesizer
	- New formats supported: Elyssis Adlib Tracker (AMD), Reality Adlib
	  Tracker (RAD), Aley's Modules (ALM)
	- Support for multiple output devices
	- Support for Scream Tracker 3.00 modules (volslides in every frame)
	- Support for S3M Adlib instruments
	- Support for S3M (very old) signed samples
	- Support for S3M pan ("The Crossing" plays correctly)
	- Support for S3M global volume
	- Support for Oktalyzer 7 bit samples
	- Support for IFF modules and variations
	- S3M arpeggio kludge removed
	- S3M module length adjusted discarding 0xff paterns
	- S3M set tempo/BPM effect adjusted
	- XM envelope loop bug fixed ("Shooting Star" plays correctly)
	- XM 16 bit sample conversion bug fixed ("Hyperdrive" plays correctly)
	- Support for XM instruments with 29 byte headers (for "Braintomb") 
	- AWE32 pan setting fixed
	- Glissando in linear period mod bug fixed
	- Volume overflow bug fixed (again)
	- Tone portamento update bug fixed
	- Period setting workaround for panic.s3m
	- Pattern jump effect bug fixed
	- Oktalyzer loader bugs fixed
	- period_to_bend precision loss bug fixed
	- Option -s fixed to play with correct tempo/BPM/volume
	- Added support for bzip, compress, zip and lha compressed modules
	- Added Protracker and Soundtracker wrappers to the MOD loader
	- Support for MDZ modules with ADPCM samples
	- IPC stuff removed, player engine built as a library
	- Fixed memory leak in MOD loader
	- Fixed memory leak in oss_seq
	- X11 version (xxmp)
	- Interactive commands
	- xmprc file

1.0.1 (970419):
	- IPC global volume setting bug fixed
	- FAR number of patterns bug fixed
	- S3M volume setting effect correctly handled (fixes Skaven's
	  2nd Reality)
	- Option to disable dynamic panning to prevent AWE-32 clicking

1.0.0 (970330): First non-experimental release
	- Added option -t (maximum playing time)
	- Added option -K to enable IPC
	- Test module removed from package


Experimental versions
---------------------

0.99c (970320): Fixed more bugs reported by Michael Janson
	- S3M loader changed to recognize fine and extra fine volume slides
	  only when the slide nibble is not zero (fixes PM's 2nd Reality)
	- XM patterns with 0 (==0xff) rows are being correctly handled
	  (Wave's Home Vist should play better)
	- Tone portamento effect does not reset envelopes (fixes Wave's
	  Home Visit pattern 0, channels 0 to 5)
	- Loop click removal fixed & improved - chipsamples sound smoother
	  using gmod's method to prevent clicking
	- Continue vibrato effect bugfixed

0.99b (970318): Fixed bugs reported by Antti Huovilainen and Michael Janson
	- Extra fine portamento bug fixed (ascent.s3m should play better)
	- Volume column tone portamento in XM shifted left 4 bits (fixes
	  guitar in Zodiak's Status Mooh order 7, channel 7)
	- Note delay bug fixed (fixes bass in Jogeir Liljedahl's Guitar
	  Slinger) - delay was working as note retrig
	- Sample offset effect bug fixed (fixes snare drum in Zodiak's
	  Status Mooh order 0D channel 5) - offset 00 uses previous offset
	- New instrument event with same instrument does not retrig the
	  sample (fixes pad in Romeo Knight's Wir Happy Hippos)
	- Global volume limited to 0x40 (fixes fadeout in Zodiak's Reflecter)
	- Sample loop adjusted for click removal
	- 669 loader changed to use secondary effects for tempo/break
	- S3M loader changed to use generic pattern loops (S3M-specific
	  pattern loop kluge removed from xm_play.c)
	- MOD loader fixed - the module may have unused patterns stored
	  and this situation was confusing the loader
	- Effect F changed to recognize 32 frames per row

0.99a (970313):
	- General code review
	- Internal module format changed to XXM
	- Added endianism correction
	- Volume overdrive bug fixed
	- Verbosity levels adjusted
	- Vibrato implementation bug fixed
	- Instrument vibrato sweep implemented
	- New module formats supported: STM, 669, WOW, MTM, PTM, OKT, FAR
	- Added mute/solo channel command line options
	- Tempo 0 ignored
	- Lots of cosmetic changes
	- Option to reduce sample resolution to 8 bits
	- Envelope sustain bug ("Zodiak bug") fixed (reported by Beta)
	- Infinite loop in pattern jump bug fixed

0.09e (970105): Improved S3M support and general bugfixes
	- Yet another pattern loop bug fixed
	- S3M J00 (arpeggio) effect workaround
	- S3M stereo enable/disable implemented
	- S3M sample pan bug fixed
	- Added warning for S3M Adlib channels
	- Improved S3M channel pan handling
	- Incremental verbosity option
	- Tone portamento behaviour fixed (for "Elimination Part I")
	- Added parameter -i to ignore S3M end of module markers
	- S3M FFx/F00 (continue fine period slide) effect bug fixed
	  (bug was audible in the Second Reality opening theme)
	- Global volume slide bug fixed
	- installbin target fixed in the Makefile
	- Volume reset with no instrument for new note bug fixed
	  (bug was audible in "Knulla Kuk" by Moby)

0.09d (970101):
	- Pattern jump bug fixed
	- Added support for ??CH mods - thanks to Toru Egashira
	  <toru@jms.jeton.or.jp>
	- Fine pitchbending effect bug fixed
	- Signal handling fixed (again)
	- USR1 and USR2 signals changed to ABRT and HUP
	- Command line parameter to force MOD octave range
	- NTSC timing for MOD files
	- Glissando effect implemented
	- Retrig and multi-retrig effects bug fixed
	- S3M lossage: fine volume slide effect translation bug fixed
	- S3M lossage: C2SPD translation to relnote/finetune bug fixed
	- S3M lossage: pattern loop fixed
	- S3M lossage: module loop bug fixed
	- Pattern loop (for restart order>=0x7f) bug fixed
	- version.o dependencies fixed in the Makefile

0.09c (970101): broken version (unreleased)

0.09b (961210):
	- Note release and fadeout bug fixed
	- Module restart (SIGUSR2) bug fixed
	- Octave shift bug fixed ("Move to da beat" plays OK)
	- "Squeak" bug fixed (the bug was caused by a tone portamento
	  with no destination note)
	- Pitchbending effect bug fixed ("Crystal Dragon" plays OK)

0.09a (961207): First public release.
	- Panel signal handling fixed
	- base_note set with C4 frequency of 130.812 Hz (actually C3)
	- GUS_VOICE_POS enabled for AWE_DEVICE (Iwai's patch)
	- Envelope fadeout (release) fixed
	- Note skip bug corrected after some shotgun debugging
	- GUS panning fixed (bypassing sequencer.h)
	- Added panning amplitude command line option
	- Added a channel pan parameter
	- Changed the XM loader to always unpack the patterns
	- S3M pan positions fixed
	- Timing variables changed to floating point - I really don't like
          FP, maybe I've been hacking in assembly language too much
	- Added 15-instrument MOD loader
	- Added XM finetune interpolation
	- Arpeggio bug fixed: pitchbend increments between semitones is 100
          and not 128 (why don't they use ROUND numbers?)
	- Changed period2bend to prevent lossage in higher octaves
	- Pattern loop effect implemented (running_lamer.mod plays OK)
	- Auto-detector (?) for 15-instrument MODs (option -f removed)
	- Added linear period support
	- All source files checked into RCS


Development (unreleased) versions
---------------------------------

0.08 (961031):
	- Increased code mess
	- Included Iwai's AWE support
	- devices.c created to wrap output devices
	- sequencer.c, awe.c and gus.c included in devices.c
	- Portability macros set in the Makefile (but not used)
	- Manpage draft included in the package
	- Added command-line device selector
	- Finally got rid of those ridiculous fread()s in xm_load.c
	- xm_instrument_header split into xm_instrument_header and
	  xm_instrument
	- Removed OSS macros from xm_play.c
	- Volume overflow bug fixed ("Thematic Hymn" plays OK)
	- Scream Tracker S3M loader
	- Fixed the song length bug
	- XM relnotes are working again!
	- Added a garbage character filter to the MOD loader
	- Floating point stuff removed
	- Sequencer sync message support added
	- Multiple file entry point bug fixed
	- Song loop bug fixed, added a loop-enable option
	- Tremolo and extra fine portamento effects fixed
	- Player doesn't try to play invalid instruments (and dump core)
	- SIGUSR1 and SIGUSR2 handlers added (abort/restart module)
	- MOD effects with parameter 0 filtered in the loader (nasty bug)
	- Finetunes partially fixed ("Ooo-uh-uh-uh" does not work)
	- Started X11 panel (VERY experimental)
	- Volume column effect fxp bug fixed
	- Envelope retrig on tone portamento bug fixed
	- MOD sample loop length fixed
	- Finetune in tone portamento bug fixed
	
0.07 (961011): We've screwed up XM relnotes in this version. Yuck!
	- Sample loop bug fixed
	- Extra fine portamento effect implemented
	- Global volume set/slide effects implemented
	- Pan slide effect implemented
	- Delay pattern effect implemented
	- Retriggered tremolo/vibrato implemented
	- Added tremolo/vibrato waveforms 4, 5 and 6 (no retrigger)
	- Stereo reverse/mono command line options are now functional
	- Pan slide effect implemented (but does it work?)
	- Arpeggio effect implemented
	- "Official" Amiga (exponential) periods implemented
        - Multi-retrig and delay effects implemented
	- Retrig and cut implemented as special cases of multi retrig
	- Fixed vibrato/tremolo waveforms
	- Added some macros to reduce the code mess
	- Finetunes/relnotes processed by the player (and not by the loader)
	
0.06 (960924): This version can play most MODs
	- Changed a lot of variable names
        - Fixed envelope processing
	- Fixed pitchbending (SEQ_BENDER vs SEQ_PITCHBEND) bug
	- Fixed panning (SEQ_CONTROL vs SEQ_PANNING) bug
	- Fixed multisample struct definition bug
	- Fixed note number "obi-wan" bug ("Neverending Story" plays OK)
	- Fixed tone portamento behavior ("Art of Chrome" plays OK)
	- Added MOD finetune support ("Elimination Part I" plays OK)
	- Added offset, cut, delay and retrig effects

0.05 and before:
	- Lots of changes.


