/* Extended Module Player
 * Copyright (C) 1996-1998 Claudio Matsuoka and Hipolito Carraro Jr
 * $Id$.
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See doc/COPYING
 * for more information.
 */

#ifndef __DRIVER_H
#define __DRIVER_H

#include "xxm.h"

#if !defined(DRIVER_OSS_SEQ) && !defined(DRIVER_OSS_MIX)

#define GUS_PATCH 1

struct patch_info {
    unsigned short key;
    short device_no;			/* Synthesizer number */
    short instr_no;			/* Midi pgm# */
    unsigned int mode;
    int len;				/* Size of the wave data in bytes */
    int loop_start, loop_end;		/* Byte offsets from the beginning */
    unsigned int base_freq;
    unsigned int base_note;
    unsigned int high_note;
    unsigned int low_note;
    int panning;			/* -128=left, 127=right */
    int detuning;
    int volume;
    char data[1];			/* The waveform data starts here */
};

#endif


#define XMP_PAT_16BIT	0x0001
#define XMP_PAT_SIGNED	0x0002
#define XMP_PAT_LOOP	0x0004
#define XMP_PAT_BILOOP	0x0008

struct drv_config {
#define MIX_SIGNED	0x0001
#define MIX_BIGENDIAN	0x0002
#define MIX_16BIT	0x0004
#define MIX_ULAW	0x0008
    int	fmt;			/* Bitmapped 0=sig, 1=end, 2=res */
#define MIX_MONO	1
#define MIX_STEREO	2
    int	mode;			/* Mode, 1=mono, 2=stereo */
    int	rate;			/* Sampling rate in hertz */
    int	nbuf;			/* Number of output buffers */
    int nchn;			/* Number of channels set */
    int	itpt;			/* 1 if interpolator is active */
    char *parm[64];		/* FIXME: Parameters */
};

int		drv_open	(char *);
void		drv_register	(void);
void		drv_list	(const int);
void		drv_help	(void);
void		drv_loadpatch	(FILE *, int, int, int,
    				 struct xxm_sample *, char *);
void		drv_savepatch	(FILE *, int, int, int,
    				 struct xxm_sample *, char *);

#define parm_init() for (parm = cfg->parm; *parm; parm++) { \
	token = strtok (*parm, ":="); token = strtok (NULL, "");
#define parm_end() }
#define parm_error() { \
	fprintf (stderr, "xmp: incorrect parameters in -c %s\n", *parm); \
	exit (-4); }
#define chkparm0(x,y) { if (!strcmp (*parm,x)) { y; } }
#define chkparm1(x,y) { if (!strcmp (*parm,x)) { y; } }
#define chkparm2(x,y,z,w) { if (!strcmp (*parm,x)) { \
	if (2 > sscanf (token, y, z, w)) parm_error (); } }

#endif /* __DRIVER_H */
