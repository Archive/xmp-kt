/* Extended Module Player
 * Copyright (C) 1996-1998 Claudio Matsuoka and Hipolito Carraro Jr
 * $Id$
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See doc/COPYING
 * for more information.
 */

#ifndef __FORMATS_H
#define __FORMATS_H

void fmt_list (const int);
void fmt_register (void);

int xm_load (FILE *);
int xm_save (FILE *);
int mod_load (FILE *);
int mod_save (FILE *);
int m15_load (FILE *);
int m15_save (FILE *);
int s3m_load (FILE *);
int stm_load (FILE *);
int rtm_load (FILE *);
int mdl_load (FILE *);
int ssn_load (FILE *);
int far_load (FILE *);
int mtm_load (FILE *);
int ult_load (FILE *);
int fnk_load (FILE *);
int alm_load (FILE *);
int amd_load (FILE *);
int rad_load (FILE *);
int ptm_load (FILE *);
int okt_load (FILE *);
int it_load (FILE *);
int pru1_load (FILE *);
int pru2_load (FILE *);
int ac1d_load (FILE *);
int unic_load (FILE *);
int lax_load (FILE *);
int fcm_load (FILE *);
int kris_load (FILE *);
int pp10_load (FILE *);
int xann_load (FILE *);

#endif /* __FORMATS_H */
