/* Extended Module Player
 * Copyright (C) 1997 Claudio Matsuoka and Hipolito Carraro Jr
 * $Id$
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See doc/COPYING
 * for more information.
 */

#ifndef __MIXER_H
#define __MIXER_H

#define MIX_OK	0
#define MIX_ERR	1

int	smix_open	(struct drv_config *);
void	smix_close	(void);
void	smix_starttimer	(void);
void	smix_resetvoice	(int);
void	smix_echoback	(int);
void	smix_numvoices	(int);
void	smix_voicepos	(int, int);
void	smix_echoback	(int);
void	smix_setpatch	(int, int);
void	smix_setvol	(int, int);
void	smix_setnote	(int, int);
void	smix_setpan	(int, int);
int	smix_mixer	(void);
void	smix_setbend	(int, int);
void	smix_starttimer	(void);
void	smix_stoptimer	(void);
void	smix_resetvoices (void);
void	smix_bufwipe	(void);
void	smix_clearmem	(void);
void	smix_sync	(void);
void	smix_writepatch	(struct patch_info *);
int	smix_getmsg	(void);
void	smix_getfrags	(int*, int*);
void	smix_init	(struct drv_config *);

#endif /* __MIXER_H */

