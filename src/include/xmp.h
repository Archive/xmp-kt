/* Extended Module Player
 * Copyright (C) 1996-1998 Claudio Matsuoka and Hipolito Carraro Jr
 * $Id$
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See doc/COPYING
 * for more information.
 */

#ifndef __XMP_H
#define __XMP_H

/* Event echo messages */
#define XMP_ECHO_NONE	0x00
#define XMP_ECHO_END	0x01
#define XMP_ECHO_BPM	0x02
#define XMP_ECHO_VOL	0x03
#define XMP_ECHO_INS	0x04
#define XMP_ECHO_ORD	0x05
#define XMP_ECHO_ROW	0x06
#define XMP_ECHO_CHN	0x07
#define XMP_ECHO_PBD	0x08
#define XMP_ECHO_GVL	0x09

/* xmp_player_ctl arguments */
#define XMP_ORD_NEXT	0x00
#define XMP_ORD_PREV	0x01
#define XMP_ORD_SET	0x02
#define XMP_MOD_STOP	0x03
#define XMP_MOD_RESTART	0x04
#define XMP_MOD_PAUSE	0x05
#define XMP_GVOL_INC	0x06
#define XMP_GVOL_DEC	0x07
#define XMP_TIMER_STOP          0x08
#define XMP_TIMER_RESTART       0x09

#define xmp_ord_next()		xmp_player_ctl (XMP_ORD_NEXT, 0)
#define xmp_ord_prev()		xmp_player_ctl (XMP_ORD_PREV, 0)
#define xmp_ord_set(x)		xmp_player_ctl (XMP_ORD_SET, (x))
#define xmp_mod_stop()		xmp_player_ctl (XMP_MOD_STOP, 0)
#define xmp_stop_module()	xmp_player_ctl (XMP_MOD_STOP, 0)
#define xmp_mod_restart()	xmp_player_ctl (XMP_MOD_RESTART, 0)
#define xmp_restart_module()	xmp_player_ctl (XMP_MOD_RESTART, 0)
#define xmp_mod_pause()		xmp_player_ctl (XMP_MOD_PAUSE, 0)
#define xmp_gvol_inc()		xmp_player_ctl (XMP_GVOL_INC, 0)
#define xmp_gvol_dec()		xmp_player_ctl (XMP_GVOL_DEC, 0)
#define xmp_timer_stop()        xmp_player_ctl (XMP_TIMER_STOP, 0)
#define xmp_timer_restart()     xmp_player_ctl (XMP_TIMER_RESTART, 0)
#define xmp_mod_load            xmp_load_module
#define xmp_mod_save            xmp_save_module
#define xmp_mod_play            xmp_play_module
#define xmp_mod_new             blank_load

/* Errors */

#define XMP_E_DINIT	-1	/* can't initialize driver */
#define XMP_E_NODRV	-2	/* no such driver */
#define XMP_E_DSPEC	-3	/* driver not specified */



struct xmp_fmt_info {
    char *suffix;
    char *tracker;
    int (*loader)();
    int (*saver)();
    struct xmp_fmt_info *next;
};

struct xmp_drv_info {
    char *id;
    char *description;
    char **help;
    int (*init)();
    void (*shutdown)();
    void (*numvoices)();
    void (*voicepos)();
    void (*echoback)();
    void (*setpatch)();
    void (*setvol)();
    void (*setnote)();
    void (*setpan)();
    void (*setbend)();
    void (*starttimer)();
    void (*stoptimer)();
    void (*reset)();
    void (*bufdump)();
    void (*bufwipe)();
    void (*clearmem)();
    void (*sync)();
    void (*writepatch)();
    int (*getmsg)();
    struct xmp_drv_info *next;
};

struct xmp_module_info {
    char title[40];
    int chn;
    int pat;
    int ins;
    int trk;
    int smp;
    int len;
    int bpm;
    int tpo;
};

struct xmp_options {
    int verbose;		/* Verbosity level */
    int linear;			/* XM linear period mode */
    int reverse;		/* Reverse stereo (-1, 0, 1) */
    int loop;			/* Allow pattern jumps */
    int start;			/* Initial order */
    int mix;			/* Percentage of L/R channel separation */
    int flags;                  /* Set Amiga & Linear Table Mode */
    int modrange;		/* Force Amiga period limits */
    int ntsc;			/* Use NTSC timing instead of PAL */
    int freq;			/* Software mixing rate (Hz) */
    int res8bit;		/* Output resolution */
    int smp8bit;		/* Convert samples to 8 bit */
    int time;			/* Maximum playing time in seconds */
    int noenv;			/* Disable envelopes  */
    int nopan;			/* Don't use dynamic pan */
    int interpolate;		/* Software mixer interpolating mode */
    int surround;		/* Stereo surround mode */
    int tempo;			/* Initial tempo */
    char *outfile;		/* Output file name when mixing to file */
    int usmp;			/* Use unsigned samples */
    int bsmp;			/* Use big-endian 16 bit samples */
    char *drv_id;		/* ID of driver to use */
    double rrate;		/* Replay rate */
    int c4rate;			/* C4 replay rate */
    int vol_base;		/* Volume base */
    int *vol_xlat;		/* Volume translation table */
    char *filename;		/* Module file name */
    /* Internal options */
    int vef;			/* Volumeslides in every frame */
};


extern char *__xmp_version;
extern char *__xmp_date;
extern char *__xmp_copyright;
extern char *__xmp_build;


void	xmp_init			(int, char **, struct xmp_options *);
int	xmp_load_module			(char *);
int	xmp_save_module			(char *);
int	xmp_play_module			(void);
void	xmp_play_pattern		(int);
struct xmp_module_info*
	xmp_get_module_info		(struct xmp_module_info *);
struct xmp_fmt_info*
	xmp_get_fmt_info		(struct xmp_fmt_info **);
struct xmp_drv_info*
	xmp_get_drv_info		(struct xmp_drv_info **);
char*	xmp_get_driver_description 	(void);
int	xmp_open_audio			(struct xmp_options *);
void	xmp_close_audio			(void);
void	xmp_display_license		(void);
void	xmp_set_driver_parameter 	(char *);
void	xmp_channel_mute		(int, int, int);
void	xmp_get_driver_cfg		(int *, int *, int *, int *);
void	xmp_register_event_callback	(void (*)());
void	xmp_unregister_event_callback	(void);
int	xmp_player_ctl			(int, int);

int	xmp_tell_parent			(void);
int	xmp_wait_parent			(void);
int	xmp_check_parent		(int);
int	xmp_tell_child			(void);
int	xmp_wait_child			(void);
int	xmp_check_child			(int);
void*	xmp_get_shared_mem		(int);
#define xmp_calloc(a, b) xmp_get_shared_mem((a)*(b))
void	xmp_detach_shared_mem		(void *);
int	xmp_verbosity_level		(int);
void  xmp_shared_mem_push_mark        (void);
void  xmp_shared_mem_pop              (void);
int   blank_load                      (void);


#endif /* __XMP_H */
