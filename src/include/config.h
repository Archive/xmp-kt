/* src/include/config.h.  Generated automatically by configure.  */
/* config.h.in.  Generated automatically from configure.in by autoheader.  */
/* acconfig.h.  Generated manually by Claudio Matsuoka */

/* Define HAVE_ATTRIBUTE_PACKED if your compiler understands __attribute__
 * ((packed))
 */
#define HAVE_ATTRIBUTE_PACKED 1

/* Define __lint and __EXTENSIONS__ in SunOS 5 */
/* I don't think these are needed for a 'normal' Solaris box, but
 * barracuda needs this stuff <sigh>
 */
/* #undef	__lint */
/* #undef	__EXTENSIONS__ */

/* Define DRIVER_OSS_* to enable OSS /dev/sequencer and /dev/audio support */
#define DRIVER_OSS_SEQ 1
#define DRIVER_OSS_MIX 1

/* Define to enable HP-UX audio support */
/* #undef DRIVER_HPUX */

/* Define to enable Solaris (CS4231) audio support */
/* #undef DRIVER_SOLARIS */

/* Define to enable SGI audio support */
/* #undef DRIVER_SGI */

/* Define to enable BSD audio support */
/* #undef DRIVER_BSD */

/* Define to enable socket audio output */
/* #undef DRIVER_NET */

/* Define to enable ESD (Enlghtened Sound Daemon) support  */
/* #undef DRIVER_ESD */

/* Define if you don't have vprintf but do have _doprnt.  */
/* #undef HAVE_DOPRNT */

/* Define if OSS defines audio_buf_info.  */
#define HAVE_AUDIO_BUF_INFO 1

/* Define if you have the vprintf function.  */
#define HAVE_VPRINTF 1

/* Define if you have the usleep function.  */
/* #undef HAVE_USLEEP */

/* Define if you have the nanosleep function.  */
/* #undef HAVE_NANOSLEEP */

/* Define if you have the getopt_long function.  */
#define HAVE_GETOPT_LONG 1
#ifdef HAVE_GETOPT_LONG
#define ELIDE_CODE
#endif

/* Define as the return type of signal handlers (int or void).  */
#define RETSIGTYPE void

/* Define if you have the ANSI C header files.  */
#define STDC_HEADERS 1

/* Define if your processor stores words with the most significant
   byte first (like Motorola and SPARC, unlike Intel and VAX).  */
/* #undef WORDS_BIGENDIAN */

/* Define if you have the select function.  */
#define HAVE_SELECT 1

/* Define if your stdarg doesn't work (yikes!)  */
/* #undef HAVE_BROKEN_STDARG */

/* Define if you have the <sbus/audio/audio.h> header file.  */
/* #undef HAVE_SBUS_AUDIO_AUDIO_H */

/* Define if you have the <awe_voice.h> header file.  */
/* #undef HAVE_AWE_VOICE_H */

/* Define if you have the <linux/awe_voice.h> header file.  */
#define HAVE_LINUX_AWE_VOICE_H 1

/* Define if you have the <machine/soundcard.h> header file.  */
/* #undef HAVE_MACHINE_SOUNDCARD_H */

/* Define if you have the <linux/ultrasound.h> header file.  */
#define HAVE_LINUX_ULTRASOUND_H 1

/* Define if you have the <machine/ultrasound.h> header file.  */
/* #undef HAVE_MACHINE_ULTRASOUND_H */

/* Define if you have the <sys/audioio.h> header file.  */
/* #undef HAVE_SYS_AUDIOIO_H */

/* Define if you have the <sys/audio.io.h> header file.  */
/* #undef HAVE_SYS_AUDIO_IO_H */

/* Define if you have the <sun/audioio.h> header file.  */
/* #undef HAVE_SUN_AUDIOIO_H */

/* Define if you have the <strings.h> header file.  */
#define HAVE_STRINGS_H 1

/* Define if you have the <getopt.h> header file.  */
#define HAVE_GETOPT_H 1

/* Define if you have the <sys/awe_voice.h> header file.  */
/* #undef HAVE_SYS_AWE_VOICE_H */

/* Define if you have the <sys/select.h> header file.  */
#define HAVE_SYS_SELECT_H 1

/* Define if you have the <sys/soundcard.h> header file.  */
#define HAVE_SYS_SOUNDCARD_H 1

/* Define if you have the <sys/ultrasound.h> header file.  */
/* #undef HAVE_SYS_ULTRASOUND_H */

/* Define if you have the <sys/resource.h> header file.  */
/* #undef HAVE_SYS_RESOURCE_H */

/* Define if you have the <sys/rtprio.h> header file.  */
/* #undef HAVE_SYS_RTPRIO_H */

/* End of config.h.in */
