/* Extended Module Player
 * Copyright (C) 1996-1998 Claudio Matsuoka and Hipolito Carraro Jr
 * $Id$
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See doc/COPYING
 * for more information.
 */

#ifndef __XXM_H
#define __XXM_H

struct xxm_header {
#define XXM_FLG_LINEAR	0x01
#define XXM_FLG_MODRNG	0x02
    int flg;				/* Flags */
    int pat;				/* Number of patterns */
    int ptc;				/* Number of patches */
    int trk;				/* Number of tracks */
    int chn;				/* Tracks per pattern */
    int ins;				/* Number of instruments */
    int smp;				/* Number of samples */
    int tpo;				/* Initial tempo */
    int bpm;				/* Initial BPM */
    int len;				/* Module length in patterns */
    int rst;				/* Restart position */
    int gvl;				/* Global volume */
    /*char *title;*/
};

struct xxm_channel {
    int pan;
    int vol;
#define XXM_CHANNEL_FM 0x01
    int flg;
};

struct xxm_trackinfo {
    int index;				/* Track index */
};

struct xxm_pattern {
    int rows;				/* Number of rows */
    struct xxm_trackinfo info[1];
};

struct xxm_event {
    uint8 note;				/* Note number (0==no note) */
    uint8 ins;				/* Patch number */
    uint8 vol;				/* Volume (0 to 64) */
    uint8 fxt;				/* Effect type */
    uint8 fxp;				/* Effect parameter */
    uint8 f2t;				/* Secondary effect type */
    uint8 f2p;				/* Secondary effect parameter */
};

struct xxm_track {
    int rows;				/* Number of rows */
    struct xxm_event event[1];
};

struct xxm_envinfo {
#define XXM_ENV_ON	0x01
#define XXM_ENV_SUS	0x02
#define XXM_ENV_LOOP	0x04
#define XXM_ENV_RLS	0x08
    int flg;				/* Flags */
    int npt;				/* Number of envelope points */
    int scl;				/* Envelope scaling */
    int sus;				/* Sustain point */
    int lps;				/* Loop start point */
    int lpe;				/* Loop end point */
};

struct xxm_instrument_header {
    uint8 name[32];			/* Instrument name */
    int nsm;				/* Number of samples */
    int rls;
    struct xxm_envinfo aei;		/* Amplitude envelope info */
    struct xxm_envinfo pei;		/* Pan envelope info */
    struct xxm_envinfo fei;		/* Frequency envelope info */
};

struct xxm_instrument_map {
    uint8 ins[96];			/* Instrument number for each key */
};

struct xxm_instrument {
    int vol;				/* Volume */
    int pan;				/* Pan */
    int xpo;				/* Transpose */
    int fin;				/* Finetune */
    int vwf;				/* Vibrato waveform */
    int vde;				/* Vibrato depth */
    int vra;				/* Vibrato rate */
    int vsw;				/* Vibrato sweep */
    int sid;				/* Sample number */
};

struct xxm_sample {
    uint8 name[32];			/* Sample name */
    int len;				/* Sample length */
    int lps;				/* Loop start */
    int lpe;				/* Loop end */
#define XXM_SMP_16BIT	0x01
#define XXM_SMP_FDLOOP	0x02
#define XXM_SMP_BDLOOP	0x04
#define XXM_SMP_RLS	0x02
    int flg;				/* Flags */
    struct patch_info *patch;		/* Patch */
};

#endif /* __XM_H */
