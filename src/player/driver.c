/* Extended Module Player
 * Copyright (C) 1996,1997 Claudio Matsuoka and Hipolito Carraro Jr
 * $Id$
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See doc/COPYING
 * for more information.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdlib.h>
#include <string.h>

#include "xmpi.h"
#include "driver.h"
#include "adpcm.h"

int __mode_fm = 0;

extern struct xmp_drv_info drv_file;
#ifdef DRIVER_SOLARIS
extern struct xmp_drv_info drv_solaris;
#endif
#ifdef DRIVER_HPUX
extern struct xmp_drv_info drv_hpux;
#endif
#ifdef DRIVER_BSD
extern struct xmp_drv_info drv_bsd;
#endif
#ifdef DRIVER_SGI
extern struct xmp_drv_info drv_sgi;
#endif
#ifdef DRIVER_OSS_SEQ
extern struct xmp_drv_info drv_oss_seq;
#endif
#ifdef DRIVER_OSS_MIX
extern struct xmp_drv_info drv_oss_mix;
#endif
#ifdef DRIVER_NET
extern struct xmp_drv_info drv_net;
#endif
#ifdef DRIVER_ESD
extern struct xmp_drv_info drv_esd;
#endif

extern char ivalid[256];

struct xmp_drv_info *drv, *__drv_head;
struct drv_config cfg;

void (*__event_callback)(unsigned long);


static struct xmp_drv_info *select_driver (char *i)
{
    struct xmp_drv_info *d;

    if (i) {
	for (d = __drv_head; d; d = d->next)
	    if (!strcmp (d->id, i))
		return d;
    }
    return NULL;
}


int drv_open (char *id)
{
    struct xmp_drv_info *d = NULL;

    cfg.fmt = 0;

    if (opt.bsmp)
	cfg.fmt |= MIX_BIGENDIAN;

    if (!opt.res8bit) {
	cfg.fmt |= MIX_16BIT;
	cfg.fmt |= MIX_SIGNED;
    }

    cfg.mode = opt.reverse ? MIX_STEREO : MIX_MONO;
    cfg.rate = opt.freq;
    cfg.itpt = opt.interpolate;
    cfg.nbuf = 2;

    if (opt.outfile)
	id = "file";

    if (id != NULL) {
	d = select_driver (id);
	if (d != NULL) {
	    if (d->init (&cfg) == 0)
		return XMP_E_DINIT;
	}
    } else {
	struct xmp_drv_info *t;

	for (t = __drv_head->next; t; t = t->next) {
	    if (opt.verbose > 2) {
		report ("Probing %s... ", t->description);
	    }
	    if (t->init (&cfg)) {
		d = t;
		if (opt.verbose > 2)
		    report ("found\n");
		break;
	    } else if (opt.verbose > 2) {
		report ("not found\n");
	    }
	}
    }

    if (d == NULL) {
	if (id != NULL)
	    return XMP_E_NODRV;

	return XMP_E_DSPEC;
    }

    drv = d;

    return 0;
}


static void register_driver (struct xmp_drv_info *d)
{
    if (!__drv_head)
	__drv_head = d;
    else {
	struct xmp_drv_info *c;
	for (c = __drv_head; c->next; c = c->next);
	c->next = d;
    }
    d->next = NULL;
}


void drv_register ()
{
    /* Output to file will be always available */
    register_driver (&drv_file);
#ifdef DRIVER_SOLARIS
    register_driver (&drv_solaris);
#endif
#ifdef DRIVER_HPUX
    register_driver (&drv_hpux);
#endif
#ifdef DRIVER_BSD
    register_driver (&drv_bsd);
#endif
#ifdef DRIVER_SGI
    register_driver (&drv_sgi);
#endif
#ifdef DRIVER_OSS_SEQ
    register_driver (&drv_oss_seq);
#endif
#ifdef DRIVER_OSS_MIX
    register_driver (&drv_oss_mix);
#endif
#ifdef DRIVER_NET
    register_driver (&drv_net);
#endif
#ifdef DRIVER_ESD
    register_driver (&drv_esd);
#endif
}


void drv_loadpatch (FILE * f, int id, int basefreq, int flags,
	struct xxm_sample *xxs, char *buffer)
{
    int i;
    uint8 *buf;
    struct patch_info *patch;

    /* FM patches */

    if (xxs == NULL) {
	patch = calloc (1, sizeof (struct patch_info) + 11);
	memcpy (&patch->data, buffer, 11);
	patch->len = -1;
	patch->instr_no = id;
	drv->writepatch (patch);
	free (patch);
	return;
    }

    if (id == 0xff)
	return;

    /* Empty samples */

    if (xxs->len <= 4) {
	char _b[4];
	if (~flags & XMP_SMP_NOLOAD)
	    fread (_b, 1, xxs->len, f);
	return;
    }

    /* Patches with samples */

    if (opt.smp8bit && (xxs->flg & WAVE_16_BITS)) {
	/* Convert 16 bit samples to 8 bit */
	patch = calloc (1, xxs->len / 2 + sizeof (struct patch_info));
	buf = calloc (1, xxs->len);

	if (flags & XMP_SMP_NOLOAD) {
	    memcpy (buf, buffer, xxs->len);
	} else
	    fread (buf, 1, xxs->len, f);

#ifdef WORDS_BIGENDIAN
	{
	    int i;
	    char c;

	    for (i = 0; i < xxs->len; i += 2) {
		c = buf[i];
		buf[i] = buf[i + 1];
		buf[i + 1] = c;
	    }
	}
#endif

	if (flags & XMP_SMP_DIFF)
	    diff2abs (xxs->len, xxs->flg & WAVE_16_BITS, buf);
	else if (flags & XMP_SMP_8BDIFF)
	    diff2abs (xxs->len, 0, buf);

	xxs->len /= 2;
	xxs->lpe /= 2;
	xxs->lps /= 2;

	for (i = 0; i < xxs->len; i++)
	    buf[i] = ((uint16 *) buf)[i] >> 8;

	memcpy (patch->data, buf, xxs->len);
	free (buf);
	xxs->flg &= ~WAVE_16_BITS;
    } else {
	patch = calloc (1, xxs->len + sizeof (struct patch_info));
	if (flags & XMP_SMP_NOLOAD) {
	    memcpy (patch->data, buffer, xxs->len);
	} else {
	    int x;
	    char s[5];

	    x = ftell (f);
	    fread (s, 1, 5, f);
	    fseek (f, x, SEEK_SET);

	    /* Test for ADPCM encoded samples - these are usually found on
	     * "MDZ" files (zipped M.K.)
	     */
	    if (!strncmp (s, "ADPCM", 5)) {
		int x2 = xxs->len / 2;
		char table[16];
		struct adpcm_state state;

		fseek (f, 5, SEEK_CUR);		/* Skip "ADPCM" */
		fread (table, 1, 16, f);
		fread (patch->data + x2, 1, x2, f);
		adpcm_decoder (patch->data + x2, patch->data, table,
			xxs->len, &state);
	    } else {
	        fread (patch->data, 1, xxs->len, f);

		if (xxs->flg & WAVE_16_BITS) {
#ifdef WORDS_BIGENDIAN
		    int i;
		    char c;

		    for (i = 0; i < xxs->len; i += 2) {
			c = patch->data[i];
			patch->data[i] = patch->data[i + 1];
			patch->data[i + 1] = c;
		    }
#endif
		}
	    }
	}

	if (flags & XMP_SMP_7BIT)
	    double_sample (xxs->len, patch->data);
	if (flags & XMP_SMP_DIFF)
	    diff2abs (xxs->len, xxs->flg & WAVE_16_BITS, patch->data);
	else if (flags & XMP_SMP_8BDIFF)
	    diff2abs (xxs->len, 0, patch->data);
    }

    patch->key = GUS_PATCH;
    patch->instr_no = id;
    patch->mode = xxs->flg;
    patch->mode |= (flags & XMP_SMP_UNS) ? WAVE_UNSIGNED : 0;
    patch->len = xxs->len;
    patch->loop_start = (xxs->lps > xxs->len)? 0 : xxs->lps;
    patch->loop_end = (xxs->lpe > xxs->len)? xxs->len : xxs->lpe;

    /* New routine to minimize loop clicking */

    if ((patch->mode & WAVE_LOOPING) && !(patch->mode & WAVE_BIDIR_LOOP)) {
	if (patch->mode & WAVE_16_BITS) {
	    patch->data[patch->loop_end] = patch->data[patch->loop_start];
	    patch->data[patch->loop_end + 1] = 
		patch->data[patch->loop_start + 1];
	    patch->len += 2;
	} else {
	    patch->data[patch->loop_end] = patch->data[patch->loop_start];
	    patch->len += 1;
	}
    } else {
	if (patch->mode & WAVE_16_BITS) {
	    patch->data[patch->len] = patch->data[patch->len - 2];
	    patch->data[patch->len + 1] = patch->data[patch->len - 1];
	    patch->len += 2;
	} else {
	    patch->data[patch->len] = patch->data[patch->len - 1];
	    patch->len += 1;
	}
    }

    patch->base_note = C4_FREQ;
    patch->base_freq = basefreq;
    patch->high_note = 0x7fffffff;
    patch->low_note = 0;
    patch->volume = 120;
    patch->panning = 0;
    patch->detuning = 0;

    xxs->patch = patch;

    drv->writepatch (patch);

    ivalid[id] = 1;
}

void drv_savepatch (FILE * f, int id, int basefreq, int flags,
      struct xxm_sample *xxs, char *buffer)
{
    int i;
    uint8 *buf;
    struct patch_info *patch;

    patch = xxs->patch;

#if 0
    /* How to deal with saving FM patches ? -K. */

    /* FM patches */

    if (xxs == NULL) {
      patch = calloc (1, sizeof (struct patch_info) + 11);
      memcpy (&patch->data, buffer, 11);
      patch->len = -1;
      patch->instr_no = id;
      drv->writepatch (patch);
      free (patch);
      return;
    }
#endif

    /* Empty samples */

    if (!xxs->len)
      return;

    /* Patches with samples */

    if (opt.smp8bit && TEST_FLAG (xxs->flg, WAVE_16_BITS)) {
      /* Convert 8 bit internal to 16 bit samples */
      buf = calloc (1, xxs->len * 2);

      if (flags & XMP_SMP_NOLOAD) {
          memcpy (buf, buffer, xxs->len);
      } else

#ifdef WORDS_BIGENDIAN
      {
          int i;
          char c;

          for (i = 0; i < xxs->len; i += 2) {
              c = buf[i];
              buf[i] = buf[i + 1];
              buf[i + 1] = c;
          }
      }
#endif

      if (TEST_FLAG (flags, XMP_SMP_DIFF))
          abs2diff (xxs->len, xxs->flg & WAVE_16_BITS, buf);
      else if (TEST_FLAG (flags, XMP_SMP_8BDIFF))
          abs2diff (xxs->len, 0, buf);

      for (i = 0; i < xxs->len; i++)
          ((uint16 *)buf)[i] = patch->data[i] << 8;

      fwrite (buf, 1, xxs->len, f);
      free (buf);
    } else {
      /* Make a copy of patch data if we're going to trash it while saving */
      if (TEST_FLAG (flags, XMP_SMP_7BIT|XMP_SMP_DIFF|XMP_SMP_8BDIFF)) {
          buf = calloc(1, xxs->len);
          memcpy (buf, patch->data, xxs->len);
      } else {
          buf = patch->data;
      }

      if (flags & XMP_SMP_NOLOAD) {
          memcpy (patch->data, buffer, xxs->len);
      } else {
#if 0
          int x;
          char s[5];

          x = ftell (f);
          fread (s, 1, 5, f);
          fseek (f, x, SEEK_SET);

#ifdef ADPCM_SAMPLES
          /* Test for ADPCM encoded samples - these are usually found on
           * "MDZ" files (zipped MODs) used by the MODPlug Player.
           */
          if (!strncmp (s, "ADPCM", 5)) {
              int x2 = xxs->len / 2;
              char table[16];
              struct adpcm_state state;

              fseek (f, 5, SEEK_CUR);         /* Skip "ADPCM" */
              fread (table, 1, 16, f);
              fread (patch->data + x2, 1, x2, f);
              adpcm_decoder (patch->data + x2, patch->data, table,
                      xxs->len, &state);
          } else
#endif
#endif
          {

              if (xxs->flg & WAVE_16_BITS) {
#ifdef WORDS_BIGENDIAN
                  int i;
                  char c;

                  for (i = 0; i < xxs->len; i += 2) {
                      c = buf[i];
                      buf[i] = buf[i + 1];
                      buf[i + 1] = c;
                  }
#endif
              }
          }
      }

      if (TEST_FLAG (flags, XMP_SMP_7BIT))
          halve_sample (xxs->len, buf);
      if (TEST_FLAG (flags, XMP_SMP_DIFF))
          abs2diff (xxs->len, xxs->flg & WAVE_16_BITS, buf);
      else if (TEST_FLAG (flags, XMP_SMP_8BDIFF))
          abs2diff (xxs->len, 0, buf);

        fwrite (buf, 1, xxs->len, f);

      if(buf != (uint8*)patch->data) free(buf);
    }

}

