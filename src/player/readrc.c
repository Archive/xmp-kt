/* Extended Module Player
 * Copyright (C) 1996,1997 Claudio Matsuoka and Hipolito Carraro Jr
 * $Id$
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See doc/COPYING
 * for more information.
 */

#include "config.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "xmp.h"

static char _drv_id[32];


static void delete_spaces (char *l)
{
    char *s;

    for (s = l; *s; s++) {
	if ((*s == ' ') || (*s == '\t'))
	    memmove (s, s + 1, strlen (s));
    }
}


static int get_yesno (char *s)
{
    return !(strncmp (s, "y", 1) && strncmp (s, "o", 1));
}


int xmpi_read_rc (struct xmp_options *opt)
{
    FILE *rc;
    char *myrc, *home = getenv ("HOME");
    char *hash, *var, *val, line[256];
    char *cparm;

    myrc = malloc ((home ? strlen (home) : 0) + 16);
    sprintf (myrc, "%s/.xmprc", home);

    if ((rc = fopen (myrc, "r")) == NULL) {
	if ((rc = fopen ("/etc/xmprc", "r")) == NULL) {
	    free (myrc);
	    return -1;
	}
    }
    free (myrc);

    while (!feof (rc)) {
	memset (line, 0, 256);
	fscanf (rc, "%255[^\n]", line);
	fgetc (rc);

	/* Delete comments */
	if ((hash = strchr (line, '#')))
	    *hash = 0;

	delete_spaces (line);

	if (!(var = strtok (line, "=\n")))
	    continue;

	val = strtok (NULL, " \t\n");

#define getval_yn(x,y) { \
	if (!strcmp(var,x)) { y = get_yesno (val); continue; } }

#define getval_no(x,y) { \
	if (!strcmp(var,x)) { y = atoi (val); continue; } }

	getval_yn ("8bit", opt->smp8bit);
	getval_yn ("interpolate", opt->interpolate);
	getval_yn ("loop", opt->loop);
	getval_yn ("modrange", opt->modrange);
	getval_no ("mix", opt->mix);
	getval_no ("srate", opt->freq);
	getval_no ("time", opt->time);
	getval_no ("verbosity", opt->verbose);

	if (!strcmp (var, "driver")) {
	    strncpy (_drv_id, val, 31);
	    opt->drv_id = _drv_id;
	    continue;
	}
	if (!strcmp (var, "bits")) {
	    opt->res8bit = (atoi (val) == 8);
	    continue;
	}
	if (!strcmp (var, "reverse")) {
	    if (get_yesno (val))
		opt->reverse = -1;
	    continue;
	}
	if (!strcmp (var, "pan")) {
	    opt->nopan = !get_yesno (val);
	    continue;
	}
	if (!strcmp (var, "period_mode")) {
	    if (!strcmp (val, "amiga"))
		opt->linear = 0;
	    else if (!strcmp (val, "linear"))
		opt->linear = 1;
	    continue;
	}
	if (!strcmp (var, "mono")) {
	    opt->reverse *= !get_yesno (val);
	    continue;
	}

	/* If the line does not match any of the previous parameter,
	 * send it to the device driver
	 */
	cparm = calloc (1, strlen (var) + strlen (val) + 2);
	sprintf (cparm, "%s=%s", var, val);
	xmp_set_driver_parameter (cparm);
    }

    fclose (rc);

    return 0;
}
