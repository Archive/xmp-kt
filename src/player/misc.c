/* Extended Module Player
 * Copyright (C) 1996-1998 Claudio Matsuoka and Hipolito Carraro Jr
 * $Id$
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See doc/COPYING
 * for more information.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <ctype.h>
#include <string.h>

#ifndef HAVE_BROKEN_STDARG
#include <stdarg.h>
#endif

#include "xmpi.h"


/* This conditional looks pretty weird, but the Sun machine I was using 
 * for the tests (barracuda.inf.ufpr.br) actually has a "broken stdarg".
 */
#ifndef HAVE_BROKEN_STDARG

int report (char *fmt,...)
{
    va_list a;
    int n;

    va_start (a, fmt);
    n = vfprintf (stderr, fmt, a);
    va_end (a);

    return n;
}

#endif


char *str_adj (char *s)
{
    int i;

    for (i = 0; i < strlen (s); i++)
	if (!isprint (s[i]) || ((uint8) s[i] > 127))
	    s[i] = ' ';

    while (*s && (s[strlen (s) - 1] == ' '))
	s[strlen (s) - 1] = 0;

    return s;
}


