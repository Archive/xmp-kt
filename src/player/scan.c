/* Extended Module Player
 * Copyright (C) 1996-1998 Claudio Matsuoka and Hipolito Carraro Jr
 * $Id$
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See doc/COPYING
 * for more information.
 */

/*
 * Sun, 31 May 1998 17:50:02 -0600
 * Reported by ToyKeeper <scriven@CS.ColoState.EDU>:
 * For loop-prevention, I know a way to do it which lets most songs play
 * fine once through even if they have backward-jumps. Just keep a small
 * array (256 bytes, or even bits) of flags, each entry determining if a
 * pattern in the song order has been played. If you get to an entry which
 * is already non-zero, skip to the next song (assuming looping is off).
 */

/*
 * Tue, 6 Oct 1998 21:23:17 +0200 (CEST)
 * Reported by John v/d Kamp <blade_@dds.nl>:
 * scan.c was hanging when it jumps to an invalid restart value.
 * (Fixed by hipolito)
 */


#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdlib.h>
#include "xmpi.h"
#include "xxm.h"
#include "effects.h"

#define TRUE		1
#define CLEAR		0
#define NO_JUMP		-1
#define S3M_END		0xff
#define MAX_CH		1024
#define DEFAULT_TIME	6

extern struct xmp_ord_info xmpi_oinfo[256];
extern int xxh_row, xxh_ord, xxh_num;

static int ord;
static int bpm;
static int tempo;
static int jmp_ord;
static int first_row;
static int base_time;
static char **tab_cnt;


static int
scan_pattern (int row, int last_row, int last_chn, int num)
{
    int f1, f2, p, chn;
    int get_out, cnt_row, loop, clock;
    struct xxm_event *event;

    get_out = cnt_row = loop = clock = CLEAR;
    for (first_row = CLEAR; !get_out && row <= last_row; row++, cnt_row++) {
	if (num)
	    tab_cnt[ord][row] += num;
	else {
	    if (tab_cnt[ord][row] != 0) {
		jmp_ord = ord;
		first_row = row;
		break;
	    }
	    ++tab_cnt[ord][row];
	}

	for (chn = CLEAR; chn < xxh->chn; chn++) {
	    event = &EVENT (xxo[ord], chn, row);

	    f1 = event->fxt;
	    f2 = event->f2t;

	    if (f1 == FX_TEMPO || f2 == FX_TEMPO) {
		clock += 1000 * cnt_row * tempo * base_time / bpm;
		p = (f1 == FX_TEMPO) ? event->fxp: event->f2p;
		if (p >= 0x20)
		    bpm = p;
		else
		    tempo = p;
		cnt_row = CLEAR;
	    }
	    if (f1 == FX_S3M_TEMPO || f2 == FX_S3M_TEMPO) {
		clock += 1000 * cnt_row * tempo * base_time / bpm;
		p = (f1 == FX_S3M_TEMPO) ? event->fxp: event->f2p;
		tempo = p;
		cnt_row = CLEAR;
	    }
	    if (f1 == FX_JUMP || f2 == FX_JUMP) {
		jmp_ord = (f1 == FX_JUMP) ? event->fxp: event->f2p;
		get_out = TRUE;
	    }
	    if (f1 == FX_BREAK || f2 == FX_BREAK) {
		p = (f1 == FX_BREAK) ? event->fxp: event->f2p;
		first_row = 10 * MSN (p) + LSN (p);	/* Ugh! */
		get_out = TRUE;
	    }
	    if (f1 == FX_EXTENDED || f2 == FX_EXTENDED) {
		p = (f1 == FX_EXTENDED)? event->fxp: event->f2p;

		if ((p >> 4) == EX_F_PATT_DELAY)
		    clock += 1000 * (p & 0x0f) * tempo * base_time / bpm;

		if (row == last_row && chn >= last_chn)
		    continue;

		if ((p >> 4) == EX_PATTERN_LOOP) {
		    if (p &= 0x0f)
			clock += scan_pattern (loop, row, chn, p) * p;
		    else 
			loop = row;
		}
	    }
	}
    }

    return clock += 1000 * cnt_row * tempo * base_time / bpm;
}


int xmpi_scan_module ()
{
    int clock_rst, clock;

    base_time = opt.rrate;

    tab_cnt = calloc (sizeof (char *), xxh->len);
    for (ord = xxh->len; ord--;)
	tab_cnt[ord] =
	    calloc (1, xxo[ord] >= xxh->pat ? 1 : xxp[xxo[ord]]->rows);

    jmp_ord = ord = NO_JUMP;
    first_row = clock_rst = clock = CLEAR;
    tempo = (tempo = opt.tempo ? opt.tempo : xxh->tpo) ? tempo : DEFAULT_TIME;
    bpm = xxh->bpm;

    while (TRUE) {
	if (jmp_ord >= 0 && jmp_ord < xxh->len) {
	    ord = jmp_ord;
	    jmp_ord = NO_JUMP;
	    if (tab_cnt[ord][first_row] != 0)
		break;				/* End of module */
	} else 
	    ++ord;

	if (ord >= xxh->len) {
	    jmp_ord = (xxo[xxh->rst] >= xxh->pat) ? CLEAR : xxh->rst;
	    continue;
	}

	if (xxo[ord] >= xxh->pat) {
	    if (xxo[ord] == S3M_END) {		/* Ugh! */
		++tab_cnt[ord][first_row];
		break;
	    }
	    continue;
	}

	xmpi_oinfo[ord].bpm = bpm;
	xmpi_oinfo[ord].tempo = tempo;
	xmpi_oinfo[ord].time = clock / 100;
	xmpi_oinfo[ord].gvl = 64;		/* gvl is always 64 (?!?) */

	clock += scan_pattern (first_row, xxp[xxo[ord]]->rows - 1, MAX_CH, CLEAR);

	if (ord < opt.start && !opt.loop)	/* HIP: I don't think it's */
	    clock_rst = clock;			/* correct, but anyway... */
    }
    xxh_num = opt.start > ord? CLEAR: tab_cnt[ord][first_row];
    xxh_row = first_row;
    xxh_ord = ord;

    for (ord = xxh->len; ord--; free (tab_cnt[ord]));
    free (tab_cnt);

    return (clock - clock_rst) / 100;
}

