/* Extended Module Player
 * Copyright (C) 1996-1998 Claudio Matsuoka and Hipolito Carraro Jr
 * $Id$
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See doc/COPYING
 * for more information.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "xmpi.h"


/* Convert differential to absolute sample data */
void diff2abs (int l, int r, char *p)
{
    uint16 new, old = 0;
    uint16 *w = (uint16 *) p;

    if (r) {
	for (l >>= 1; l--;) {
	    new = *w + old;
	    *w++ = new;
	    old = new;
	}
    } else {
	for (; l--;) {
	    new = *p + old;
	    *p++ = (uint8) new;
	    old = (uint8) new;
	}
    }
}

/* Convert absolute to differential sample data */
void abs2diff (int l, int r, char *p)
{
    uint16 new, old = 0;
    uint16 *w = (uint16 *) p;

    if (r) {
      for (l >>= 1; l--;) {
          new = *w - old;
          old = *w;
          *w++ = new;
      }
    } else {
      for (; l--;) {
          new = *p - old;
          old = (uint8) *p;
          *p++ = (uint8) new;
      }
    }
}


/* Convert 7 bit samples to 8 bit */
void double_sample (int len, char *buf)
{
    for (; len--; *buf++ <<= 1);
}

/* Convert 8 bit samples to 7 bit */
void halve_sample (int len, char *buf)
{
    for (; len--; *buf++ >>= 1);
}


/* Convert signed to unsigned sample data */
void sig2uns (int l, int r, char *p)
{
    uint16 *w = (uint16 *) p;

    if (r) {
	for (l >>= 2; l--;) {
	    *w += 0x8000;
	    w++;
	}
    } else {
	for (; l--;) {
	    *p += 0x80;
	    p++;
	}
    }
}


/* Convert HSC OPL2 instrument data to SBI instrument data */
void hscins_to_sbi (char *a)
{
    char b[11];
    int i;

    for (i = 0; i < 10; i += 2)
	FIX_ENDIANISM_16 (*(uint16 *) & a[i]);

    memcpy (b, a, 11);
    a[8] = b[10];
    a[10] = b[9];
    a[9] = b[8];
}


/* Convert little-endian 16-bit samples to big-endian */
void change_sex (int l, char *p)
{
    uint8 b;

    for (l >>= 2; l--;) {
	b = *p;
	*p++ = *p;
	*p++ = b;
    }
}

