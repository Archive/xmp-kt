/* Extended Module Player
 * Copyright (C) 1996-1998 Claudio Matsuoka and Hipolito Carraro Jr
 * $Id$
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See doc/COPYING
 * for more information.
 */

#include "config.h"

#include <stdlib.h>
#include "xmpi.h"
#include "formats.h"

struct xmp_fmt_info *__fmt_head;


static void register_fmt (char *suffix, char *tracker, int (*loader) (), int (*saver)())
{
    struct xmp_fmt_info *f;

    f = malloc (sizeof (struct xmp_fmt_info));
    f->tracker = tracker;
    f->suffix = suffix;
    f->loader = loader;
    f->saver = saver;

    if (!__fmt_head)
	__fmt_head = f;
    else {
	struct xmp_fmt_info *i;

	for (i = __fmt_head; i->next; i = i->next);
	i->next = f;
    }

    f->next = NULL;
}


void fmt_register ()
{
    register_fmt ("XM", "Fast Tracker II", xm_load, xm_save);
    register_fmt ("MOD", "Noise/Fast/Protracker", mod_load, mod_save);
    register_fmt ("S3M", "Scream Tracker 3", s3m_load, NULL);
    register_fmt ("STM", "Scream Tracker 2", stm_load, NULL);
    register_fmt ("MTM", "Multitracker", mtm_load, NULL);
    register_fmt ("ULT", "Ultra Tracker", ult_load, NULL);
    register_fmt ("PTM", "Poly Tracker", ptm_load, NULL);
    register_fmt ("OKT", "Oktalyzer", okt_load, NULL);
    register_fmt ("FAR", "Farandole Composer", far_load, NULL);
    register_fmt ("WOW", "Mod's Grave", NULL, NULL);
    register_fmt ("FC-M", "FC-M Packer", fcm_load, NULL);
    register_fmt ("KRIS", "Kris Tracker", kris_load, NULL);
    register_fmt ("UNIC", "Unic Tracker", unic_load, NULL);
    register_fmt ("LAX", "Laxity Tracker", lax_load, NULL);
    register_fmt ("PRU1", "ProRunner 1.0", pru1_load, NULL);
    register_fmt ("PRU2", "ProRunner 2.0", pru2_load, NULL);
    register_fmt ("AC1D", "AC1D Packer", ac1d_load, NULL);
    register_fmt ("PP10", "Pha Packer/Propacker 1.0", pp10_load, NULL);
    register_fmt ("XANN", "XANN Packer", xann_load, NULL);
    register_fmt ("669", "Composer 669", ssn_load, NULL);
#ifdef DRIVER_OSS_SEQ
    register_fmt ("AMD", "Amusic Adlib Tracker", amd_load, NULL);
    register_fmt ("RAD", "Reality Adlib Tracker", rad_load, NULL);
    /* register_fmt ("HSC", "HSC Adlib Tracker", hsc_load, NULL); */
#endif
    /* register_fmt ("FNK", "Funktracker", fnk_load, NULL); */
    register_fmt ("ALM", "Aley's modules", alm_load, NULL);
    register_fmt ("MOD", "Soundtracker 2.2", m15_load, NULL);
}
