/* Extended Module Player
 * Copyright (C) 1996,1997 Claudio Matsuoka and Hipolito Carraro Jr
 * $Id$
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See doc/COPYING
 * for more information.
 */

#ifndef __PLAYER_H
#define __PLAYER_H

#define SET(f)	SET_FLAG (xc->flags,f)
#define RESET(f) RESET_FLAG (xc->flags,f)
#define TEST(f)	TEST_FLAG (xc->flags,f)

/* t = envelope flags		i = index
 * e = envelope points		s = sustain point
 * a = loop start point		b = loop end point
 */
#define DO_ENVELOPE(t,i,e,s,a,b) { if ((!(t & XXM_ENV_SUS) || xc->release \
    || (i < e[s * 2])) && (i < 0xffff)) i++; if ((t & XXM_ENV_LOOP) \
    && (i >= e[b * 2])) i = e[a * 2]; }

/* Pattern loop stack size */
#define ROW_MAX		0x0100

/* Global flags */
#define PATTERN_BREAK	0x0001 
#define PATTERN_LOOP	0x0002 
#define MODULE_ABORT	0x0004 
#define MODULE_RESTART	0x0008 

#if 0
#define CLEAR_CHANNEL(x) { \
    memset ((uint8 *) &xc[x] + 2 * sizeof (int) + sizeof (void *), 0, \
    sizeof (xc[x]) - 2 * sizeof (int) - sizeof (void *)); \
    xc[x].ins = -1; }
#endif

/* ????? */

#define CLEAR_CHANNEL(x) { \
    memset (&xc[x], 0, sizeof (xc[x])); \
    xc[x].ins = -1; xc[x].masterpan = xxc[x].pan; }

#define XXIH xxih[xc->ins]
#define XXIM xxim[xc->ins]
#define XXAE xxae[xc->ins]
#define XXPE xxpe[xc->ins]
#define XXFE xxfe[xc->ins]
#define XXI xxi[xc->ins]

struct retrig_t {
    int s;
    int m;
    int d;
};

struct flow_control {
    int pbreak;
    int loop;
    int jump;
    int delay;
    int jumpline;
    int row_cnt;
    int loop_row;
    int loop_cnt;
    uint8 *row_ptr;
    uint8 *loop_ptr;
    uint8 *loop_stack;
};

/* The following macros are used to set the flags for each channel */
#define VOL_SLIDE	0x0001
#define PAN_SLIDE	0x0002
#define TONEPORTA	0x0004
#define PITCHBEND	0x0008
#define VIBRATO		0x0010
#define TREMOLO		0x0020
#define FINE_VOLS	0x0040
#define FINE_BEND	0x0080
/* These don't need to be "persistent" between frames */
#define NEW_NOTE	0x0100
#define NEW_VOL		0x0200
#define NEW_PAN		0x0400
#define NEW_INS		0x0800
#define FINETUNE	0x1000

/* Prefixes: 
 * a_ for arpeggio
 * v_ for volume slide
 * p_ for pan
 * f_ for frequency (period) slide
 * y_ for vibrato (v is already being used by volume)
 * s_ for slide to note (tone portamento)
 * t_ for tremolo
 */

struct xmp_channel {
    int flags;			/* Channel flags */
    uint8 note;			/* Note number */
    uint8 key;			/* Key number */
    int period;			/* Amiga or linear period */
    int pitchbend;		/* Pitch bender value */
    int finetune;		/* Guess what */
    int ins;			/* Instrument number */
    int smp;			/* Sample number */
    int oldins;			/* Previous instrument number */
    int pan;			/* Current pan */
    int masterpan;		/* Master pan -- for S3M set pan effect */
    int delay;			/* Note delay in frames */
    int retrig;			/* Retrig delay in frames */
    int rval;			/* Retrig parameters */
    int rtype;			/* Retrig type */
    int rcount;			/* Retrig counter */
    int tremor;			/* Tremor */
    int tcnt_up;		/* Tremor counter (up cycle) */
    int tcnt_dn;		/* Tremor counter (down cycle) */
    int keyoff;			/* Key off */
    int kcount;			/* Key off counter */
    int volume;			/* Current volume */
    int v_val;			/* Volume slide value */
    int v_fval;			/* Fine volume slide value */
    int p_val;			/* Current pan value */
    uint16 v_idx;		/* Volume envelope index */
    int y_type;			/* Vibrato waveform */
    int y_depth;		/* Vibrato depth */
    int y_sweep;		/* Vibrato sweep */
    int y_rate;			/* Vibrato rate */
    int y_idx;			/* Vibrato index */
    int t_type;			/* Tremolo waveform */
    int t_depth;		/* Tremolo depth */
    int t_rate;			/* Tremolo rate */
    int t_idx;			/* Tremolo index */
    int f_val;			/* Frequency slide value */
    int f_fval;			/* Fine frequency slide value */
    uint16 p_idx;		/* Pan envelope index */
    int release;		/* Note released */
    int fadeout;		/* Current fadeout (release) value */
    int gliss;			/* Glissando active */
    int s_end;			/* Target period for tone portamento */
    int s_sgn;			/* Tone portamento up/down switch */
    int s_val;			/* Delta for tone portamento */
    int a_val[3];		/* Arpeggio relative notes */
    int a_idx;			/* Arpeggio index */
    int insvib_idx;		/* Instrument vibrato index */
    int insvib_swp;		/* Instrument vibrato sweep */
    int offset;			/* Sample offset */
};

void process_fx (uint8, uint8, uint8, struct xmp_channel *);

#endif /* __PLAYER_H */
