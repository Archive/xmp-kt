/* Extended Module Player
 * Copyright (C) 1996-1998 Claudio Matsuoka and Hipolito Carraro Jr
 * $Id$.
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See doc/COPYING
 * for more information.
 */

#define NOT_IMPLEMENTED


/* Values for multi-retrig */
static struct retrig_t rval[] = {
    {   0,  1,  1 }, {  -1,  1,  1 }, {  -2,  1,  1 }, {  -4,  1,  1 },
    {  -8,  1,  1 }, { -16,  1,  1 }, {   0,  2,  3 }, {   0,  1,  2 },
    {   0,  1,  1 }, {   1,  1,  1 }, {   2,  1,  1 }, {   4,  1,  1 },
    {   8,  1,  1 }, {  16,  1,  1 }, {   0,  3,  2 }, {   0,  2,  1 },
    {   0,  0,  1 }	/* Note cut */
};
    

void process_fx (uint8 note, uint8 fxt, uint8 fxp, struct xmp_channel *xc)
{
    switch (fxt) {
    case FX_ARPEGGIO:
	if (!fxp)
	    break;
	xc->a_val[1] = 100 * MSN (fxp);
	xc->a_val[2] = 100 * LSN (fxp);
	break;
    case FX_PORTA_UP:				/* Portamento up */
	SET (PITCHBEND);
	if (fxp)
	    xc->f_val = -fxp;
	else if (xc->f_val > 0)
	    xc->f_val *= -1;
	break;
    case FX_PORTA_DN:				/* Portamento down */
	SET (PITCHBEND);
	if (fxp)
	    xc->f_val = fxp;
	else if (xc->f_val < 0)
	    xc->f_val *= -1;
	break;
    case FX_TONE_VSLIDE:			/* Toneporta + vol slide */
	SET (VOL_SLIDE);
	if (fxp)
	    xc->v_val = MSN (fxp) - LSN (fxp);
	fxp = 0;
    case FX_TONEPORTA:				/* Tone portamento */
	if (TEST (NEW_NOTE)) {
	    xc->s_end = note_to_period (
		note + XXI[XXIM.ins[note]].xpo,
		XXI[XXIM.ins[note]].fin,
		xxh->flg & XXM_FLG_LINEAR);
	    RESET (NEW_NOTE);
	}
	xc->s_sgn = xc->period < xc->s_end ? 1 : -1;
	if (fxp)
	    xc->s_val = fxp;
	if (xc->s_end != -1)
	    SET (TONEPORTA);
	break;
    case FX_VIBRA_VSLIDE:			/* Vibrato + vol slide */
	SET (VOL_SLIDE);
	if (fxp)
	    xc->v_val = MSN (fxp) - LSN (fxp);
	fxp = 0;
    case FX_VIBRATO:				/* Vibrato */
	SET (VIBRATO);
	if (LSN (fxp))
	    xc->y_depth = LSN (fxp);
	if (MSN (fxp))
	    xc->y_rate = MSN (fxp);
	break;
    case FX_TREMOLO:				/* Tremolo */
	SET (TREMOLO);
	if (MSN (fxp))
	    xc->t_rate = MSN (fxp);
	if (LSN (fxp))
	    xc->t_depth = LSN (fxp);
	break;
    case FX_SETPAN:				/* Set pan */
	SET (NEW_PAN);
	xc->pan = fxp;
	break;
    case FX_OFFSET:				/* Set sample offset */
	if (fxp)
	    xc->offset = offset = fxp * 256;
	else
	    offset = xc->offset;
	break;
    case FX_VOLSLIDE:				/* Volume slide */
	SET (VOL_SLIDE);
	if (fxp)
	    xc->v_val = MSN (fxp) - LSN (fxp);
	break;
    case FX_JUMP:				/* Order jump */
	flow.pbreak = 1;
	flow.jump = fxp;
	break;
    case FX_VOLSET:				/* Volume set */
	SET (NEW_VOL);
	xc->volume = fxp;
	break;
    case FX_BREAK:				/* Pattern break */
	flow.pbreak = 1;
	flow.jumpline = 10 * MSN (fxp) + LSN (fxp);
	break;
    case FX_EXTENDED:				/* Extended effect */
	fxt = fxp >> 4;
	fxp &= 0x0f;
	switch (fxt) {
	case EX_F_PORTA_UP:			/* Fine portamento up */
	    SET (FINE_BEND);
	    if (fxp)
		xc->f_fval = -fxp * 4;
	    else if (xc->f_val > 0)
		xc->f_val *= -1;
	    break;
	case EX_F_PORTA_DN:			/* Fine portamento down */
	    SET (FINE_BEND);
	    if (fxp)
		xc->f_fval = fxp * 4;
	    else if (xc->f_val < 0)
		xc->f_val *= -1;
	    break;
	case EX_GLISS:				/* Glissando toggle */
	    xc->gliss = fxp;
	    break;
	case EX_VIBRATO_WF:			/* Set vibrato waveform */
	    xc->y_type = fxp & 3;
	    break;
	case EX_FINETUNE:			/* Set finetune */
	    SET (FINETUNE);
	    xc->finetune = fxp;
	    break;
	case EX_PATTERN_LOOP:			/* Loop pattern */
	    if (fxp) {
		if (!flow.loop) {
		    if (flow.loop_stack[flow.loop_cnt]) {
			if (--flow.loop_stack[flow.loop_cnt])
			    flow.loop = 1;
		    } else {
			flow.loop_stack[flow.loop_cnt] = fxp;
			flow.loop = 1;
		    }
		}
		flow.loop_cnt++;
	    } else if (!(flow.loop && flow.loop_cnt)) {
		flow.loop = 0;
		flow.loop_ptr = flow.row_ptr;
		flow.loop_row = flow.row_cnt;
		flow.loop_cnt = 0;
	    }
	    break;
	case EX_TREMOLO_WF:			/* Set tremolo waveform */
	    xc->t_type = fxp & 3;
	    break;
	case EX_RETRIG:				/* Retrig note */
	    xc->retrig = xc->rcount = fxp ? fxp + 1 : xc->rval;
	    xc->rval = xc->retrig;
	    xc->rtype = 0;
	    break;
	case EX_F_VSLIDE_UP:			/* Fine volume slide up */
	    SET (FINE_VOLS);
	    if (fxp)
		xc->v_fval = fxp;
	    break;
	case EX_F_VSLIDE_DN:			/* Fine volume slide down */
	    SET (FINE_VOLS);
	    if (fxp)
		xc->v_fval = -fxp;
	    break;
	case EX_F_CUT:				/* Cut note */
	    xc->retrig = xc->rcount = fxp + 1;
	    xc->rtype = 0x10;
	    break;
	case EX_F_DELAY:			/* Note delay */
	    xc->delay = fxp + 1;
	    break;
	case EX_F_PATT_DELAY:			/* Pattern delay */
	    flow.delay = fxp;
	    break;
	}
	break;
    case FX_TEMPO:				/* Set tempo */
	if (fxp) {
	    if (fxp < 0x20) {
		tempo = fxp;
	    } else {
		tick_duration = opt.rrate / (__bpm = fxp);
	    }
	}
	break;
    case FX_S3M_TEMPO:
	tempo = fxp;
	break;
    case FX_GLOBALVOL:				/* Set global volume */
	__global_vol = fxp;
	if (__global_vol > 0x40)
	    __global_vol = 0x40;
	break;
    case FX_G_VOLSLIDE:				/* Global volume slide */
	g_vslide = 1;
	if (fxp)
	    global_vslide = MSN (fxp) - LSN (fxp);
	break;
    case FX_KEYOFF:				/* Key off */
	xc->keyoff = 1;
	xc->kcount = fxp;
	break;
    case FX_ENVPOS:				/* Set envelope position */
	NOT_IMPLEMENTED;
	break;
    case FX_MASTER_PAN:				/* Set master pan */
	xc->masterpan = fxp;
	break;
    case FX_PANSLIDE:				/* Pan slide */
	SET (PAN_SLIDE);
	if (fxp)
	    xc->p_val = MSN (fxp) - LSN (fxp);
	break;
    case FX_MULTI_RETRIG:			/* Multi retrig */
	xc->retrig = xc->rcount = LSN (fxp) ? LSN (fxp) + 1 : xc->rval;
	xc->rval = xc->retrig;
	xc->rtype = MSN (fxp);
	break;
    case FX_TREMOR:				/* Tremor */
	xc->tremor = fxp;
	xc->tcnt_up = MSN (fxp);
	xc->tcnt_dn = -1;
	break;
    case FX_XF_PORTA:				/* Extra fine portamento */
	SET (FINE_BEND);
	switch (MSN (fxp)) {
	case 1:
	    xc->f_fval = -LSN (fxp);
	    break;
	case 2:
	    xc->f_fval = LSN (fxp);
	    break;
	}
	break;
#if 0
    case FX_TRK_VOL:				/* Track volume setting */
	if (fxp <= 0x40)
	    xc->mastervol = fxp;
	break;
    case FX_TRK_VSLIDE:				/* Track volume slide */
	SET (TRK_VSLIDE);
	if (fxp)
	    xc->trk_val = MSN (fxp) - LSN (fxp);
	break;
    case FX_TRK_FVSLIDE:			/* Track fine volume slide */
	SET (TRK_FVSLIDE);
	if (fxp)
	    xc->trk_fval = MSN (fxp) - LSN (fxp);
	break;
#endif
    }
}
