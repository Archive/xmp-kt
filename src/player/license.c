/* Extended Module Player
 * Copyright (C) 1996-1998 Claudio Matsuoka and Hipolito Carraro Jr
 * $Id$
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See doc/COPYING
 * for more information.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "xmpi.h"


void xmpi_display_license ()
{
    printf ("\nExtended Module Player %s %s\n%s\n%s\n",
	__xmp_version, __xmp_date, __xmp_copyright,
"AWE-specific portions of oss_seq Copyright (C) 1996,1997 by Takashi Iwai.\n"
"ulaw encoder Copyright (C) 1989 by Rich Gopstein and Harris Corporation.\n"
"ADPCM decoder Copyright (C) 1992 Stichting Mathematisch Centrum, Amsterdam,\n"
"    The Netherlands.\n"
"Powerpacker decruncher Copyright (C) 1995 Marc Espie.\n"
"XPK-SQSH decruncher Copyright (C) 1997 Bert Jahn.\n"

"\nThis program is free software; you can redistribute it and/or modify it "
"under\nthe terms of the GNU General Public License as published by the Free "
"Software \nFoundation; either version 2, or (at your option) any later "
"version.\n\n"
"This program is distributed in the hope that it will be useful, but WITHOUT\n"
"ANY WARRANTY; without even the implied warranty of MERCHANTABILITY OR FITNESS\n"
"FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details."
"\n\n"
"You should have received a copy of the GNU General Public License along with\n"
"with this program; if not, write to the Free Software Foundation, Inc.,\n"
"675 Mass Ave, Cambridge, MA 02139, USA.\n"
    );
}

