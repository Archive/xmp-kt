/* Extended Module Player
 * Copyright (C) 1997 Claudio Matsuoka and Hipolito Carraro Jr
 * $Id$
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See doc/COPYING
 * for more information.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "xmpi.h"
#include "iff.h"

static struct iff_info *iff_head;
static __id_size;
static __little_endian;


void iff_chunk (FILE * f)
{
    long size;
    char id[17];

    fread (id, 1, __id_size, f);
    fread (&size, 1, 4, f);
    if (__little_endian == IFF_LITTLE_ENDIAN)
	L_ENDIAN32 (size);
    else
	B_ENDIAN32 (size);
    iff_process (id, size, f);
}


void iff_register (char *id, void (*loader) ())
{
    struct iff_info *f;

    __id_size = 4;
    __little_endian = 0;
    f = malloc (sizeof (struct iff_info));
    strcpy (f->id, id);
    f->loader = loader;
    if (!iff_head)
	iff_head = f;
    else {
	struct iff_info *i;
	for (i = iff_head; i->next; i = i->next);
	i->next = f;
    }
    f->next = NULL;
}


int iff_process (char *id, long size, FILE * f)
{
    char *buffer;
    struct iff_info *i;

    if ((buffer = malloc (size)) == NULL)
	return -1;
    fread (buffer, 1, size, f);
    for (i = iff_head; i; i = i->next)
	if (!strncmp (id, i->id, __id_size))
	    i->loader (size, buffer);
    free (buffer);
    return 0;
}


/* This function should be used for degenerated IFFoid files */
void iff_distort (int n, int lend)
{
    __id_size = n;
    __little_endian = lend;
}
