/* Extended Module Player
 * Copyright (C) 1997 Claudio Matsuoka and Hipolito Carraro Jr
 * $Id$
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See doc/COPYING
 * for more information.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdlib.h>
#include <string.h>

#include "xmpi.h"
#include "driver.h"
#include "mixer.h"
#include "period.h"

#define BPM_MIN		32
#define C4_NOTE		6947

struct channel_info {
    int note;
    int pan;
    int vol;
    int period;		/* Current period */
    int pbase;		/* Base period */
    int itpt;		/* Interpolation */
    int pos;		/* Position in sample */
    int fidx;		/* Function index */
    int fxor;		/* Function xor control */ 
    int smp;		/* Sample number */
    int end;		/* Loop end */
    int freq;
    int shl8;
    void* sptr;		/* Sample pointer */
};

#define VOL_FT		3
#define SHIFT_FT	16
#define AND_FT		0xffff

#define FLAG_ITPT	0x01
#define FLAG_16BIT	0x02
#define FLAG_STEREO	0x04
#define FLAG_BACKWARD	0x08
#define FLAG_ACTIVE	0x10
#define FIDX_FLAGMASK	(FLAG_ITPT | FLAG_16BIT | FLAG_STEREO)

#define LIM_FT		12
#define LIM8_HI		127
#define LIM8_LO		-127
#define LIM12_HI	4095
#define LIM12_LO	-4096
#define LIM16_HI	32767
#define LIM16_LO	-32768

#define PATCH_OK	-1

static struct patch_info	**Patch_info_array;
static struct channel_info	*Channel_info_array;

static int* Tmp_bank;			/* Temp buffer for 32 bit samples */
static int* Tmp_bk;			/* Same thing */
static char** Out_array;		/* Array of output buffers */
static int16 Out_mask;			/* For signal conversion */
static int   Out_cur;			/* Current output buffer */
static int Ticksize;			/* Number of elements per tick */
static int Itpt_inc;			/* Interpolator	increment */
static int Vol_right;			/* Right channel volume */
static int Vol_left;			/* Left channel/mono volume */

struct drv_config *Cfg;

extern int __bpm;

static void	out_mn8norm	(int, char*, int, int);
static void	out_mn8itpt	(int, char*, int, int);
static void	out_mn16norm	(int, int16*, int, int);
static void	out_mn16itpt	(int, int16*, int, int);
static void	out_st8norm	(int, char*, int, int);
static void	out_st8itpt	(int, char*, int, int);
static void	out_st16norm	(int, int16*, int, int);
static void	out_st16itpt	(int, int16*, int, int);

/* Array index:
 *
 * bit 0: 0=noninterpolated, 1=interpolated
 * bit 1: 0=8 bit, 1=16 bit
 * bit 2: 0=mono, 1=stereo
 */

static void (*out_fn[])() = {
    out_mn8norm,
    out_mn8itpt,
    out_mn16norm,
    out_mn16itpt,
    out_st8norm,
    out_st8itpt,
    out_st16norm,
    out_st16itpt
};


static int __echo_msg;
char *mixer_buffer;


/* Mixers
 *
 * To increase performance eight mixers are defined, one for each
 * combination of the following parameters: interpolation, resolution
 * and number of channels.
 */
#define INTERPOLATE() \
    if (itpt >> SHIFT_FT) { \
	cur_bk += itpt >> SHIFT_FT; \
	smp_x1 = in_bk[cur_bk]; \
	smp_dt = in_bk[cur_bk + 1] - smp_x1; \
	itpt &= AND_FT; \
    } \
    smp_in = smp_x1 + ((itpt * smp_dt) >> SHIFT_FT);

#define DONT_INTERPOLATE() \
    smp_in = in_bk[itpt >> SHIFT_FT];


/* Handler for 8 bit samples, interpolated stereo output */
static void out_st8itpt (int count, char *in_bk, int cur_bk, register int itpt)
{
    int vl, vr;
    int smp_x1 = 0, smp_dt = 0;
    register int smp_in;
    register int *tmp_bk = Tmp_bk;
    int itpt_inc = Itpt_inc;

    vl = Vol_left << 8;
    vr = Vol_right << 8;
    while (count--) {
	INTERPOLATE ();
	*(tmp_bk++) += smp_in * vr;
	*(tmp_bk++) += smp_in * vl;
	itpt += itpt_inc;
    }
}


/* Handler for 16 bit samples, interpolated stereo output */
static void out_st16itpt (int count, int16 *in_bk, int cur_bk, register int itpt)
{
    int vl, vr;
    int smp_x1 = 0, smp_dt = 0;
    register int smp_in;
    register int *tmp_bk = Tmp_bk;
    int itpt_inc = Itpt_inc;

    vl = Vol_left;
    vr = Vol_right;
    while (count--) {
	INTERPOLATE ();
	*(tmp_bk++) += smp_in * vr;
	*(tmp_bk++) += smp_in * vl;
	itpt += itpt_inc;
    }
}


/* Handler for 8 bit samples, non-interpolated stereo output */
static void out_st8norm (int count, char *in_bk, int cur_bk, register int itpt)
{
    int vl, vr;
    register int smp_in;
    register int *tmp_bk = Tmp_bk;
    int itpt_inc = Itpt_inc;

    in_bk += cur_bk;
    vl = Vol_left << 8;
    vr = Vol_right << 8;
    while (count--) {
	DONT_INTERPOLATE ();
	*(tmp_bk++) += smp_in * vr;
	*(tmp_bk++) += smp_in * vl;
	itpt += itpt_inc;
    }
}


/* Handler for 16 bit samples, non-interpolated stereo output */
static void out_st16norm (int count, int16 *in_bk, int cur_bk, register int itpt)
{
    int vl, vr;
    register int smp_in;
    register int *tmp_bk = Tmp_bk;
    int itpt_inc = Itpt_inc;

    in_bk += cur_bk;
    vl = Vol_left;
    vr = Vol_right;
    while (count--) {
	DONT_INTERPOLATE ();
	*(tmp_bk++) += smp_in * vr;
	*(tmp_bk++) += smp_in * vl;
	itpt += itpt_inc;
    }
}


/* Handler for 8 bit samples, interpolated mono output */
static void out_mn8itpt (int count, char *in_bk, int cur_bk, register int itpt)
{
    int smp_x1 = 0, smp_dt = 0, vl;
    register int smp_in;
    register int *tmp_bk = Tmp_bk;
    int itpt_inc = Itpt_inc;

    vl = Vol_left << 9;
    while (count--) {
	INTERPOLATE ();
	*(tmp_bk++) += smp_in * vl;
	itpt += itpt_inc;
    }
}


/* Handler for 16 bit samples, interpolated mono output */
static void out_mn16itpt (int count, int16 *in_bk, int cur_bk, register int itpt)
{
    int smp_x1 = 0, smp_dt = 0, vl;
    register int smp_in;
    register int *tmp_bk = Tmp_bk;
    int itpt_inc = Itpt_inc;

    vl = Vol_left << 1;
    while (count--) {
	INTERPOLATE ();
	*(tmp_bk++) += smp_in * vl;
	itpt += itpt_inc;
    }
}


/* Handler for 8 bit samples, non-interpolated mono output */
static void out_mn8norm (int count, char *in_bk, int cur_bk, register int itpt)
{
    int vl;
    register int smp_in;
    register int *tmp_bk = Tmp_bk;
    int itpt_inc = Itpt_inc;

    in_bk += cur_bk;
    vl = Vol_left << 9;
    while (count--) {
	DONT_INTERPOLATE ();
	*(tmp_bk++) += smp_in * vl;
	itpt += itpt_inc;
    }
}


/* Handler for 16 bit samples, non-interpolated mono output */
static void out_mn16norm (int count, int16 *in_bk, int cur_bk, register int itpt)
{
    int vl;
    register int smp_in;
    register int *tmp_bk = Tmp_bk;
    int itpt_inc = Itpt_inc;

    in_bk += cur_bk;
    vl = Vol_left << 1;
    while (count--) {
	DONT_INTERPOLATE ();
	*(tmp_bk++) += smp_in * vl;
	itpt += itpt_inc;
    }
}


/* Fill the output buffer calling one of the handlers. The buffer contains
 * sound for one tick (a PAL frame or 1/50s for standard vblank-timed mods)
 */
int smix_mixer ()
{
    int chn, i, sample;
    int t_cnt, p_cnt;
    struct channel_info *ci;
    struct patch_info *pi;
    int lstart, lend;
    int *orig_bk;
    void *dest_bk;

    Ticksize = Cfg->rate * opt.rrate / __bpm / 100;

    /* Clear the buffer */
    memset (Tmp_bank, 0, Ticksize * Cfg->mode * sizeof (int));

    for (chn = Cfg->nchn; chn--;) {
	ci = &Channel_info_array[chn];
	if (!(ci->fidx && ci->vol))
	    continue;

	pi = Patch_info_array[ci->smp];
	Tmp_bk = Tmp_bank;

	Vol_left = (ci->vol * (0x80 + ci->pan)) >> VOL_FT;
	Vol_right = (ci->vol * (0x80 - ci->pan)) >> VOL_FT;
	Itpt_inc = ((long long) ci->pbase << SHIFT_FT) / ci->period;

	/* This is for bidirectional sample loops */
	if (ci->fidx & FLAG_BACKWARD)
	    Itpt_inc *= -1;

	/* Sample loop processing */
	lstart = pi->loop_start;
	lend = pi->loop_end;

	/* Offsets in samples, not bytes */
	if (ci->fidx & FLAG_16BIT) {
	    lstart >>= 1;
	    lend >>= 1;
	}

	for (t_cnt = Ticksize; t_cnt;) {
	    /* How many samples we can write before the loop break or
	     * sample end ... */
	    p_cnt = 1 + (((long long) (ci->end - ci->pos) << SHIFT_FT) -
		ci->itpt) / Itpt_inc;

	    /* ... inside the tick boundaries */
	    if (p_cnt > t_cnt)
		p_cnt = t_cnt;

	    t_cnt -= p_cnt;

	    /* Call the output handler */
	    out_fn[ci->fidx & FIDX_FLAGMASK] (p_cnt , ci->sptr, ci->pos,
		ci->itpt + (1 << SHIFT_FT));

	    Tmp_bk += p_cnt * Cfg->mode;
	    ci->itpt += Itpt_inc * p_cnt;
	    ci->pos += ci->itpt >> SHIFT_FT;
	    ci->itpt &= AND_FT;

	    /* No more samples in this tick */
	    if (!t_cnt)
		continue;

	    /* Single shot sample */
	    if (!(ci->fidx ^= ci->fxor)) {
		t_cnt = 0;
		continue;
	    }

	    if (!(ci->fidx & FLAG_BACKWARD || ci->fxor)) {
		ci->pos -= lend - lstart;
	    } else {
		Itpt_inc *= -1;
		ci->itpt += Itpt_inc;
		ci->pos += ci->itpt >> SHIFT_FT;
		ci->itpt &= AND_FT;
		ci->end = Itpt_inc > 0 ? lend : lstart;
	    }
	}
    }

    /* Transfer data from the 32-bit mixing buffer to the output buffer */

    orig_bk = Tmp_bank;
    dest_bk = Out_array[Out_cur];

    i = Ticksize * Cfg->mode;

    if (Cfg->fmt & MIX_ULAW) {
	for (; i; --i, ++orig_bk, ++(int8 *) dest_bk) {
	    sample = (*orig_bk >> (LIM_FT + 4));
	    if (sample > LIM12_HI)
		sample = LIM12_HI;
	    else if (sample < LIM12_LO)
		sample = LIM12_LO;
	    *(int8 *) dest_bk = ulaw_encode (sample);
	}
    } else if (Cfg->fmt & MIX_16BIT) {
	for (; i; --i, ++orig_bk, ++(int16 *) dest_bk) {
	    sample = (*orig_bk >> LIM_FT);
	    if (sample > LIM16_HI)
		sample = LIM16_HI;
	    else if (sample < LIM16_LO)
		sample = LIM16_LO;
	    *(int16 *) dest_bk = sample + Out_mask;
	}
    } else {
	for (; i; --i, ++orig_bk, ++(int8 *) dest_bk) {
	    sample = (*orig_bk >> (LIM_FT + 8));
	    if (sample > LIM8_HI)
		sample = LIM8_HI;
	    else if (sample < LIM8_LO)
		sample = LIM8_LO;
	    *(int8 *) dest_bk = sample + Out_mask;
	}
    }

    mixer_buffer = Out_array[Out_cur];

    /* The mixer works with multiple buffers -- this is useless when
     * using muiti-buffered sound output (e.g. OSS fragments) but
     * can be useful for DMA transfers in DOS.
     */
    if (++Out_cur >= Cfg->nbuf)
	Out_cur = 0;

    i = Ticksize * Cfg->mode;
    if (Cfg->fmt & MIX_16BIT)
	i <<= 1;

    return i;
}


static int buffer_alloc ()
{
    int count, out_size;

#define MAX_SRATE 48000
    /* out_size = 15 * rate * mode * res / BPM_MIN / 6; */
    out_size = 15 * MAX_SRATE * 2 * 2 / BPM_MIN / 6;

    if ((Tmp_bank = calloc (15 * MAX_SRATE * 2 / BPM_MIN / 6,
		sizeof (int))) == NULL)
	  return MIX_ERR;

    if ((Out_array = calloc (Cfg->nbuf, sizeof (void *))) == NULL) {
	free (Tmp_bank);
	return MIX_ERR;
    }
    for (count = 0; count < Cfg->nbuf; count++) {
	if ((Out_array[count] = calloc (1, out_size)) == NULL) {
	    while (count)
		free (Out_array[--count]);
	    free (Out_array);
	    free (Tmp_bank);
	    return MIX_ERR;
	}
    }

    return MIX_OK;
}


void smix_init (struct drv_config *cfg)
{
    static int buf_alloc = 0;

    Cfg = cfg;
    Out_mask = 0x0000;
    Out_cur = 0;
    Patch_info_array = calloc (sizeof (struct patch_info), 256);
    Channel_info_array = calloc (sizeof (struct channel_info), 32);

    switch (Cfg->fmt) {
    case 0:
	Out_mask = 0x0080;
    case MIX_SIGNED:
	break;
    case MIX_16BIT:
	Out_mask = 0x8000;
    case MIX_SIGNED | MIX_16BIT:
	break;
    case MIX_BIGENDIAN | MIX_16BIT:
	Out_mask = 0x8000;
    case MIX_SIGNED | MIX_BIGENDIAN | MIX_16BIT:
	break;
    }

    if (!buf_alloc) {
	buffer_alloc ();
	buf_alloc = 1;
    }
}


void smix_close ()
{
    struct patch_info **pi;
    int count;

    while (Cfg->nbuf)
	free (Out_array[--Cfg->nbuf]);
    free (Out_array);
    free (Tmp_bank);

    for (pi = Patch_info_array, count = 256; count--; ++pi) {
	if (*pi != NULL) {
	    free (*pi);
	}
    }

    free (Patch_info_array);
    free (Channel_info_array);
}


void smix_setpatch (int chn, int smp)
{
    struct channel_info *ci = &Channel_info_array[chn];
    struct patch_info *pi = Patch_info_array[smp];

    if (!pi) {
	smix_resetvoice (chn);
	return;
    }
    ci->smp = smp;
    ci->vol = ci->pos = ci->itpt = 0;
    ci->end = pi->len;
    ci->freq = pi->base_freq;

    ci->note = PATCH_OK;

    ci->fidx = FLAG_ACTIVE;
    ci->fidx |= Cfg->itpt ? FLAG_ITPT : 0;

    if (Cfg->mode == 2) {
	ci->fidx |= FLAG_STEREO;
	ci->pan = pi->panning;
    } else {
	ci->pan = 0;
    }

    if (TEST_FLAG (pi->mode, WAVE_16_BITS)) {
	ci->fidx |= FLAG_16BIT;
	ci->sptr = pi->data - 2;
    } else {
	ci->sptr = pi->data - 1;
    }

    if (!TEST_FLAG (pi->mode, WAVE_LOOPING)) {
	ci->fxor = ci->fidx;
    } else {
	if (ci->end > pi->loop_end)
	    ci->end = pi->loop_end;

	ci->fxor = FLAG_BACKWARD *
	    TEST_FLAG (pi->mode, WAVE_BIDIR_LOOP);
    }

    if (ci->fidx & FLAG_16BIT)
	ci->end >>= 1;
}


void smix_setnote (int chn, int note)
{
    struct channel_info *ci = &Channel_info_array[chn];

    if (ci->note != PATCH_OK)
	smix_setpatch (chn, ci->smp);
    ci->note = note;
    ci->period = note_to_period2 (note, 0);
    ci->pbase = (long long) C4_NOTE * ci->freq / Cfg->rate;
}


void smix_voicepos (int chn, int index)
{
    struct channel_info *ci = &Channel_info_array[chn];
    int lend;

    lend = Patch_info_array[ci->smp]->mode & WAVE_LOOPING ?
	Patch_info_array[ci->smp]->loop_end : ci->end;

    if (ci->fidx & FLAG_16BIT) {
	index >>= 1;
	lend >>= 1;
    }

    if (index < lend) {
	ci->itpt = 0;
	ci->pos = index;
	if (ci->fidx & FLAG_BACKWARD) {
	    ci->fidx ^= ci->fxor;
	    ci->end = lend;
	    Itpt_inc *= -1;
	}
    } else {
	ci->vol = 0;
    }
}


void smix_numvoices (int num)
{
    Cfg->nchn = num;
}


void smix_resetvoice (int chn)
{
    memset (&Channel_info_array[chn], 0, sizeof (struct channel_info));
}


void smix_setbend (int chn, int bend)
{
    Channel_info_array[chn].period =
	note_to_period2 (Channel_info_array[chn].note, bend);
}


void smix_setvol (int chn, int vol)
{
    Channel_info_array[chn].vol = vol;
}


void smix_setpan (int chn, int pan)
{
    if (Cfg->mode == 2)
	Channel_info_array[chn].pan = pan;
}


void smix_echoback (int msg)
{
    __event_callback (msg);
    __echo_msg = msg;
}


int smix_getmsg ()
{
    return __echo_msg;
}


void smix_writepatch (struct patch_info *patch)
{
    if (!patch) {
	int i;
	for (i = 256; i--; ) {
	    if (Patch_info_array[i]) {
		free (Patch_info_array[i]);
		Patch_info_array[i] = NULL;
	    }
	}
	memset (Channel_info_array, 0, sizeof (struct channel_info) * 32);
	return;
    }

#ifdef WORDS_BIGENDIAN
    if (patch->mode & WAVE_16_BITS)
        change_sex (patch->len, patch->data);
#endif
    if (patch->mode & WAVE_UNSIGNED)
	sig2uns (patch->len, patch->mode & WAVE_16_BITS, patch->data);

    Patch_info_array[patch->instr_no] = patch;
}
