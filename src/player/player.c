/* Extended Module Player
 * Copyright (C) 1996-1998 Claudio Matsuoka and Hipolito Carraro Jr
 * $Id$
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See doc/COPYING
 * for more information.
 */

/*
 * Sat, 18 Apr 1998 20:23:07 +0200  Frederic Bujon <lvdl@bigfoot.com>
 * Pan effect bug fixed: In Fastracker II the track panning effect erases
 * the instrument panning effect, and the same should happen in xmp.
 */

/*
 * Fri, 26 Jun 1998 13:29:25 -0400 (EDT)
 * Reported by Jared Spiegel <spieg@phair.csh.rit.edu>
 * when the volume envelope is not enabled (disabled) on a sample, and a
 * notoff is delivered to ft2 (via either a noteoff in the note column or
 * command Kxx [where xx is # of ticks into row to give a noteoff to the
 * sample]), ft2 will set the volume of playback of the sample to 00h.
 *
 * (claudio's note: ok, implementing effect K)
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include "xmpi.h"
#include "xxm.h"
#include "effects.h"
#include "player.h"
#include "driver.h"
#include "period.h"

extern struct xmp_drv_info *drv;

static int vol_base;
static int *vol_xlat;

static int g_vslide = 0;
static struct flow_control flow;
static double tick_duration;
static double playing_time;

int __bpm;
int __pause;
struct xmp_chctl __xctl[32];
struct xmp_ord_info xmpi_oinfo[256];
char ivalid[256];

static struct xmp_channel xc[32];
static int tempo;
static int global_vslide;
static int offset = 0;
static double next_time = 0;


/* Vibrato/tremolo waveform tables */
static int waveform[4][64] = {
   {   0,  24,  49,  74,  97, 120, 141, 161, 180, 197, 212, 224,
     235, 244, 250, 253, 255, 253, 250, 244, 235, 224, 212, 197,
     180, 161, 141, 120,  97,  74,  49,  24,   0, -24, -49, -74,
     -97,-120,-141,-161,-180,-197,-212,-224,-235,-244,-250,-253,
    -255,-253,-250,-244,-235,-224,-212,-197,-180,-161,-141,-120,
     -97, -74, -49, -24  },	/* Sine */

   {   0,  -8, -16, -24, -32, -40, -48, -56, -64, -72, -80, -88,
     -96,-104,-112,-120,-128,-136,-144,-152,-160,-168,-176,-184,
    -192,-200,-208,-216,-224,-232,-240,-248, 255, 248, 240, 232,
     224, 216, 208, 200, 192, 184, 176, 168, 160, 152, 144, 136,
     128, 120, 112, 104,  96,  88,  80,  72,  64,  56,  48,  40,
      32,  24,  16,   8  },	/* Ramp down */

   { 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
     255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
     255, 255, 255, 255, 255, 255, 255, 255,-255,-255,-255,-255,
    -255,-255,-255,-255,-255,-255,-255,-255,-255,-255,-255,-255,
    -255,-255,-255,-255,-255,-255,-255,-255,-255,-255,-255,-255,
    -255,-255,-255,-255  },	/* Square */

   {   0,   8,  16,  24,  32,  40,  48,  56,  64,  72,  80,  88,
      96, 104, 112, 120, 128, 136, 144, 152, 160, 168, 176, 184,
     192, 200, 208, 216, 224, 232, 240, 248,-255,-248,-240,-232,
    -224,-216,-208,-200,-192,-184,-176,-168,-160,-152,-144,-136,
    -128,-120,-112,-104, -96, -88, -80, -72, -64, -56, -48, -40,
     -32, -24, -16,  -8  }	/* Ramp up */
};



struct xxm_header *xxh;
struct xxm_pattern **xxp;		/* Patterns */
struct xxm_track **xxt;			/* Tracks */
struct xxm_channel xxc[32];		/* Channel info */
uint8 xxo[256];				/* Orders */
struct xxm_instrument_header *xxih;	/* Instrument headers */
struct xxm_instrument_map *xxim;	/* Instrument map */
struct xxm_instrument **xxi;		/* Instruments */
struct xxm_sample *xxs;			/* Samples */
uint16 **xxae;				/* Amplitude envelope */
uint16 **xxpe;				/* Pan envelope */
uint16 **xxfe;				/* Pitch envelope */

struct xmp_options opt;
char module_name[MODULE_NAME_MAXSIZE];

int xxh_row, xxh_ord, xxh_num;

int __current_ord;
int __current_pat;
int __global_vol;


#include "effects.c"


static void dummy ()
{
    /* dummy */
}


static int get_envelope (uint16 *env, int p, int x)
{
    int x1, x2, y1, y2;

    p--;
    p <<= 1;

    if ((x1 = env[p]) <= x)
	return env[p + 1];

    do {
	p -= 2;
	x1 = env[p];
    } while ((x1 > x) && p);

    y1 = env[p + 1];
    x2 = env[p + 2];
    y2 = env[p + 3];

    return ((y2 - y1) * (x - x1) / (x2 - x1)) + y1;
}


static void fetch_and_play (int t, struct xxm_event *e,
    struct xmp_channel *xc, struct xmp_chctl *xctl, int ch)
{
    uint8 note = 0;
    uint32 finalvol;
    uint16 vol_envelope;
    int finalpan, pan_envelope, vol2;

    if (!t) {
	/* Reset values */
	xc->flags = 0;
	if (!ch)
	    g_vslide = 0;

	xc->delay = xc->retrig = xc->tremor = xc->keyoff = 0;
	xc->a_idx = xc->a_val[1] = xc->a_val[2] = 0;

	note = e->note;

	if (e->ins) {
	    if (e->ins <= xxh->ins && xxih[e->ins - 1].nsm) {
		xc->oldins = xc->ins;
		xc->ins = e->ins - 1;
		SET (NEW_INS);
	    } else
		xc->ins = -1;
	}

	if (note) {
	    if (note == 0x61)
		xc->release = 1;
	    if (xc->ins != -1 && note < 0x61 && XXIH.nsm) {
		note--;
		if (XXIM.ins[note] != 0xff) {
		    SET (NEW_NOTE);
		    xc->s_end = note_to_period (
			note + XXI[XXIM.ins[note]].xpo,
			XXI[XXIM.ins[note]].fin,
			xxh->flg & XXM_FLG_LINEAR);
		} else
		    RESET (NEW_INS);
	    } else
		RESET (NEW_INS);
	}

	if (e->vol) {		/* Volume set */
	    SET (NEW_VOL);
	    xc->volume = e->vol - 1;
	}

	/* Secondary effect is processed _first_ and can be overriden
	 * by the primary effect.
	 */
	process_fx (note, e->f2t, e->f2p, xc);
	process_fx (note, e->fxt, e->fxp, xc);

	/* Process note */
	if (TEST (NEW_NOTE)) {
	    xc->key = note;
	    xc->note = note + XXI[XXIM.ins[note]].xpo;
	    if (!TEST (FINETUNE))
		xc->finetune = XXI[XXIM.ins[note]].fin;
	    xc->period = note_to_period (xc->note, xc->finetune,
		xxh->flg & XXM_FLG_LINEAR);
	}

	/* Process instrument */
	if (TEST (NEW_INS) && !TEST (NEW_VOL))
	    xc->volume = XXI[XXIM.ins[xc->key]].vol;

	if (TEST (NEW_NOTE | NEW_INS)) {
	    xc->p_idx = xc->v_idx = xc->insvib_idx = xc->release = 0;
	    xc->fadeout = 0x1fff;
	    xc->insvib_idx = 0;
	    xc->insvib_swp = XXI->vsw;
	}

	if (TEST (TONEPORTA))
	    RESET (NEW_INS);

	/* Fadeout */
	if (xc->release)
	    xc->fadeout = (XXIH.aei.flg & XXM_ENV_ON) ?
		((xc->fadeout > XXIH.rls) ?  xc->fadeout - XXIH.rls : 0) : 0;

	if (TEST (NEW_NOTE) || (TEST (NEW_INS) && xc->ins != xc->oldins)) {
	    if (XXIM.ins[xc->key] != 0xff) {
		xc->smp = XXI[XXIM.ins[xc->key]].sid;
		if (ivalid[xc->smp]) {
		    xc->y_idx = xc->t_idx = 0;
		    drv->setpatch (ch, xc->smp);
		    drv->setnote (ch, xc->note);

		    /* Pan setting fixed by Frederic Bujon <lvdl@bigfoot.com> */
		    xc->pan = (e->fxt == FX_SETPAN) ?
			e->fxp : XXI[XXIM.ins[xc->key]].pan;
		}
	    } else
		xc->smp = 0xff;
	}

	if (offset) {
	    drv->voicepos (ch, offset);
	    offset = 0;
	}
    }

    if (xc->ins == -1) {
	drv->setvol (ch, 0);
	return;
    }

    vol_envelope = (XXIH.aei.flg & XXM_ENV_ON) && !opt.noenv ?
	get_envelope (XXAE, XXIH.aei.npt, xc->v_idx) : 64;

    pan_envelope = (XXIH.pei.flg & XXM_ENV_ON) && !opt.noenv ?
	get_envelope (XXPE, XXIH.pei.npt, xc->p_idx) : 32;

    /* Update envelopes */
    DO_ENVELOPE (XXIH.aei.flg, xc->v_idx, XXAE,
	XXIH.aei.sus, XXIH.aei.lps, XXIH.aei.lpe);
    DO_ENVELOPE (XXIH.pei.flg, xc->p_idx, XXPE,
	XXIH.pei.sus, XXIH.pei.lps, XXIH.pei.lpe);

    vol2 = xc->volume;

    if (TEST (TREMOLO))
	vol2 += waveform[xc->t_type][xc->t_idx] * xc->t_depth / 512;
    if (vol2 > vol_base)
	vol2 = vol_base;
    if (vol2 < 0)
	vol2 = 0;

    finalvol = (uint32) (xc->fadeout * vol_envelope * __global_vol *
	((int) vol2 * 0x40 / vol_base)) >> 20;

    finalvol = vol_xlat ? (vol_xlat[finalvol >> 5] * 100) >> 6 :
	(finalvol * 80) >> 12;

    xc->pitchbend = period_to_bend (xc->period + (TEST (VIBRATO) ?
	waveform[xc->y_type][xc->y_idx] * xc->y_depth * 64 / 8192 : 0) +
	waveform[XXI->vwf][xc->insvib_idx] * XXI->vde / (1024 *
	(1 + xc->insvib_swp)),
	xc->note, xc->finetune, xxh->flg & XXM_FLG_MODRNG, xc->gliss,
	xxh->flg & XXM_FLG_LINEAR);

    /* From Takashi Iwai's awedrv FAQ:
     *
     * Q3.9: Many clicking noises can be heard in some midi files.
     *    A: If this happens when panning status changes, it is due to the
     *       restriction of Emu8000 chip. Try -P option with drvmidi. This
     *       option suppress the realtime pan position change. Otherwise,
     *       it may be a bug.
     */

    finalpan = opt.nopan ? 128 : xc->pan + (pan_envelope - 32) *
	(128 - abs (xc->pan - 128)) / 32;
    finalpan = xc->masterpan + (finalpan - 128) *
	(128 - abs (xc->masterpan - 128)) / 128;

    drv->echoback ((finalpan << 12) | (ch << 4) | XMP_ECHO_CHN);

    if (TEST (NEW_NOTE | NEW_INS | NEW_VOL | PITCHBEND | TONEPORTA)) {
	drv->echoback ((xc->key << 12) | (xc->ins << 4) | XMP_ECHO_INS);
	drv->echoback ((xc->volume << 4) | XMP_ECHO_VOL);
	drv->echoback ((xc->pitchbend << 4) | XMP_ECHO_PBD);
    }

    /* Do delay */
    if (xc->delay) {
	if (--xc->delay)
	    finalvol = 0;
	else
	    drv->setnote (ch, xc->note);
    }

    /* Do tremor */
    if (xc->tremor) {
	if (xc->tcnt_dn > 0)
	    finalvol = 0;
	if (!xc->tcnt_up--)
	    xc->tcnt_dn = LSN (xc->tremor);
	if (!xc->tcnt_dn--)
	    xc->tcnt_up = MSN (xc->tremor);
    }

    /* Do keyoff */
    if (xc->keyoff) {
	if (!--xc->kcount)
	    xc->release = 1;
    }

    /* Do cut/retrig */
    if (xc->retrig) {
	if (!--xc->rcount) {
	    drv->setnote (ch, xc->note);
	    xc->volume += rval[xc->rtype].s;
	    xc->volume *= rval[xc->rtype].m;
	    xc->volume /= rval[xc->rtype].d;
	    xc->rcount = xc->retrig;
	}
    }

    /* Do global volume slide */
    if (t || opt.vef) {
	if (!ch && g_vslide) {
	    __global_vol += global_vslide;
	    if (__global_vol < 0)
		__global_vol = 0;
	    else if (__global_vol > 0xff)
		__global_vol = 0xff;
	}

	if (TEST (VOL_SLIDE)) {
	    xc->volume += xc->v_val;
	    if (xc->volume < 0)
		xc->volume = 0;
	    else if (xc->volume > vol_base)
		xc->volume = vol_base;
	}
    }

    if (!(t % tempo)) {
	/* Process "fine" effects */
	if (TEST (FINE_VOLS))
	    xc->volume += xc->v_fval;
	if (TEST (FINE_BEND))
	    xc->period = (4 * xc->period + xc->f_fval) / 4;
    } else {
	/* Do pan and pitch sliding */
	if (TEST (PAN_SLIDE)) {
	    xc->pan += xc->p_val;
	    if (xc->pan < 0)
		xc->pan = 0;
	    else if (xc->pan > 0xff)
		xc->pan = 0xff;
	}

	if (TEST (PITCHBEND)) {
	    if (xxh->flg & XXM_FLG_LINEAR) {
		xc->period += xc->f_val;
		if (xc->period < MIN_PERIOD_L)
		    xc->period = MIN_PERIOD_L;
		else if (xc->period > MAX_PERIOD_L)
		    xc->period = MAX_PERIOD_L;
	    } else {
		xc->period += xc->f_val;
		if (xc->period < MIN_PERIOD_A)
		    xc->period = MIN_PERIOD_A;
		else if (xc->period > MAX_PERIOD_A)
		    xc->period = MAX_PERIOD_A;
	    }
	}

	/* Workaround for panic.s3m (from Toru Egashira's NSPmod) */
	if (xc->period <= 8)
	    xc->volume = 0;
    }

    /* Do tone portamento */
    if (TEST (TONEPORTA)) {
	xc->period += (xc->s_sgn * xc->s_val);
	if ((xc->s_sgn * xc->s_end) <= (xc->s_sgn * xc->period)) {
	    xc->period = xc->s_end;
	    RESET (TONEPORTA);
	}
    }

    /* Update vibrato, tremolo and arpeggio indexes */
    xc->insvib_idx += XXI->vra >> 2;
    xc->insvib_idx %= 64;
    if (xc->insvib_swp > 1)
	xc->insvib_swp -= 2;
    else
	xc->insvib_swp = 0;
    xc->y_idx += xc->y_rate;
    xc->y_idx %= 64;
    xc->t_idx += xc->t_rate;
    xc->t_idx %= 64;
    xc->a_idx++;
    xc->a_idx %= 3;

    /* Adjust pitch and pan, than play the note */
    drv->setbend (ch, (xc->pitchbend + xc->a_val[xc->a_idx]));
    drv->setpan (ch, (finalpan - 0x80) * opt.mix / 100 * opt.reverse);

    if (!xctl->mute)
	drv->setvol (ch, ivalid[xc->smp] ? finalvol : 0);
}


void xmpi_play_init ()
{
    int c;

    if (__event_callback == NULL)
	__event_callback = dummy;

    tick_duration = opt.rrate / __bpm;
    playing_time = offset = 0;
    vol_base = opt.vol_base;
    vol_xlat = opt.vol_xlat;

    for (c = 0; c < 32; c++)
	CLEAR_CHANNEL (c);

    drv->bufwipe ();
    drv->sync (next_time = 0);
    drv->numvoices (32);
    drv->reset ();

    for (c = 0; c < xxh->chn; c++)
	xc[c].masterpan = xxc[c].pan;

    drv->numvoices (xxh->chn);
    drv->starttimer ();

    flow.loop_stack = calloc (sizeof (uint8), ROW_MAX);
    flow.jump = -1;
    __global_vol = 0x40;
    global_vslide = 0;
}

void xmpi_play_cleanup()
{
    drv->echoback (XMP_ECHO_END);
    drv->reset ();
    while (drv->getmsg () != XMP_ECHO_END)
	drv->bufdump ();
    drv->stoptimer ();
    drv->writepatch (NULL);	/* Unload all patches */
    drv->bufdump ();

    free (flow.loop_stack);
    opt.start = 0;
}

int xmpi_play_pat(int pat, int *o, int *get_out)
{
	int r, t, c, tempr;	/* rows, tick, channel */
	uint8 *p = 0;		/* pattern */

	r = xxp[pat]->rows;
	if (flow.jumpline >= r)
	    flow.jumpline = 0;
	flow.loop_row = -(flow.jumpline + 1);
	flow.row_cnt = flow.jumpline;
	flow.loop_cnt = flow.jumpline = 0;

	for (; flow.row_cnt < r; flow.row_cnt++, flow.row_ptr = p) {
	    if (!opt.loop && (*o) == xxh_ord && flow.row_cnt == xxh_row) {
		if (!(*get_out))
		    return 1;

                if ((*get_out) > 0)
		  --(*get_out);
	    }

            /*
	    if (__pause) {
		xmpi_timer_stop ();
		while (__pause) {
		    xmpi_select_read (1, 125);
		    __event_callback (0);
		}
		xmpi_timer_restart ();
	    }
            */

	    drv->echoback ((tempo << 12) | (__bpm << 4) | XMP_ECHO_BPM);
	    drv->echoback ((__global_vol << 4) | XMP_ECHO_GVL);
	    drv->echoback ((xxo[*o] << 12) | ((*o) << 4) | XMP_ECHO_ORD);
	    drv->echoback (((xxp[xxo[*o]]->rows & 0xff) << 12) |
		((flow.row_cnt & 0xff) << 4) | XMP_ECHO_ROW);

	    for (t = 0; t < (tempo * (1 + flow.delay)); t++) {
/* FIXME: t won't be accurate when jumping to patterns of diff lengths/temp? */

		/* xmp_player_ctl processing */

		if ((*o) != __current_ord) {
		    if (__current_ord == -1)
			__current_ord++;

		    if (__current_ord == -2) {	/* set by xmp_module_stop */
			drv->bufwipe ();
			return 1;
		    }

		    /*flow.row_cnt = -1;
		    flow.row_ptr = p;*/
		    (*o) = __current_ord;
                    __current_pat = pat = xxo[*o];
                    tempr = xxp[pat]->rows;
                    if ((flow.row_cnt > tempr) && (tempr < r))
                      flow.row_cnt -= r - tempr;
                    r = tempr;
		    tempo = xmpi_oinfo[*o].tempo;
		    __global_vol = xmpi_oinfo[*o].gvl;
		    tick_duration = opt.rrate / (__bpm = xmpi_oinfo[*o].bpm);
		    /*(*o)--;*/
		    drv->bufwipe ();
		    /*drv->sync (next_time = 0);*/
		    drv->reset ();
                    /*
		    for (c = 0; c < xxh->chn; c++)
			CLEAR_CHANNEL (c);
		    break;
                    */
		}

		for (c = 0; c < xxh->chn; c++) {
		    fetch_and_play (t, &EVENT (pat, c, flow.row_cnt),
			&xc[c], &__xctl[c], c);
		}

		if (opt.time && (opt.time < playing_time))
		    return 1;

		playing_time += opt.rrate / (100 * __bpm);

		next_time += tick_duration;
		drv->sync (next_time);
		drv->bufdump ();
	    }

	    flow.delay = 0;

	    if (flow.row_cnt == -1)
		break;

	    if (flow.pbreak) {
		flow.pbreak = 0;
		break;
	    }

	    if (flow.loop) {
		flow.loop_cnt = 0;
		p = flow.loop_ptr;
		if (flow.loop_row < 0) {
		    flow.row_cnt = -(flow.loop_row + 2);
		    flow.loop = 0;
		} else
		    flow.row_cnt = flow.loop_row - 1;
	    }
	}
        return 0;
}

void xmpi_play_pattern(int pat)
{
  int o = -1;
  int get_out = -1; /* disable get_out */
  __current_ord = -1;
  __global_vol = xmpi_oinfo[0].gvl;
  __bpm = xmpi_oinfo[0].bpm;
  tempo = xmpi_oinfo[0].tempo;
  xmpi_play_init();

  for(;;) {
    __current_ord = o = -1;
    __current_pat = pat;
    if (xmpi_play_pat(pat, &o, &get_out))
      break;
    flow.jump = -1;
  }
  xmpi_play_cleanup();
}

void xmpi_play_module()
{
    int get_out;
    int o, pat;

    o = opt.start;
    __global_vol = xmpi_oinfo[o].gvl;
    __bpm = xmpi_oinfo[o].bpm;
    tempo = xmpi_oinfo[o].tempo;

    xmpi_play_init();

    for (get_out = xxh_num; ; o++) {
        if (o >= xxh->len)
	    o = (xxo[xxh->rst] >= xxh->pat) ? 0 : xxh->rst;

	__current_ord = o;
	__current_pat = pat = xxo[o];

	if ((xxo[o] >= xxh->pat) && (xxo[o] != 0xff))
	    continue;

	/* S3M uses 0xff as an end mark */
	if (xxo[o] == 0xff) {
	    if (!opt.loop)
		break;
	    o = xxh->rst;
	}

        if (xmpi_play_pat(pat, &o, &get_out))
          break;

	if (flow.jump != -1) {
	    o = flow.jump;
	    flow.jump = -1;
	    continue;
	} else if (opt.loop && (o == (xxh->len - 1))) {
	    o = xxh->rst;
	    continue;
	}
    }
    xmpi_play_cleanup();
}

void xmpi_timer_stop ()
{
    int c;

    for (c = 0; c < 32; c++)
	drv->setvol (c, 0);
    drv->stoptimer ();
    drv->bufdump ();
}


void xmpi_timer_restart ()
{
    drv->sync (next_time = 0);
    drv->starttimer ();
}

