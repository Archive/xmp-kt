/* Extended Module Player
 * Copyright (C) 1996-1998 Claudio Matsuoka and Hipolito Carraro Jr
 * $Id$
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See doc/COPYING
 * for more information.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "xmpi.h"
#include "xxm.h"
#include "effects.h"
#include "driver.h"

extern char module_name[MODULE_NAME_MAXSIZE];
extern struct xmp_chctl xctl[32];

static char module_type[80];

#define LOAD_INIT() { \
    memset (xxh, 0, sizeof (*xxh)); \
    memset (module_name, 0, MODULE_NAME_MAXSIZE); \
    fseek (f, 0, SEEK_SET); \
}

#define MODULE_INFO() { \
    if (opt.verbose) { \
	if (*module_name) report ("Module title   : %s\n", module_name); \
        report ("Module type    : %s\n", module_type); \
        if (xxh->len) report ("Module length  : %d patterns\n", xxh->len); \
    } \
}

#define INSTRUMENT_INIT() { \
    xxih = xmp_calloc (sizeof (struct xxm_instrument_header), xxh->ins); \
    xxim = xmp_calloc (sizeof (struct xxm_instrument_map), xxh->ins); \
    xxi = xmp_calloc (sizeof (struct xxm_instrument *), xxh->ins); \
    xxs = xmp_calloc (sizeof (struct xxm_sample), xxh->smp); \
    xxae = xmp_calloc (sizeof (uint16 *), xxh->ins); \
    xxpe = xmp_calloc (sizeof (uint16 *), xxh->ins); \
}
/*    xxfe = calloc (sizeof (uint16 *), xxh->ins); \ */

#define PATTERN_INIT() { \
    xxt = xmp_calloc (sizeof (struct xxm_track *), xxh->trk); \
    xxp = xmp_calloc (sizeof (struct xxm_pattern *), xxh->pat + 1); \
}

#define PATTERN_ALLOC(x) { \
    xxp[x] = xmp_calloc (1, sizeof (struct xxm_pattern) + \
         sizeof (struct xxm_trackinfo) * (xxh->chn - 1)); \
}

#define TRACK_ALLOC(i) { \
    int j; \
    for (j = 0; j < xxh->chn; j++) { \
	xxp[i]->info[j].index = i * xxh->chn + j; \
	xxt[i * xxh->chn + j] = xmp_calloc (sizeof (struct xxm_track) + \
	    sizeof (struct xxm_event) * xxp[i]->rows, 1); \
	xxt[i * xxh->chn + j]->rows = xxp[i]->rows; \
    } \
}
