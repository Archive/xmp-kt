/* Extended Module Player
 * Copyright (C) 1996-1998 Claudio Matsuoka and Hipolito Carraro Jr
 * $Id$
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See doc/COPYING
 * for more information.
 */

#ifndef __MOD_H__
#define __MOD_H__

struct mod_instrument {
    uint8 name[22];		/* Instrument name */
    uint16 size;		/* Sample length in 16-bit words */
    int8 finetune;		/* Finetune (signed nibble) */
    int8 volume;		/* Linear playback volume */
    uint16 loop_start;		/* Loop start in 16-bit words */
    uint16 loop_size;		/* Loop length in 16-bit words */
} PACKED;

struct m31_header {
    uint8 name[20];
    struct mod_instrument ins[31];
    uint8 len;
    uint8 restart;		/* Number of patterns in Soundtracker,
				 * Restart in Noisetracker/Startrekker,
				 * 0x7F in Protracker
				 */
    uint8 order[128];
    uint8 magic[4];
} PACKED;


/* Soundtracker 15-instrument module header */

struct m15_header {
    uint8 name[20];
    struct mod_instrument ins[15];
    uint8 len;
    uint8 restart;
    uint8 order[128];
} PACKED;


union mod_header {
    struct m31_header m31;
    struct m15_header m15;
} PACKED;

#endif /* __MOD_H__ */
