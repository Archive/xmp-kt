/* Extended Module Player
 * Copyright (C) 1996-1998 Claudio Matsuoka and Hipolito Carraro Jr
 * $Id$
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See doc/COPYING
 * for more information.
 */


#define PT3_FLAG_CIA	0x0001	/* VBlank if not set */
#define PT3_FLAG_FILTER	0x0002	/* Ignored by xmp */
#define PT3_FLAG_SONG	0x0004	/* Modules have this bit unset */
#define PT3_FLAG_IRQ	0x0008	/* Ignored by xmp */
#define PT3_FLAG_VARPAT	0x0010	/* Variable pattern length */
#define PT3_FLAG_8VOICE	0x0020	/* 4 voices if not set */
#define PT3_FLAG_16BIT	0x0040	/* 8 bit samples if not set */
#define PT3_FLAG_RAWPAT	0x0080	/* Packed patterns if not set */


struct pt3_chunk_info {
    char name[32];
    uint16 nins;
    uint16 npos;
    uint16 npat;
    uint16 gvol;
    uint16 dbpm;
    uint16 flgs;
    uint16 cday;
    uint16 cmon;
    uint16 cyea;
    uint16 chrs;
    uint16 cmin;
    uint16 csec;
    uint16 dhrs;
    uint16 dmin;
    uint16 dsec;
    uint16 dmsc;
} PACKED;

struct pt3_chunk_inst {
    char name[32];
    uint16 ilen;
    uint16 ilps;
    uint16 ilpl;
    uint16 ivol;
    int16 fine;
} PACKED;

