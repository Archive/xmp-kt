/* Extended Module Player
 * Copyright (C) 1996-1998 Claudio Matsuoka and Hipolito Carraro Jr
 * $Id$
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See doc/COPYING
 * for more information.
 */

/*
 * Tue, 30 Jun 1998 20:23:11 +0200
 * Reported by John v/d Kamp <blade_@dds.nl>:
 * I have this song from Purple Motion called wcharts.s3m, the global
 * volume was set to 0, creating a devide by 0 error in xmp. There should
 * be an extra test if it's 0 or not.
 */

/*
 * Sat, 29 Aug 1998 18:50:43 -0500 (CDT)
 * Reported by Joel Jordan <scriber@usa.net>:
 * S3M files support tempos outside the ranges defined by xmp (that is,
 * the MOD/XM tempo ranges).  S3M's can have tempos from 0 to 255 and speeds
 * from 0 to 255 as well, since these are handled with separate effects
 * unlike the MOD format.  This becomes an issue in such songs as Skaven's
 * "Catch that Goblin", which uses speeds above 0x1f.
 *
 * Claudio's note: S3M supports speeds from 0 to 255 and tempos from 32
 * to 255 (S3M speed == xmp tempo, S3M tempo == xmp BPM).
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "load.h"
#include "s3m.h"
#include "period.h"


#define NONE 0xff
#define FX_S3M_EXTENDED 0xfe

struct ffx_xlat {
    int pv, pu, pd;
    int v, u, d;
};


static uint16 *pp_ins;		/* Parapointers to instruments */
static uint16 *pp_pat;		/* Parapointers to patterns */
static struct ffx_xlat ffx[32];
static int lp_flag;
static int arpeggio_val;

/* Effect conversion table */
static uint8 fx[] =
{
    NONE, FX_S3M_TEMPO, FX_JUMP, FX_BREAK, FX_VOLSLIDE, FX_PORTA_DN,
    FX_PORTA_UP, FX_TONEPORTA, FX_VIBRATO, FX_TREMOR, FX_ARPEGGIO,
    FX_VIBRA_VSLIDE, FX_TONE_VSLIDE, NONE, NONE, FX_OFFSET, NONE,
    FX_MULTI_RETRIG, FX_TREMOLO, FX_S3M_EXTENDED, FX_TEMPO,
    NONE, FX_GLOBALVOL, NONE, NONE, NONE, NONE
};


/* Effect translation */
static void xlat_fx (int c, struct xxm_event *e)
{
    uint8 h = MSN (e->fxp), l = LSN (e->fxp);

    switch (e->fxt = fx[e->fxt]) {
    case FX_ARPEGGIO:			/* Arpeggio */
	if (e->fxp)
	    arpeggio_val = e->fxp;
	else
	    e->fxp = arpeggio_val;
	break;
    case FX_VOLSLIDE:			/* Fine volume slides */
	if (!e->fxp && ffx[c].pv) {
	    e->fxt = ffx[c].pv;
	    e->fxp = ffx[c].v;
	}
	if (l && h && (l != 0xf) && (l != 0xe) && (h != 0xf) && (h != 0xe))
	    e->fxp &= 0x0f;
	if (l && h == 0xf) {
	    e->fxt = FX_EXTENDED;
	    e->fxp = (EX_F_VSLIDE_DN << 4) + l;
	}
	if (l && h == 0xe) {
	    e->fxt = FX_EXTENDED;
	    e->fxp = (EX_F_VSLIDE_DN << 4) + l;
	}
	if (h && l == 0xf) {
	    e->fxt = FX_EXTENDED;
	    e->fxp = (EX_F_VSLIDE_UP << 4) + h;
	}
	if (h && l == 0xe) {
	    e->fxt = FX_EXTENDED;
	    e->fxp = (EX_F_VSLIDE_UP << 4) + h;
	}
	ffx[c].pv = e->fxt;
	ffx[c].v = e->fxp;
	break;
    case FX_PORTA_DN:			/* Fine portamento down */
	if (!e->fxp && ffx[c].pd) {
	    e->fxt = ffx[c].pd;
	    e->fxp = ffx[c].d;
	}
	if (h == 0xf) {
	    e->fxt = FX_EXTENDED;
	    e->fxp = (EX_F_PORTA_DN << 4) + l;
	}
	if (h == 0xe) {
	    e->fxt = FX_XF_PORTA;
	    e->fxp = 0x20 + l;
	}
	ffx[c].pd = e->fxt;
	ffx[c].d = e->fxp;
	break;
    case FX_PORTA_UP:			/* Fine portamento up */
	if (!e->fxp && ffx[c].pu) {
	    e->fxt = ffx[c].pu;
	    e->fxp = ffx[c].u;
	}
	if (h == 0xf) {
	    e->fxt = FX_EXTENDED;
	    e->fxp = (EX_F_PORTA_UP << 4) + l;
	}
	if (h == 0xe) {
	    e->fxt = FX_XF_PORTA;
	    e->fxp = 0x10 + l;
	}
	ffx[c].pu = e->fxt;
	ffx[c].u = e->fxp;
	break;
    case FX_TEMPO:
	if (e->fxp < 0x20)
	    e->fxt = 0x20;
	break;
    case FX_S3M_TEMPO:
	if (e->fxp < 0x20)
	    e->fxt = FX_TEMPO;
	break;
    case FX_S3M_EXTENDED:		/* Extended effects */
	e->fxt = FX_EXTENDED;
	switch (h) {
	case 0x1:			/* Glissando */
	    e->fxp = LSN (e->fxp) | (EX_GLISS << 4);
	    break;
	case 0x2:			/* Finetune */
	    e->fxp = LSN (e->fxp) | (EX_FINETUNE << 4);
	    break;
	case 0x3:			/* Vibrato wave */
	    e->fxp = LSN (e->fxp) | (EX_VIBRATO_WF << 4);
	    break;
	case 0x4:			/* Tremolo wave */
	    e->fxp = LSN (e->fxp) | (EX_TREMOLO_WF << 4);
	    break;
	case 0x5:
	case 0x6:
	case 0x7:
	case 0x9:
	case 0xa:			/* Ignore */
	    e->fxt = e->fxp = 0;
	    break;
	case 0x8:			/* Set pan */
	    e->fxt = FX_MASTER_PAN;
	    e->fxp = l << 4;
	    break;
	case 0xb:			/* Pattern loop */
	    e->fxp = LSN (e->fxp) | (EX_PATTERN_LOOP << 4);
	    lp_flag = (LSN (e->fxp)) ? -1 : 1;
	    break;
	}
	break;
    case NONE:				/* No effect */
	e->fxt = e->fxp = 0;
	break;
    }
}


int s3m_load (FILE * f)
{
    int c, r, i, j;
    struct s3m_adlib_header sah;

    struct xxm_event *event = 0, dummy;
    struct s3m_file_header sfh;
    struct s3m_instrument_header sih;
    int pat_len;
    uint8 n, b, x8, tmp[80];
    uint16 x16;

    memset (ffx, 0, sizeof (ffx));
    lp_flag = 1;

    LOAD_INIT ();

    /* Load and convert header */
    fread (&sfh, 1, sizeof (sfh), f);
    if (strncmp ((char *) sfh.magic, "SCRM", 4))
	return -1;
    L_ENDIAN16 (sfh.ordnum);
    L_ENDIAN16 (sfh.insnum);
    L_ENDIAN16 (sfh.patnum);
    L_ENDIAN16 (sfh.ffi);
    str_adj ((char *) sfh.name);
    strcpy (module_name, (char *) sfh.name);
    xxh->len = sfh.ordnum;
    xxh->ins = sfh.insnum;
    xxh->smp = xxh->ins;
    xxh->pat = sfh.patnum;
    pp_ins = calloc (2, xxh->ins);
    pp_pat = calloc (2, xxh->pat);
    if (sfh.flags & S3M_AMIGA_RANGE)
	xxh->flg |= XXM_FLG_MODRNG;
    if (sfh.flags & S3M_ST300_VOLS)
	opt.vef = 1;
    /* opt.vol_base = 4096 / sfh.gv; */
    xxh->tpo = sfh.is;
    xxh->bpm = sfh.it;
    /* xxh->flg |= XXM_FLG_LOOPS3M; */

    for (i = 0; i < 32; i++) {
	if (sfh.chset[i] != S3M_CH_OFF)
	    xxh->chn = i + 1;
	else
	    continue;
	xxc[i].pan = sfh.mv & 0x80 ?
		(((sfh.chset[i] & S3M_CH_PAN) >> 3) * 0xff) & 0xff : 0x80;
    }
    xxh->trk = xxh->pat * xxh->chn;

    fread (xxo, 1, xxh->len, f);
    /* S3M skips pattern 0xfe */
    for (i = 0; i < (xxh->len - 1); i++)
	if (xxo[i] == 0xfe) {
	    memcpy (&xxo[i], &xxo[i + 1], xxh->len - i - 1);
	    xxh->len--;
	}
    while (xxh->len && xxo[xxh->len - 1] == 0xff)
	xxh->len--;

    for (i = 0; i < xxh->ins; i++) {
	fread (&pp_ins[i], 2, 1, f);
	L_ENDIAN16 (pp_ins[i]);
    }
    for (i = 0; i < xxh->pat; i++) {
	fread (&pp_pat[i], 2, 1, f);
	L_ENDIAN16 (pp_pat[i]);
    }

    /* Default pan positions */
    for (i = 0, sfh.dp -= 0xfc; !sfh.dp && n && (i < 32); i++) {
	fread (tmp, 1, 1, f);
	if (tmp[0] && S3M_PAN_SET)
	    xxc[i].pan = (tmp[0] << 4) & 0xff;
    }

    opt.c4rate = C4_NTSC_RATE;

    sprintf (module_type, "Scream Tracker 3.%02x", sfh.version & 0xff);
    if (sfh.version == 0x1300)
	opt.vef = 1;

    MODULE_INFO ();

    PATTERN_INIT ();

    /* Read patterns */

    if (opt.verbose)
	report ("Stored patterns: %d ", xxh->pat);

    for (i = 0; i < xxh->pat; i++) {
	PATTERN_ALLOC (i);
	xxp[i]->rows = 64;
	TRACK_ALLOC (i);

	if (!pp_pat[i])
	    continue;

	fseek (f, pp_pat[i] * 16, SEEK_SET);
	r = 0;
	fread (&x16, 2, 1, f);
	pat_len = x16;
	L_ENDIAN16 (pat_len);
	pat_len -= 2;

	while (--pat_len >= 0) {
	    fread (&b, 1, 1, f);

	    if (b == S3M_EOR) {
		r++;
		lp_flag++;
		continue;
	    }

	    c = b & S3M_CH_MASK;
	    event = c >= xxh->chn ? &dummy : &EVENT (i, c, r);
	    if (!lp_flag) {
		event->f2t = FX_EXTENDED;
		event->f2p = 0x60;
		lp_flag++;
	    }

	    if (b & S3M_NI_FOLLOW) {
		fread (&n, 1, 1, f);

		switch (n) {
		case 255:
		    n = 0;
		    break;	/* Empty note */
		case 254:
		    n = 0x61;
		    break;	/* Key off */
		default:
		    n = 1 + 12 * MSN (n) + LSN (n);
		}

		event->note = n;
		fread (&n, 1, 1, f);
		event->ins = n;
		pat_len -= 2;
	    }

	    if (b & S3M_VOL_FOLLOWS) {
		fread (&n, 1, 1, f);
		event->vol = n + 1;
		pat_len--;
	    }

	    if (b & S3M_FX_FOLLOWS) {
		fread (&n, 1, 1, f);
		event->fxt = n;
		fread (&n, 1, 1, f);
		event->fxp = n;
		xlat_fx (c, event);
		pat_len -= 2;
	    }
	}

	if (opt.verbose)
	    report (".");
    }

    if (opt.verbose)
	report ("\n");

    if (opt.verbose > 1) {
	report ("Stereo enabled : %s\n", sfh.mv % 0x80 ? "yes" : "no");
	report ("Pan settings   : %s\n", sfh.dp ? "no" : "yes");
    }

    INSTRUMENT_INIT ();

    /* Read and convert instruments and samples */

    if (opt.verbose)
	report ("Instruments    : %d ", xxh->ins);

    for (i = 0; i < xxh->ins; i++) {
	xxi[i] = xmp_calloc (sizeof (struct xxm_instrument), 1);
	fseek (f, pp_ins[i] * 16, SEEK_SET);
	fread (&x8, 1, 1, f);
	xxi[i][0].pan = 0x80;
	xxi[i][0].sid = i;

	if (x8 >= 2) {
	    /* OPL2 FM instrument */
	    fread (&sah, 1, sizeof (sah), f);
	    if (strncmp (sah.magic, "SCRI", 4))
		return -2;
	    L_ENDIAN16 (sah.c2spd);
	    sah.magic[0] = 0;
	    str_adj ((char *) sah.name);
	    strncpy ((char *) xxih[i].name, sah.name, 24);
	    xxih[i].nsm = 1;
	    xxi[i][0].vol = sah.vol;
	    c2spd_to_note (sah.c2spd, &xxi[i][0].xpo, &xxi[i][0].fin);
	    xxi[i][0].xpo += 12;
	    drv_loadpatch (f, xxi[i][0].sid, 0, 0, NULL, (char *) &sah.reg);
	    if (opt.verbose <= 1) {
		if (opt.verbose)
		    report (".");
		continue;
	    }

	    report ("\n[%2X] %-28.28s ", i, sah.name);

	    if (opt.verbose > 1) {
		for (j = 0; j < 11; j++) {
		    report ("%02x ", (uint8) sah.reg[j]);
		}
	    }

	    continue;
	}
	fread (&sih, 1, sizeof (sih), f);

	if ((x8 == 1) && strncmp (sih.magic, "SCRS", 4))
	    return -2;

	L_ENDIAN16 (sih.memseg);
	L_ENDIAN32 (sih.length);
	L_ENDIAN32 (sih.loopbeg);
	L_ENDIAN32 (sih.loopend);
	L_ENDIAN16 (sih.c2spd);

	xxih[i].nsm = !!(xxs[i].len = sih.length);
	xxs[i].lps = sih.loopbeg;
	xxs[i].lpe = sih.loopend;

	xxs[i].flg = sih.flags & 1 ? WAVE_LOOPING : 0;
	xxi[i][0].vol = sih.vol;
	sih.magic[0] = 0;
	str_adj ((char *) sih.name);
	strncpy ((char *) xxih[i].name, sih.name, 24);

	if ((opt.verbose > 1) && (strlen ((char *) sih.name) || xxs[i].len))
	    report ("\n[%2X] %-28.28s %04x %04x %04x %c V%02x %5d ",
		i, sih.name, xxs[i].len, xxs[i].lps, xxs[i].lpe, xxs[i].flg
		& WAVE_LOOPING ? 'L' : ' ', xxi[i][0].vol, sih.c2spd);

	c2spd_to_note (sih.c2spd, &xxi[i][0].xpo, &xxi[i][0].fin);

	if (!xxs[i].len)
	    continue;
	fseek (f, 16L * sih.memseg, SEEK_SET);
	drv_loadpatch (f, xxi[i][0].sid, opt.c4rate,
	    (sfh.ffi - 1) * XMP_SMP_UNS, &xxs[i], NULL);
	if (opt.verbose)
	    report (".");
    }

    if (opt.verbose)
	report ("\n");

    return 0;
}
