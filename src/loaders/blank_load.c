/* Extended Module Player - blank_load.c
 * Copyright (C) 1996,1997 Claudio Matsuoka and Hipolito Carraro Jr
 * Modified by Widget
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See docs/COPYING
 * for more information.
 */

#include "config.h"

#include "load.h"

int blank_load ()
{
    static int new_song_number = 0;
    int i, j, r;
/*
    int instr_no = 0;
    uint8 *patbuf, *p, b;
*/
    struct xxm_event *event;

    xxh = xmp_calloc(sizeof (*xxh), 1);

    sprintf (module_name, "New Song - %02x", new_song_number++);
    /*strcpy(xxh->title, module_name);*/
    strcpy (module_type, "XXM");

    xxh->len = 1;
    xxh->rst = 0;
    xxh->chn = 8;
    xxh->pat = 1;
    xxh->trk = 8;
    xxh->tpo = 6;
    xxh->bpm = 120;
    xxh->flg = 0;

    xxo[0] = 0;

    PATTERN_INIT ();

/* make blank pattern */

    PATTERN_ALLOC (0);
    r = xxp[0]->rows = 0x40;
    TRACK_ALLOC (0);
    
    for (j = 0; j < (xxh->chn * r); j++) {
      event = &EVENT (0, j % xxh->chn, j / xxh->chn);
      event->note = 0;
      event->ins = 0;
      event->vol = 0;
      event->fxt = 0;
      event->fxp = 0;
      event->f2t = 0;
      event->f2p = 0;
    }

    xxh->ins = xxh->smp = 0;

    INSTRUMENT_INIT ();

    /* If dynamic pan is disabled, XM modules will use the standard
     * MOD channel panning (LRRL).
     */

    for (i = 0; i < xxh->chn; i++)
      xxc[i].pan = (((i + 1) / 2) % 2) * 0xff;

    return 0;
}
