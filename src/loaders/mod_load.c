/* Extended Module Player
 * Copyright (C) 1996-1998 Claudio Matsuoka and Hipolito Carraro Jr
 * $Id$
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See doc/COPYING
 * for more information.
 */

/* This loader recognizes the following variants of the Protracker 2.1
 * module format:
 *
 * - Protracker M.K. and M!K!
 * - Noisetracker N.T. and M&K! (not tested)
 * - Fast Tracker 6CHN and 8CHN
 * - Fasttracker II/Take Tracker ?CHN and ??CH
 * - Startrekker FLT4 and FLT8
 * - M.K. with ADPCM samples (MDZ)
 * - Soundtracker 15-instrument module
 * - Mod's Grave M.K. w/ 8 channels (WOW)
 * - Atari Octalyzer CD81 (not Oktalyzer!) (not tested)
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "load.h"
#include "mod.h"
#include "period.h"

static struct {
    char *magic;
    char *tracker;
    int ch;
} mod_magic[] = {
    { "M.K.", "Protracker", 4 },
    { "M!K!", "Protracker", 4 },
    { "M&K!", "Noisetracker", 4 },
    { "N.T.", "Protracker", 4 },
    { "FLT4", "Startrekker", 4 },
    { "FLT8", "Startrekker", 8 },
    { "6CHN", "Fast Tracker", 6 },
    { "8CHN", "Fast Tracker", 8 },
    { "CD81", "Octalyzer", 8 },
    { "", 0 }
};

static int module_load (FILE *, int);


int m15_load (FILE *f)
{
    return module_load (f, 15);
}


int mod_load (FILE *f)
{
    return module_load (f, 31);
}


static int module_load (FILE *f, int nins)
{
    int i, j;
    int smp_size, hdr_size, pat_size, wow;
    struct xxm_event *event;
    struct stat st;
    union mod_header mh;
    uint8 mod_event[4];
    char *id = "";

    LOAD_INIT ();

    fstat (fileno (f), &st);
    xxh->tpo = 6;
    xxh->bpm = 125;
    xxh->ins = nins;
    xxh->smp = xxh->ins;
    xxh->chn = 0;
    smp_size = 0;
    pat_size = 0;
    hdr_size = 0;

    if (xxh->ins > 15) {
	hdr_size = sizeof (struct m31_header);
	fread (&mh, 1, sizeof (struct m31_header), f);
	for (i = 0; mod_magic[i].ch; i++) {
	    if (!(strncmp ((char *) mh.m31.magic, mod_magic[i].magic, 4))) {
		xxh->chn = mod_magic[i].ch;
		id = mod_magic[i].tracker;
		break;
	    }
	}

	if (!xxh->chn) {
	    if (!strncmp ((char *) mh.m31.magic + 2, "CH", 2) &&
		isdigit (*mh.m31.magic) && isdigit (mh.m31.magic[1])) {
		if ((xxh->chn = (*mh.m31.magic - '0') *
			10 + mh.m31.magic[1] - '0') > 32)
		    return -1;
	    } else if (!strncmp ((char *) mh.m31.magic + 1, "CHN", 3) &&
		isdigit (*mh.m31.magic)) {
		if (!(xxh->chn = (*mh.m31.magic - '0')))
		    return -1;
	    } else
		return -1;
	    id = "Multichannel";
	}
    } else {
	hdr_size = sizeof (struct m15_header);
	fread (&mh, 1, sizeof (struct m15_header), f);
	id = "Soundtracker 15-instrument";
	xxh->chn = 4;

	if (mh.m15.ins[0].finetune > 0x0f)
	    return -1;
    }

    strncpy (module_name, (char *) mh.m31.name, 20);

    xxh->len = xxh->ins > 15 ? mh.m31.len : mh.m15.len;
    xxh->rst = xxh->ins > 15 ? mh.m31.restart : mh.m15.restart;
    if (xxh->rst >= xxh->len)
	xxh->rst = 0;
    memcpy (xxo, xxh->ins > 15 ? mh.m31.order : mh.m15.order, 128);

    /* Thunder's method to determine the number of patterns (shown below)
     * doesn't work for badly ripped mods. 
     *
     * xxh->pat=(st.st_size-smp_size-header_size)/(64*xxh->chn*4);
     */

    for (i = 0; i < 128; i++)
	if (xxo[i] > xxh->pat)
	    xxh->pat = xxo[i];
    xxh->pat++;

    if (xxh->pat > 0x7f || xxh->len == 0 || xxh->len > 0x7f)
	return -1;

    pat_size = 256 * xxh->chn * xxh->pat;

    for (i = 0; i < xxh->ins; i++) {
	B_ENDIAN16 (mh.m31.ins[i].size);
	B_ENDIAN16 (mh.m31.ins[i].loop_start);
	B_ENDIAN16 (mh.m31.ins[i].loop_size);

	if (mh.m31.ins[i].size > 0x8000 || mh.m31.ins[i].loop_start > 0x8000
		|| mh.m31.ins[i].loop_size > 0x8000)
	    return -1;
    }

    INSTRUMENT_INIT ();

    for (i = 0; i < xxh->ins; i++) {
	xxi[i] = xmp_calloc (sizeof (struct xxm_instrument), 1);
	xxs[i].len = 2 * mh.m31.ins[i].size;
	xxs[i].lps = 2 * mh.m31.ins[i].loop_start;
	xxs[i].lpe = xxs[i].lps + 2 * mh.m31.ins[i].loop_size;
	xxs[i].flg = mh.m31.ins[i].loop_size > 1 ? WAVE_LOOPING : 0;
	xxi[i][0].fin = (int8)(mh.m31.ins[i].finetune << 4);
	xxi[i][0].vol = mh.m31.ins[i].volume;
	xxi[i][0].pan = 0x80;
	xxi[i][0].sid = i;
	xxih[i].nsm = !!(xxs[i].len);
	xxih[i].rls = 0xfff;
	smp_size += xxs[i].len;
	strncpy (xxih[i].name, mh.m31.ins[i].name, 22);
	str_adj (xxih[i].name);
	strncpy (xxs[i].name, mh.m31.ins[i].name, 22);
	str_adj (xxs[i].name);
    }

    /* Another filter for Soundtracker modules */
    if (xxh->ins == 15 && sizeof (struct m15_header) + pat_size
	+ smp_size > st.st_size)
	return -1;

    /* Test for Mod's Grave WOW modules
     *
     * Stefan Danes <sdanes@marvels.hacktic.nl> said:
     * This weird format is identical to '8CHN' but still uses the 'M.K.' ID.
     * You can only test for WOW by calculating the size of the module for 8 
     * channels and comparing this to the actual module length. If it's equal, 
     * the module is an 8 channel WOW.
     */

    if ((wow = (!strncmp ((char *) mh.m31.magic, "M.K.", 4) &&
		(0x43c + xxh->pat * 8 * 4 * 0x40 + smp_size == st.st_size)))) {
	xxh->chn = 8;
	id = "Mod's Grave";
    }

    /* Test for UNIC tracker modules
     *
     * From Gryzor's Pro-Wizard PW_FORMATS-Engl.guide:
     * ``The UNIC format is very similar to Protracker... At least in the
     * heading... same length : 1084 bytes. Even the "M.K." is present,
     * sometimes !! Maybe to disturb the rippers.. hehe but Pro-Wizard
     * doesn't test this only!''
     *
     * And neither does xmp. Corrupted M.K.'s, however, will qualify as
     * valid UNIC modules -- they must be filtered out in the UNIC loader.
     */

    else if (xxh->ins == 31 && sizeof (struct m31_header) + xxh->pat * 0x300 +
	smp_size == st.st_size) {
	return -1;
    }

    xxh->trk = xxh->chn * xxh->pat;

    if (xxh->ins > 15)
	sprintf (module_type, "%s %4.4s", id, mh.m31.magic);
    else
	sprintf (module_type, "%s", id);

    MODULE_INFO ();

    for (i = 0; (opt.verbose > 1) && (i < xxh->ins); i++) {
	if ((strlen ((char *) xxih[i].name) || (xxs[i].len > 2)))
	    report ("[%2X] %-22.22s %04x %04x %04x %c V%02x %+d\n",
		i, xxih[i].name, xxs[i].len, xxs[i].lps,
		xxs[i].lpe, mh.m31.ins[i].loop_size > 1 ? 'L' : ' ',
		xxi[i][0].vol, (char) xxi[i][0].fin >> 4);
    }

    PATTERN_INIT ();

    /* Load and convert patterns */
    if (opt.verbose)
	report ("Stored patterns: %d ", xxh->pat);

    for (i = 0; i < xxh->pat; i++) {
	PATTERN_ALLOC (i);
	xxp[i]->rows = 64;
	TRACK_ALLOC (i);
	for (j = 0; j < (64 * xxh->chn); j++) {
	    event = &EVENT (i, j % xxh->chn, j / xxh->chn);
	    fread (mod_event, 1, 4, f);

	    event->note = period_to_note ((LSN (mod_event[0]) << 8) +
		mod_event[1]);
	    event->ins = ((MSN (mod_event[0]) << 4) | MSN (mod_event[2]));
	    event->fxt = LSN (mod_event[2]);
	    event->fxp = mod_event[3];

	    if (!event->fxp) {
		switch (event->fxt) {
		case 0x05:
		    event->fxt = 0x03;
		    break;
		case 0x06:
		    event->fxt = 0x04;
		    break;
		case 0x01:
		case 0x02:
		case 0x0a:
		    event->fxt = 0x00;
		}
	    }

	    /* Special translation for e8 (set panning) effect.
	     * This is not an official Protracker effect but DMP uses
	     * it for panning, and a couple of modules follow this
	     * "standard".
	     */
	    if ((event->fxt == 0x0e) && ((event->fxp & 0xf0) == 0x80)) {
		event->fxt = FX_SETPAN;
		event->fxp <<= 4;
	    }
	}
	if (opt.verbose)
	    report (".");
    }

    if (opt.modrange)
	xxh->flg |= XXM_FLG_MODRNG;
    if (opt.ntsc) {
	opt.rrate = NTSC_RATE;
	opt.c4rate = C4_NTSC_RATE;
    }

    /* Load samples */

    if (opt.verbose)
	report ("\nStored samples : %d ", xxh->smp);
    for (i = 0; i < xxh->smp; i++) {
	if (!xxs[i].len)
	    continue;
	drv_loadpatch (f, xxi[i][0].sid, opt.c4rate, 0,
	    &xxs[xxi[i][0].sid], NULL);
	if (opt.verbose)
	    report (".");
    }
    if (opt.verbose)
	report ("\n");

    for (i = 0; i < xxh->chn; i++)
	xxc[i].pan = (((i + 1) / 2) % 2) * 0xff;

    return 0;
}
