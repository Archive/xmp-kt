/* 
 * This is a silly sound server for OSS. I'm using this to test xmp in
 * machines with no sound device, like the RS/6000 boxes in inf.ufpr.br.
 * Basically it opens the sound device and listen for an incoming
 * connection in port 1313 -- everything it receives is then dumped to
 * the audio device.
 *
 * If you have any use for this, use '--enable-tcp-socket' when
 * configuring xmp. It will compile the network audio driver adding
 * drivers/net.o in Makefile.rules and #defining DEVICE_NET in include/
 * config.h. Then run xmp with "-dnet -Ds=server:port" to connect to
 * the server. Default values are localhost:1313.
 *
 *	$ xmp -dnet -Ds=foo:1313 -v MELTEDSU.LHA
 *	Trying 172.16.16.2...
 *	Connected to foo.bar.edu.
 *	Server at 22050Hz stereo, mode 0x00000005
 *
 *	Extended Module Player 1.1.0dev29 Sat Nov 29 10:03:00 EDT 1997
 *	Copyright (C) 1996,1997 Claudio Matsuoka and Hipolito Carraro Jr
 *	Using TCP socket [server 172.16.16.2, port 1313]
 *	Mixer set to 16bit, 22050 Hz, interpolated stereo
 *	Loading MELTEDSU.LHA...
 *	Module title   : melted surucucu
 *	Module type    : Protracker M.K. module
 *	...
 *
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <sys/soundcard.h>
#include <fcntl.h>

#include "config.h"
#include "xmpi.h"
#include "driver.h"



/**********************************************************************
 * OSS stuff (from drivers/oss_mix.c)
 **********************************************************************/

static int oss_afmt[] = {
    AFMT_U8, AFMT_S8, AFMT_U8, AFMT_S8,
    AFMT_U16_LE, AFMT_S16_LE, AFMT_U16_BE, AFMT_S16_BE
};

static int audio_fd;
static struct drv_config d;


static void set_audio (struct drv_config *cfg)
{
#if 0
    static int fragset = 0;
    int frag = 0;
#endif
    int i, afmt;

    /* frag = (fragnum << 16) + fragsize; */

    afmt = oss_afmt[cfg->fmt];
    ioctl (audio_fd, SNDCTL_DSP_SETFMT, &afmt);
    if (afmt != oss_afmt[cfg->fmt]) {
	for (i=0; afmt != oss_afmt[i]; i++);
	cfg->fmt = i;
    }

    ioctl (audio_fd, SNDCTL_DSP_STEREO, &cfg->mode);
    cfg->mode++;			/* 1=mono, 2=stereo */

    ioctl (audio_fd, SNDCTL_DSP_SPEED, &cfg->rate);

#if 0
    /* Set the fragments only once */
    if (fragnum && fragsize)
	ioctl (audio_fd, SNDCTL_DSP_SETFRAGMENT, &frag);
    fragset = 1;
#endif
}


int init_audio ()
{
    d.fmt = MIX_16BIT | MIX_SIGNED;
    d.mode = MIX_STEREO;
    d.rate = 22050;

    if ((audio_fd = open ("/dev/dsp", O_WRONLY)) == -1)
	return -1;

    set_audio (&d);

    return 0;
}


void write_audio (char *buf, int l)
{
    write (audio_fd, buf, l);
}


/**********************************************************************
 * TCP socket stuff
 **********************************************************************/

static int my_port = 1313;


#define write_sock(s,x,y,z) \
	{ sprintf (x, y, z); write (s, x, strlen (x)); }

int action (int s)
{
    int nbytes;
    char *buffer;

    buffer = malloc (4096);

    write_sock (s, buffer, "R: Sampling rate is %d\n", d.rate);
    write_sock (s, buffer, "C: Number of channels is %d\n", d.mode);
    write_sock (s, buffer, "F: Sample format is 0x%08x\n\n", d.fmt);

    while ((nbytes = read (s, buffer, 4096)) > 0) {
	write_audio (buffer, nbytes);
    }

    free (buffer);
    return 0;
}


int main (int argc, char **argv)
{
    int addr_size;
    int s, s2;
    struct sockaddr_in s_addr;
    struct sockaddr_in c_addr;
    pid_t sid;

    if (fork ()) {
	exit (0);
    }

    chdir ("/");
    sid = setsid ();
    printf ("PID = %d\n", sid);

    init_audio ();

    memset (&s_addr, 0, sizeof (struct sockaddr_in));

    s_addr.sin_port = htons (my_port);
    s_addr.sin_family = AF_INET;
    s_addr.sin_addr.s_addr = INADDR_ANY;
    addr_size = sizeof (struct sockaddr_in);

    if ((s = socket (PF_INET, SOCK_STREAM, 0)) < 0) {
	perror (argv[0]);
	exit (-1);
    }

    if (bind (s, (struct sockaddr*)&s_addr, sizeof (struct sockaddr_in)) < 0) {
	perror (argv[0]);
	exit (-1);
    }

    if (listen (s, 0) < 0) {
	perror (argv[0]);
	exit (-1);
    }

    while (42) {
	s2 = accept (s, (struct sockaddr *) &c_addr, &addr_size);
	action (s2);
	close (s2);
    }

    return 0;
}
