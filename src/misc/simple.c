/* A simple frontend for xmp */

#include <stdio.h>
#include <xmp.h>

static struct xmp_module_info mi;
static struct xmp_options opt;


static void process_echoback (unsigned long i)
{
    unsigned long msg = i >> 4;
    static int oldpos = -1;
    static int pos, pat;

    switch (i & 0xf) {
    case XMP_ECHO_ORD:
	pos = msg & 0xff;
	pat = msg >> 8;
	break;
    case XMP_ECHO_ROW:
	if (!(msg & 0xff) || pos != oldpos) {
	    printf ("\r[%s] Order %02X/%02X - Pattern %02X/%02X - Row      ",
		mi.title, pos, mi.len, pat, mi.pat - 1);
	    oldpos = pos;
	}
	printf ("\b\b\b\b\b%02X/%02X", (int)(msg & 0xff), (int)(msg >> 8));
	break;
    }
}


int main (int argc, char **argv)
{
    int i, t;

    setbuf (stdout, NULL);
    xmp_init (argc, argv, &opt);
    opt.verbose = 0;
    if ((i = xmp_open_audio (&opt)) < 0) {
	fprintf (stderr, "%s: can't open audio device", argv[0]);
	return -1;
    }

    printf ("Using libxmp version %s (%s)\n", __xmp_version, __xmp_date);
    xmp_register_event_callback (process_echoback);

    for (i = 1; i < argc; i++) {
	if (xmp_load_module (argv[i]) < 0) {
	    fprintf (stderr, "%s: error loading %s\n", argv[0], argv[i]);
	    continue;
	}
	xmp_get_module_info (&mi);
	t = xmp_play_module ();
	printf ("\n");
    }
    xmp_close_audio ();

    return 0;
}
