/* $Id$ */

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>


int main (int argc, char **argv)
{
	FILE *f;
	int i, c;
	static char buffer[256], buf2[2];

	if (argc < 2) {
		fprintf (stderr, "Usage: dump filename\n");
		exit (-1);
	}
	if ((f = fopen (argv[1], "r")) == NULL) {
		perror (*argv);
		exit (-1);
	}
	for (i = 0; (c = fgetc (f)) != -1; i++) {
		if (isprint (c)) {
			sprintf (buf2, "%c", c);
			strcat (buffer, buf2);
		} else
			strcat (buffer, ".");

		if (!(i % 16))
			printf ("%5d [%04x] ", i, i);
		printf ("%02x ", c);
		if ((i % 16) == 7)
			printf (" ");
		if ((i % 16) == 15) {
			printf (" %s\n", buffer);
			*buffer = 0;
		}
	}
	printf ("\n");
	fclose (f);
	return 0;
}
