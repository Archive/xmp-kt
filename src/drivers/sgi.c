/* Extended Module Player
 * Copyright (C) 1996-1998 Claudio Matsuoka and Hipolito Carraro Jr
 * $Id$
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See doc/COPYING
 * for more information.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <dmedia/audio.h>
#include <fcntl.h>

#include "xmpi.h"
#include "driver.h"
#include "mixer.h"

extern char *mixer_buffer;

static ALport audio_port;

static int init (struct drv_config *);
static int setaudio (struct drv_config *);
static void bufdump (void);
static void shutdown (void);

static void dummy () { }

/*
 * audio port sample rates (these are the only ones supported by the library)
 */

static int srate[] = {
    48000,
    44100,
    32000,
    22050,
    16000,
    11025,
    8000,
    0
};

static char *help[] = {
    "buffer=val", "Audio buffer size",
    NULL
};


struct xmp_drv_info drv_sgi = {
    "sgi",		/* driver ID */
    "SGI audio device",	/* driver description */
    help,		/* help */
    init,		/* init */
    shutdown,		/* shutdown */
    smix_numvoices,	/* numvoices */
    smix_voicepos,	/* voicepos */
    smix_echoback,	/* echoback */
    smix_setpatch,	/* setpatch */
    smix_setvol,	/* setvol */
    smix_setnote,	/* setnote */
    smix_setpan,	/* setpan */
    smix_setbend,	/* setbend */
    dummy,		/* starttimer */
    dummy,		/* stoptimer */
    dummy,		/* reset */
    bufdump,		/* bufdump */
    dummy,		/* bufwipe */
    dummy,		/* clearmem */
    dummy,		/* sync */
    smix_writepatch,	/* writepatch */
    smix_getmsg,	/* getmsg */
    NULL
};


static int setaudio (struct drv_config *cfg)
{
    int bsize = 32 * 1024;
    ALconfig config;
    long pvbuffer[2];
    char *token;
    char **parm = cfg->parm;
    int i;

    parm_init ();
    chkparm1 ("buffer", bsize = strtoul (token, NULL, 0));
    parm_end ();


    if ((config = ALnewconfig ()) == 0)
	return 0;

    /*
     * Set sampling rate
     */

    pvbuffer[0] = AL_OUTPUT_RATE;

    for (i = 0; srate[i]; i++) {
        if (srate[i] <= cfg->rate)
	    pvbuffer[1] = cfg->rate = srate[i];
    }

    if (i == 0)
	pvbuffer[1] = cfg->rate = srate[0];		/* 48 kHz. Wow! */

    if (ALsetparams (AL_DEFAULT_DEVICE, pvbuffer, 2) < 0)
	return 0;

    /*
     * Set sample format to signed integer
     */

    if (ALsetsampfmt (config, AL_SMPFMT_TWOSCOMP) < 0)
	return 0;

    /*
     * Set sample width; 24 bit samples are not currently supported by xmp
     */

    if (cfg->fmt & MIX_16BIT) {
	if (ALsetwidth (config, AL_SAMPLE_16) < 0) {
	    if (ALsetwidth (config, AL_SAMPLE_8) < 0)
		return 0;
	    cfg->fmt &= ~MIX_16BIT;
	}
    } else {
	if (ALsetwidth (config, AL_SAMPLE_8) < 0) {
	    if (ALsetwidth (config, AL_SAMPLE_16) < 0)
		return 0;
	    cfg->fmt |= MIX_16BIT;
	}
    }

    /*
     * Set number of channels; 4 channel output is not currently supported
     */

    if (cfg->mode == MIX_MONO) {
	if (ALsetchannels (config, AL_MONO) < 0) {
	    if (ALsetchannels (config, AL_STEREO) < 0)
		return 0;
	    cfg->mode = MIX_STEREO;
	}
    } else {
	if (ALsetchannels (config, AL_STEREO) < 0) {
	    if (ALsetchannels (config, AL_MONO) < 0)
		return 0;
	    cfg->mode = MIX_MONO;
	}
    }

    /*
     * Set buffer size
     */

    if (ALsetqueuesize (config, bsize) < 0)
	return 0;

    /*
     * Open the audio port
     */

    if ((audio_port = ALopenport ("xmp", "w", config)) == 0)
	return 0;

    return 1;
}
	

static int init (struct drv_config *cfg)
{
    if (cfg->nbuf < 2)
        cfg->nbuf = 2;

    cfg->fmt &= 0xffff;

    if (!setaudio (cfg))
	return 0;

    smix_init (cfg);

    return 1;
}


/* Build and write one tick (one PAL frame or 1/50 s in standard vblank
 * timed mods) of audio data to the output device.
 */
static void bufdump ()
{
    int i;

    i = smix_mixer ();
    ALwritesamps (audio_port, mixer_buffer, i);
}


static void shutdown ()
{
    smix_close ();
    ALcloseport (audio_port);
}
