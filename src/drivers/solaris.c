/* Extended Module Player
 * Copyright (C) 1996-1998 Claudio Matsuoka and Hipolito Carraro Jr
 * $Id$
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See doc/COPYING
 * for more information.
 */

/* CS4231 code tested on Sparc 20 and Ultra 1 running Solaris 2.5.1
 * with mono/stereo, 16 bit, 22.05 kHz and 44.1 kHz using the internal
 * speaker and headphones.
 *
 * AMD 7930 code tested on Axil 240 running Solaris 2.5.1 and an Axil
 * 220 running Linux 2.0.33.
 */

#include "config.h"

#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#if defined(HAVE_SYS_AUDIOIO_H)
#include <sys/audioio.h>
#elif defined(HAVE_SYS_AUDIO_IO_H)
#include <sys/audio.io.h>
#elif defined(HAVE_SUN_AUDIOIO_H)
#include <sun/audioio.h>
#endif

/* This is for Linux on Sparc */
#if defined(HAVE_SBUS_AUDIO_AUDIO_H)
#include <sbus/audio/audio.h>
#endif

#include "xmpi.h"
#include "driver.h"
#include "mixer.h"

extern char *mixer_buffer;

static int audio_fd;

static int init (struct drv_config *);
static int setaudio (struct drv_config *);
static void bufdump (void);
static void shutdown (void);

static void dummy () { }


static char *help[] = {
    "gain=val", "Audio output gain (0 to 255)",
    "port={s|h|l}", "Audio port (s[peaker], h[eadphones], l[ineout])",
    "buffer=val", "Audio buffer size (default is 32768)",
    NULL
};

struct xmp_drv_info drv_solaris = {
    "solaris",		/* driver ID */
    "Solaris audio device",	/* driver description */
    help,		/* help */
    init,		/* init */
    shutdown,		/* shutdown */
    smix_numvoices,	/* numvoices */
    smix_voicepos,	/* voicepos */
    smix_echoback,	/* echoback */
    smix_setpatch,	/* setpatch */
    smix_setvol,	/* setvol */
    smix_setnote,	/* setnote */
    smix_setpan,	/* setpan */
    smix_setbend,	/* setbend */
    dummy,		/* starttimer */
    dummy,		/* stoptimer */
    dummy,		/* reset */
    bufdump,		/* bufdump */
    dummy,		/* bufwipe */
    dummy,		/* clearmem */
    dummy,		/* sync */
    smix_writepatch,	/* writepatch */
    smix_getmsg,	/* getmsg */
    NULL
};


static int setaudio (struct drv_config *cfg)
{
    audio_info_t ainfo, ainfo2;
    int gain = 128;
    int bsize = 32 * 1024;
    int port = AUDIO_SPEAKER;
    char *token;
    char **parm = cfg->parm;

    parm_init ();
    chkparm1 ("gain", gain = atoi (token));
    chkparm1 ("buffer", bsize = atoi (token));
    chkparm1 ("port", port = (int)*token)
    parm_end ();

    switch (port) {
    case 'h':
	port = AUDIO_HEADPHONE;
	break;
    case 'l':
	port = AUDIO_LINE_OUT;
	break;
    case 's':
    default :
	port = AUDIO_SPEAKER;
    }

    if (gain < AUDIO_MIN_GAIN)
	gain = AUDIO_MIN_GAIN;
    if (gain > AUDIO_MAX_GAIN)
	gain = AUDIO_MAX_GAIN;

    AUDIO_INITINFO (&ainfo);		/* For CS4231 */
    AUDIO_INITINFO (&ainfo2);		/* For AMD 7930 if needed */

    ainfo.play.sample_rate = cfg->rate;
    ainfo.play.channels = cfg->mode;
    ainfo.play.precision = cfg->fmt & MIX_16BIT ? 16 : 8;
    ainfo.play.encoding = AUDIO_ENCODING_LINEAR;
    ainfo2.play.gain = ainfo.play.gain = gain;
    ainfo2.play.port = ainfo.play.port = port;
    ainfo2.play.buffer_size = ainfo.play.buffer_size = bsize;

    if (ioctl (audio_fd, AUDIO_SETINFO, &ainfo) == -1) {
	/* CS4231 initialization Failed, perhaps we have an AMD 7930 */
	if (ioctl (audio_fd, AUDIO_SETINFO, &ainfo2) == -1) {
	    close (audio_fd);
	    return 0;
	}
	
	cfg->rate = 8000;
	cfg->mode = 0;
	cfg->fmt &= ~MIX_16BIT;
	cfg->fmt |= MIX_ULAW;
	drv_solaris.description = "Solaris AMD7930 audio device";
    } else
	drv_solaris.description = "Solaris CS4231 audio device";

    return 1;
}


static int init (struct drv_config *cfg)
{
    audio_fd = open ("/dev/audio", O_WRONLY);

    if (audio_fd == -1)
	return 0;

    if (cfg->nbuf < 2)
	cfg->nbuf = 2;

    cfg->fmt &= 0xffff;

    if (!setaudio (cfg))
	return 0;

    smix_init (cfg);

    return 1;
}


/* Build and write one tick (one PAL frame or 1/50 s in standard vblank
 * timed mods) of audio data to the output device.
 */
static void bufdump ()
{
    register int i;

    for (i = smix_mixer (); i -= write (audio_fd, mixer_buffer, i); );
}


static void shutdown ()
{
    smix_close ();
    close (audio_fd);
}
