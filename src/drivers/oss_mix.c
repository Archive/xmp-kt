/* Extended Module Player
 * Copyright (C) 1996-1998 Claudio Matsuoka and Hipolito Carraro Jr
 * $Id$
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See doc/COPYING
 * for more information.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <string.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>

#if defined(HAVE_SYS_SOUNDCARD_H)
#include <sys/soundcard.h>
#elif defined(HAVE_MACHINE_SOUNDCARD_H)
#include <machine/soundcard.h>
#endif

#include "xmpi.h"
#include "driver.h"
#include "mixer.h"

extern char *mixer_buffer;

static int audio_fd;

static void bufdump (void);
static void shutdown (void);
static void flush (void);
static int init (struct drv_config *);
static void setaudio (struct drv_config *);

static void dummy () { }


static char *help[] = {
    "frag=num,size", "Set the number and size of fragments",
    "dev=<device_name>", "Audio device to use (default /dev/dsp)",
    "nosync", "Don't flush OSS buffers between modules",
#ifdef HAVE_AUDIO_BUF_INFO
    "voxware", "For VoxWare 2.90 (used in Linux 1.2.13)",
#endif
    NULL
};


struct xmp_drv_info drv_oss_mix = {
    "oss_mix",		/* driver ID */
    "OSS audio device",	/* driver description */
    help,		/* help */
    init,		/* init */
    shutdown,		/* shutdown */
    smix_numvoices,	/* numvoices */
    smix_voicepos,	/* voicepos */
    smix_echoback,	/* echoback */
    smix_setpatch,	/* setpatch */
    smix_setvol,	/* setvol */
    smix_setnote,	/* setnote */
    smix_setpan,	/* setpan */
    smix_setbend,	/* setbend */
    dummy,		/* starttimer */
    flush,		/* stoptimer */
    dummy,		/* reset */
    bufdump,		/* bufdump */
    dummy,		/* bufwipe */
    dummy,		/* clearmem */
    dummy,		/* sync */
    smix_writepatch,	/* writepatch */
    smix_getmsg,	/* getmsg */
    NULL
};


static int oss_afmt[] = {
    AFMT_U8,
    AFMT_S8,
    AFMT_U8,
    AFMT_S8,
    AFMT_U16_LE,
    AFMT_S16_LE,
    AFMT_U16_BE,
    AFMT_S16_BE
};

static char *dev_audio = "/dev/dsp";
static int fragnum, fragsize;
static int do_sync = 1;
#ifdef HAVE_AUDIO_BUF_INFO
static int voxware = 0;		/* For Linux 1.2.13 */
#endif


static void setaudio (struct drv_config *cfg)
{
    static int fragset = 0;
    int frag = 0;
    int i, afmt;

    frag = (fragnum << 16) + fragsize;

    afmt = oss_afmt[cfg->fmt];
    ioctl (audio_fd, SNDCTL_DSP_SETFMT, &afmt);
    if (afmt != oss_afmt[cfg->fmt]) {
	for (i = 0; afmt != oss_afmt[i]; i++);
	cfg->fmt = i;
    }

    i = cfg->mode - 1;
    ioctl (audio_fd, SNDCTL_DSP_STEREO, &i);
    ioctl (audio_fd, SNDCTL_DSP_SPEED, &cfg->rate);

    /* Set the fragments only once */
    if (!fragset) {
	if (fragnum && fragsize)
	    ioctl (audio_fd, SNDCTL_DSP_SETFRAGMENT, &frag);
	fragset = 1;
	return;
    }
}


static int init (struct drv_config *cfg)
{
#ifdef HAVE_AUDIO_BUF_INFO
    audio_buf_info info;
    static char buf[80];
#endif
    char *token, **parm;
    int i = 1024;

    parm_init ();
    chkparm2 ("frag", "%d,%d", &fragnum, &i);
    chkparm1 ("dev", dev_audio = token);
#ifdef HAVE_AUDIO_BUF_INFO
    chkparm1 ("voxware", voxware = 1);
#endif
    chkparm1 ("nosync", do_sync = 0);
    parm_end ();

    for (fragsize = 0; i >>= 1; fragsize++);
    if (fragsize < 4)
	fragsize = 4;
    if (fragnum > 1)
	fragnum--;

    if ((audio_fd = open (dev_audio, O_WRONLY)) == -1)
	return 0;

    if (cfg->nbuf < 2)
	cfg->nbuf = 2;

    cfg->fmt &= 0xffff;

    setaudio (cfg);

    smix_init (cfg);

#ifdef HAVE_AUDIO_BUF_INFO
    if (!voxware) {
	ioctl (audio_fd, SNDCTL_DSP_GETOSPACE, &info);

	snprintf (buf, 80, "%s [%d fragments of %d bytes]",
	    drv_oss_mix.description, info.fragstotal, info.fragsize);
	drv_oss_mix.description = buf;
    }
#endif

    return 1;
}


/* Build and write one tick (one PAL frame or 1/50 s in standard vblank
 * timed mods) of audio data to the output device.
 */
static void bufdump ()
{
    int i;

    for (i = smix_mixer (); i -= write (audio_fd, mixer_buffer, i); );
}


static void shutdown ()
{
    smix_close ();
    close (audio_fd);
}


static void flush ()
{
    if (!do_sync)
	return;

    ioctl (audio_fd, SNDCTL_DSP_SYNC);
}
