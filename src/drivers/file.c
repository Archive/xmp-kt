/* Extended Module Player
 * Copyright (C) 1996-1998 Claudio Matsuoka and Hipolito Carraro Jr
 * $Id$
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See doc/COPYING
 * for more information.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "xmpi.h"
#include "driver.h"
#include "mixer.h"

extern char *mixer_buffer;

static int audio_fd;

static int init (struct drv_config *);
static void bufdump ();
static void shutdown ();

static void dummy () { }

struct xmp_drv_info drv_file = {
    "file",		/* driver ID */
    "file",		/* driver description */
    NULL,		/* help */
    init,		/* init */
    shutdown,		/* shutdown */
    smix_numvoices,	/* numvoices */
    smix_voicepos,	/* voicepos */
    smix_echoback,	/* echoback */
    smix_setpatch,	/* setpatch */
    smix_setvol,	/* setvol */
    smix_setnote,	/* setnote */
    smix_setpan,	/* setpan */
    smix_setbend,	/* setbend */
    dummy,		/* starttimer */
    dummy,		/* stoptimer */
    dummy,		/* resetvoices */
    bufdump,		/* bufdump */
    dummy,		/* bufwipe */
    dummy,		/* clearmem */
    dummy,		/* sync */
    smix_writepatch,	/* writepatch */
    smix_getmsg,	/* getmsg */
    NULL
};


static int init (struct drv_config *cfg)
{
    char *buf;

    if (!opt.outfile)
	opt.outfile = "xmp.out";

    audio_fd = strcmp (opt.outfile, "-") ? creat (opt.outfile, 0644) : 1;

    smix_init (cfg);

    buf = malloc (strlen (drv_file.description) + strlen (opt.outfile) + 8);
    if (strcmp (opt.outfile, "-")) {
	sprintf (buf, "%s: %s", drv_file.description, opt.outfile);
	drv_file.description = buf;
    } else {
	drv_file.description = "Output to stdout";
    }

    return 1;
}


static void bufdump ()
{
    int i;

    for (i = smix_mixer (); i -= write (audio_fd, mixer_buffer, i); );
}


static void shutdown ()
{
    smix_close ();

    if (audio_fd)
	close (audio_fd);
}
