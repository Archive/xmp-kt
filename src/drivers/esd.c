/* Extended Module Player
 * Copyright (C) 1996-1998 Claudio Matsuoka and Hipolito Carraro Jr
 * $Id$
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See doc/COPYING
 * for more information.
 */

/* Based on esdcat.c from the Enlightened Sound Daemon 0.2 for Linux
 * More details at http://www.netcom.com/~ericmit/EsounD.html
 */

#include "config.h"

#include "xmpi.h"
#include "driver.h"
#include "mixer.h"
#include "esd.h"

extern char *mixer_buffer;

static int audio_fd = -1;

static int init (struct drv_config *);
static void bufdump ();
static void myshutdown ();

static void dummy () { }


struct xmp_drv_info drv_esd = {
    "esd",		/* driver ID */
    "Enlightened Sound Daemon",	/* driver description */
    NULL,		/* help */
    init,		/* init */
    myshutdown,		/* shutdown */
    smix_numvoices,	/* numvoices */
    smix_voicepos,	/* voicepos */
    smix_echoback,	/* echoback */
    smix_setpatch,	/* setpatch */
    smix_setvol,	/* setvol */
    smix_setnote,	/* setnote */
    smix_setpan,	/* setpan */
    smix_setbend,	/* setbend */
    dummy,		/* starttimer */
    dummy,		/* stoptimer */
    dummy,		/* resetvoices */
    bufdump,		/* bufdump */
    dummy,		/* bufwipe */
    dummy,		/* clearmem */
    dummy,		/* sync */
    smix_writepatch,	/* writepatch */
    smix_getmsg,	/* getmsg */
    NULL
};


static int init (struct drv_config *cfg)
{
    int format, rate = 44100;
    int bits = ESD_BITS16, channels = ESD_STEREO;
    int mode = ESD_STREAM, func = ESD_PLAY ;

    if (~cfg->fmt & MIX_16BIT)
	bits = ESD_BITS8;
    if (cfg->mode == MIX_MONO)
	channels = ESD_MONO;
    rate = cfg->rate;
    format = bits | channels | mode | func;

    printf( "opening socket, format = 0x%08x at %d Hz\n", format, rate );

    if ((audio_fd = esd_play_stream (format, rate)) <= 0) {
	fprintf (stderr, "drv_esd: unable to connect to server\n");
	return 0;
    }

    smix_init (cfg);

    return 1;
}


static void bufdump ()
{
    int i;

    for (i = smix_mixer (); i -= write (audio_fd, mixer_buffer, i); );
}


static void myshutdown ()
{
    smix_close ();

    if (audio_fd)
	close (audio_fd);
}

