/* Extended Module Player
 * Copyright (C) 1996-1998 Claudio Matsuoka and Hipolito Carraro Jr
 * $Id$
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See doc/COPYING
 * for more information.
 */

/* This code was tested on a 9000/710 running HP-UX 9.05 with 8 kHz,
 * 16 bit mono output.
 */

#include "config.h"

#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/audio.h>
#include <fcntl.h>

#include "xmpi.h"
#include "driver.h"
#include "mixer.h"

extern char *mixer_buffer;

static int audio_fd;

static int init (struct drv_config *);
static int setaudio (struct drv_config *);
static void bufdump (void);
static void shutdown (void);

static void dummy () { }

/* Standard sampling rates */
static int srate[] = {
    44100, 32000, 22050, 16000, 11025, 8000, 0
};

static char *help[] = {
    "gain=val", "Audio output gain (0 to 255)",
    "port={s|h|l}", "Audio port (s[peaker], h[eadphones], l[ineout])",
    "buffer=val", "Audio buffer size",
    NULL
};


struct xmp_drv_info drv_hpux = {
    "hpux",		/* driver ID */
    "HP-UX audio device",	/* driver description */
    help,		/* help */
    init,		/* init */
    shutdown,		/* shutdown */
    smix_numvoices,	/* numvoices */
    smix_voicepos,	/* voicepos */
    smix_echoback,	/* echoback */
    smix_setpatch,	/* setpatch */
    smix_setvol,	/* setvol */
    smix_setnote,	/* setnote */
    smix_setpan,	/* setpan */
    smix_setbend,	/* setbend */
    dummy,		/* starttimer */
    dummy,		/* stoptimer */
    dummy,		/* reset */
    bufdump,		/* bufdump */
    dummy,		/* bufwipe */
    dummy,		/* clearmem */
    dummy,		/* sync */
    smix_writepatch,	/* writepatch */
    smix_getmsg,	/* getmsg */
    NULL
};


static int setaudio (struct drv_config *cfg)
{
    int flags;
    int gain = 128;
    int bsize = 32 * 1024;
    int port = AUDIO_OUT_SPEAKER;
    struct audio_gains agains;
    struct audio_describe adescribe;
    char *token;
    char **parm = cfg->parm;
    int i;

    parm_init ();
    chkparm1 ("gain", gain = strtoul (token, NULL, 0));
    chkparm1 ("buffer", bsize = strtoul (token, NULL, 0));
    chkparm1 ("port", port = (int)*token)
    parm_end ();

    switch (port) {
    case 'h':
	port = AUDIO_OUT_HEADPHONE;
	break;
    case 'l':
        port = AUDIO_OUT_LINE;
        break;
    default :
        port = AUDIO_OUT_SPEAKER;
    }

    flags = fcntl (audio_fd, F_GETFL, 0);

    if (flags < 0)
	return 0;

    flags |= O_NDELAY;
    fcntl (audio_fd, F_SETFL, flags);

    if (flags < 0)
	return 0;

    if (ioctl (audio_fd, AUDIO_SET_DATA_FORMAT, AUDIO_FORMAT_LINEAR16BIT) == -1)
	return 0;

    if (ioctl (audio_fd, AUDIO_SET_CHANNELS, cfg->mode) == -1) {
	cfg->mode = cfg->mode == MIX_MONO ? MIX_STEREO : MIX_MONO;
        if (ioctl (audio_fd, AUDIO_SET_CHANNELS, cfg->mode) == -1)
	    return 0;
    }

    ioctl (audio_fd, AUDIO_SET_OUTPUT, port);

    for (i = 0; ioctl (audio_fd, AUDIO_SET_SAMPLE_RATE, cfg->rate) == -1; i++) {
	if ((cfg->rate = srate[i]) == 0)
	    return 0;
    }

    if (ioctl (audio_fd, AUDIO_DESCRIBE, &adescribe) == -1)
	return 0;

    if (ioctl (audio_fd, AUDIO_GET_GAINS, &agains) == -1)
	return 0;

    agains.transmit_gain = adescribe.min_transmit_gain +
	(adescribe.max_transmit_gain - adescribe.min_transmit_gain) *
	gain / 256;
		
    if (ioctl (audio_fd, AUDIO_SET_GAINS, &agains) == -1)
	return 0;

    ioctl(audio_fd, AUDIO_SET_TXBUFSIZE, bsize);

    return 1;
}
	

static int init (struct drv_config *cfg)
{
    audio_fd = open ("/dev/audio", O_WRONLY);

    if (audio_fd == -1)
	return 0;

    if (cfg->nbuf < 2)
        cfg->nbuf = 2;

    cfg->fmt &= 0xffff;

    if (!setaudio (cfg))
	return 0;

    smix_init (cfg);

    return 1;
}


/* Build and write one tick (one PAL frame or 1/50 s in standard vblank
 * timed mods) of audio data to the output device.
 */
static void bufdump ()
{
    int i;

    for (i = smix_mixer (); i -= write (audio_fd, mixer_buffer, i); );
}


static void shutdown ()
{
    smix_close ();
    close (audio_fd);
}
