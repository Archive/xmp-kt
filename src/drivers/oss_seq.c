/* Extended Module Player
 * Copyright (C) 1996-1998 Claudio Matsuoka and Hipolito Carraro Jr
 * $Id$
 * AWE32 support Copyright (C) 1996, 1997 Takashi Iwai
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See doc/COPYING
 * for more information.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#define USE_SEQ_MACROS

#include <string.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>

#if defined(HAVE_SYS_SOUNDCARD_H)
#include <sys/soundcard.h>
#elif defined(HAVE_MACHINE_SOUNDCARD_H)
#include <machine/soundcard.h>
#endif

#if defined(HAVE_SYS_ULTRASOUND_H)
#include <sys/ultrasound.h>
#elif defined(HAVE_LINUX_ULTRASOUND_H)
#include <linux/ultrasound.h>
#elif defined(HAVE_MACHINE_ULTRASOUND_H)
#include <machine/ultrasound.h>
#endif

#if defined(HAVE_AWE_VOICE_H)
#include <awe_voice.h>
#elif defined(HAVE_SYS_AWE_VOICE_H)
#include <sys/awe_voice.h>
#elif defined(HAVE_LINUX_AWE_VOICE_H)
#include <linux/awe_voice.h>
#endif

#include "xmpi.h"
#include "driver.h"

#if !defined (DISABLE_AWE) && (defined (HAVE_AWE_VOICE_H) || \
	defined (HAVE_SYS_AWE_VOICE_H) || defined (HAVE_LINUX_AWE_VOICE_H))
#define AWE_DEVICE
#endif

SEQ_DEFINEBUF(2048);

extern int __mode_fm;

static int __echo_msg;
static int seqfd = -1;

static void numvoices	(int);
static void voicepos	(int, int);
static void echoback	(int);
static void setpatch	(int, int);
static void setvol	(int, int);
static void setnote	(int, int);
static void setpan	(int, int);
static void setbend	(int, int);
static void starttimer	(void);
static void stoptimer	(void);
static void resetvoices	(void);
static void bufdump	(void);
static void bufwipe	(void);
static void clearmem	(void);
static void seq_sync	(double);
static void writepatch	(struct patch_info *);
static int init		(struct drv_config *);
static int getmsg	(void);
static void shutdown	(void);

#define seqbuf_dump bufdump


static char *help[] = {
    "chorus=mode,level", "Set chorus mode and level in AWE cards",
    "dev=<device_name>", "Device to use (default /dev/sequencer)",
    "opl2", "Use OPL2 FM synthesizer",
    "reverb=mode,level", "Set reverb mode and level in AWE cards",
    NULL
};


struct xmp_drv_info drv_oss_seq = {
    "oss_seq",		/* driver ID */
    "OSS sequencer",	/* driver description */
    help,		/* help */
    init,		/* init */
    shutdown,		/* shutdown */
    numvoices,		/* numvoices */
    voicepos,		/* voicepos */
    echoback,		/* echoback */
    setpatch,		/* setpatch */
    setvol,		/* setvol */
    setnote,		/* setnote */
    setpan,		/* setpan */
    setbend,		/* setbend */
    starttimer,		/* settimer */
    stoptimer,		/* stoptimer */
    resetvoices,	/* resetvoices */
    bufdump,		/* bufdump */
    bufwipe,		/* bufwipe */
    clearmem,		/* clearmem */
    seq_sync,		/* sync */
    writepatch,		/* writepatch */
    getmsg,		/* getmsg */
    NULL
};


static int dev;
static struct synth_info si;
static int chorusmode = 0, choruslevel = 0;
static int reverbmode = 0, reverblevel = 0;
static char *dev_sequencer = "/dev/sequencer";


static void numvoices (int n)
{
    if (si.synth_subtype == SAMPLE_TYPE_GUS) {
	if (n < 14)
	    n = 14;
	GUS_NUMVOICES (dev, n);
    }
}


static void voicepos (int ch, int i)
{
    GUS_VOICE_POS (dev, ch, i);
}


static void echoback (int i)
{
    SEQ_ECHO_BACK (i);
}


static void setpatch (int ch, int n)
{
    SEQ_SET_PATCH (dev, ch, n);
}


static void setvol (int ch, int vol)
{
    SEQ_START_NOTE (dev, ch, 255, vol);
}


static void setnote (int ch, int note)
{
#ifdef AWE_DEVICE
    /* AWE chorus and reverb handling by Takashi Iwai */
    if (si.synth_subtype == SAMPLE_TYPE_AWE32) {
	SEQ_STOP_NOTE (dev, ch, note, 0);
	/* chorus & reverb level: 0 - 255 */
	AWE_SEND_EFFECT (dev, ch, AWE_FX_CHORUS, choruslevel);
	AWE_SEND_EFFECT (dev, ch, AWE_FX_REVERB, reverblevel);
    }
#endif
    SEQ_START_NOTE (dev, ch, note, 0);
}


static void setpan (int ch, int pan)
{
    GUS_VOICEBALA (dev, ch, (pan + 0x80) >> 4)
}


static void setbend (int ch, int bend)
{
    SEQ_PITCHBEND (dev, ch, bend);
}


static void starttimer ()
{
    SEQ_START_TIMER ();
    seq_sync (0);
    bufdump ();
}


static void stoptimer ()
{
    SEQ_STOP_TIMER ();
    bufdump ();
}


static void resetvoices ()
{
    int i;

#ifdef AWE_DEVICE
    if (si.synth_subtype == SAMPLE_TYPE_AWE32) {
	if (chorusmode) {
	    AWE_CHORUS_MODE (dev, chorusmode);
	}
	if (reverbmode) {
	    AWE_REVERB_MODE (dev, reverbmode);
	}
    }
#endif
    for (i = 0; i < 32; i++) {
	SEQ_STOP_NOTE (dev, i, 255, 0);
	SEQ_EXPRESSION (dev, i, 255);
	SEQ_MAIN_VOLUME (dev, i, 100);
	SEQ_CONTROL (dev, i, CTRL_PITCH_BENDER_RANGE, 8191);
	SEQ_BENDER (dev, i, 0);
	SEQ_PANNING (dev, i, 0);
	bufdump ();
    }
}


static void bufwipe ()
{
    bufdump ();
    ioctl (seqfd, SNDCTL_SEQ_RESET, 0);
    _seqbufptr = 0;
}


static void bufdump ()
{
    int i, j;
    fd_set rfds, wfds;

    FD_ZERO (&rfds);
    FD_ZERO (&wfds);

    do {
	FD_SET (seqfd, &rfds);
	FD_SET (seqfd, &wfds);
	select (seqfd + 1, &rfds, &wfds, NULL, NULL);

	if (FD_ISSET (seqfd, &rfds)) {
	    if ((read (seqfd, &__echo_msg, 4) == 4) &&
		((__echo_msg & 0xff) == SEQ_ECHO)) {
		__echo_msg >>= 8;
		__event_callback (__echo_msg);
	    } else
		__echo_msg = 0;		/* ECHO_NONE */
	}

	if (FD_ISSET (seqfd, &wfds) && ((j = _seqbufptr) != 0)) {
	    if ((i = write (seqfd, _seqbuf, _seqbufptr)) == -1) {
		fprintf (stderr, "xmp: can't write to sequencer\n");
		exit (-4);
	    } else if (i < j) {
		_seqbufptr -= i;
		memmove (_seqbuf, _seqbuf + i, _seqbufptr);
	    } else
		_seqbufptr = 0;
	}
    } while (_seqbufptr);
}


static void clearmem ()
{
    int i = dev;

    ioctl (seqfd, SNDCTL_SEQ_RESETSAMPLES, &i);
}


static void seq_sync (double next_time)
{
    static double this_time = 0;

    if (next_time == 0)
	this_time = 0;

    if (next_time > this_time) {
	SEQ_WAIT_TIME (next_time);
	this_time = next_time;
    }
}


static void writepatch (struct patch_info *patch)
{
    struct sbi_instrument sbi;

    if (!patch) {
	clearmem ();
	return;
    }

    if (__mode_fm ^ (patch->len == -1))
	return;

    if (patch->len == -1) {
	sbi.key = FM_PATCH;
	sbi.device = dev;
	sbi.channel = patch->instr_no;
	memcpy (&sbi.operators, patch->data, 11);
	write (seqfd, &sbi, sizeof (sbi));
    }

    patch->device_no = dev;
    SEQ_WRPATCH (patch, sizeof (*patch) + patch->len - 1);

    if (patch->len != -1)
	free (patch);
}


static int getmsg ()
{
    return __echo_msg;
}


static int init (struct drv_config *cfg)
{
    int found;
    char *buf, *token;
    char **parm;

    buf = calloc (1, 256);

    parm_init ();
    chkparm2 ("chorus", "%d,%d", &chorusmode, &choruslevel);
    chkparm2 ("reverb", "%d,%d", &reverbmode, &reverblevel);
    chkparm1 ("opl2", __mode_fm = 1);
    chkparm1 ("dev", dev_sequencer = token);
    parm_end ();

    if ((seqfd = open (dev_sequencer, O_RDWR)) != -1) {
	if ((seqfd != -1) && (ioctl (seqfd, SNDCTL_SEQ_NRSYNTHS, &dev) == -1)) {
	    fprintf (stderr, "xmp: can't determine number of synths\n");
	    return 0;
	}
    } else {
	if (opt.verbose > 2)
	    fprintf (stderr, "xmp: can't open sequencer\n");
	return 0;
    }

    for (found = 0; dev--; ) {
	si.device = dev;

	if (ioctl (seqfd, SNDCTL_SYNTH_INFO, &si) == -1) {
	    fprintf (stderr, "xmp: can't determine synth info\n");
	    return 0;
	}

	if (si.synth_type == (__mode_fm ? SYNTH_TYPE_FM : SYNTH_TYPE_SAMPLE)) {
	    if (!__mode_fm) {
		int i = dev;
		ioctl(seqfd, SNDCTL_SEQ_RESETSAMPLES, &i);
		i = dev;
		ioctl(seqfd, SNDCTL_SYNTH_MEMAVL, &i);
		if (!i) {
		    continue;
		}
	    }

	    sprintf (buf, "%s [%s]", drv_oss_seq.description, si.name);
	    if (si.synth_subtype == 0x20) {
		strcat (buf,
		    "\nAWE support Copyright (C) 1996,1997 by Takashi Iwai");
	    }
	
	    drv_oss_seq.description = buf;

	    cfg->rate = 0;
	    found = 1;
	    break;
	}
    }

    if (!found) {
	close (seqfd);
	return 0;
    }

    SEQ_VOLUME_MODE (dev, VOL_METHOD_LINEAR);
    bufdump ();
    ioctl (seqfd, SNDCTL_SEQ_SYNC, 0);

    return 1;
}


static void shutdown ()
{
    close (seqfd);
}
