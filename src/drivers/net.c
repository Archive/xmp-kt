/* Extended Module Player
 * Copyright (C) 1996-1998 Claudio Matsuoka and Hipolito Carraro Jr
 * $Id$
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See doc/COPYING
 * for more information.
 */

#include "config.h"

#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#include "xmpi.h"
#include "driver.h"
#include "mixer.h"

extern char *mixer_buffer;

static int audio_fd;

static int init (struct drv_config *);
static void bufdump ();
static void myshutdown ();

static void dummy () { }


static char *help[] = {
    "server=host[:port]", "Set the server and port to connect",
    NULL
};


struct xmp_drv_info drv_net = {
    "net",		/* driver ID */
    "TCP socket",	/* driver description */
    help,		/* help */
    init,		/* init */
    myshutdown,		/* shutdown */
    smix_numvoices,	/* numvoices */
    smix_voicepos,	/* voicepos */
    smix_echoback,	/* echoback */
    smix_setpatch,	/* setpatch */
    smix_setvol,	/* setvol */
    smix_setnote,	/* setnote */
    smix_setpan,	/* setpan */
    smix_setbend,	/* setbend */
    dummy,		/* starttimer */
    dummy,		/* stoptimer */
    dummy,		/* resetvoices */
    bufdump,		/* bufdump */
    dummy,		/* bufwipe */
    dummy,		/* clearmem */
    dummy,		/* sync */
    smix_writepatch,	/* writepatch */
    smix_getmsg,	/* getmsg */
    NULL
};


static int init (struct drv_config *cfg)
{
    struct sockaddr_in server_addr;
    struct hostent *h;
    FILE *s;
    char *host = "localhost";
    int port = 1313;
    char *buf, *token, **parm;

    buf = malloc (80);

    parm_init ();
    chkparm1 ("server", host = token);
    parm_end ();

    token = strrchr (host, ':');
    if (token) {
	*token++ = 0;
	port = strtoul (token, NULL, 0);
    }

    h = gethostbyname (host);

    printf ("Trying %d.%d.%d.%d...\n",
	(uint8)(*h->h_addr_list)[0], (uint8)(*h->h_addr_list)[1],
	(uint8)(*h->h_addr_list)[2], (uint8)(*h->h_addr_list)[3]);

    server_addr.sin_port = htons (port);
    server_addr.sin_family = AF_INET;
    memcpy (&server_addr.sin_addr.s_addr, *(h->h_addr_list), 4);

    if ((audio_fd = socket (PF_INET, SOCK_STREAM, 0)) < 0) {
	fprintf (stderr, "drv_net: cannot create socket\n");
	return 0;
    }

    if (connect (audio_fd, (struct sockaddr *)&server_addr,
		sizeof (struct sockaddr_in)) < 0) {
	fprintf (stderr, "drv_net: unable to connect to server\n");
	return 0;
    }

    printf ("Connected to %s.\n", h->h_name);

#define get_cfg(x,y) { if (!strncmp (buf, x, 3)) { \
	cfg->y = strtoul (r, NULL, 0); } }

    s = fdopen (audio_fd, "r");
    do {
	char *r;

	fgets (buf, 80, s);
	buf[strlen (buf) - 1] = 0;
	r = strrchr (buf, ' ');

	if (!r)
	    break;

	get_cfg ("R: ", rate);
	get_cfg ("C: ", mode);
	get_cfg ("F: ", fmt);
    } while (strlen (buf));

    printf ("Server at %dHz %s, mode 0x%08x\n\n",
	cfg->rate, cfg->mode < 2 ? "mono" : "stereo", cfg->fmt);
    
    smix_init (cfg);

    sprintf (buf, "%s [server %d.%d.%d.%d, port %d]",
	drv_net.description,
	(uint8)(*h->h_addr_list)[0], (uint8)(*h->h_addr_list)[1],
	(uint8)(*h->h_addr_list)[2], (uint8)(*h->h_addr_list)[3],
	port);

    drv_net.description = buf;

    return 1;
}


static void bufdump ()
{
    int i;

    for (i = smix_mixer (); i -= write (audio_fd, mixer_buffer, i); );
}


static void myshutdown ()
{
    smix_close ();

    if (audio_fd)
	close (audio_fd);
}

