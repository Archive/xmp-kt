/* Extended Module Player
 * Copyright (C) 1996-1998 Claudio Matsuoka and Hipolito Carraro Jr
 * $Id$
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See doc/COPYING
 * for more information.
 */

/* This should work for OpenBSD */

#include "config.h"

#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <sys/audioio.h>

#include "xmpi.h"
#include "driver.h"
#include "mixer.h"

extern char *mixer_buffer;

static int audio_fd;

static int init (struct drv_config *);
static int setaudio (struct drv_config *);
static void bufdump (void);
static void shutdown (void);

static void dummy () { }


static char *help[] = {
    "gain=val", "Audio output gain (0 to 255)",
    "buffer=val", "Audio buffer size (default is 32768)",
    NULL
};

struct xmp_drv_info drv_bsd = {
    "bsd",		/* driver ID */
    "BSD audio device",	/* driver description */
    help,		/* help */
    init,		/* init */
    shutdown,		/* shutdown */
    smix_numvoices,	/* numvoices */
    smix_voicepos,	/* voicepos */
    smix_echoback,	/* echoback */
    smix_setpatch,	/* setpatch */
    smix_setvol,	/* setvol */
    smix_setnote,	/* setnote */
    smix_setpan,	/* setpan */
    smix_setbend,	/* setbend */
    dummy,		/* starttimer */
    dummy,		/* stoptimer */
    dummy,		/* reset */
    bufdump,		/* bufdump */
    dummy,		/* bufwipe */
    dummy,		/* clearmem */
    dummy,		/* sync */
    smix_writepatch,	/* writepatch */
    smix_getmsg,	/* getmsg */
    NULL
};


static int setaudio (struct drv_config *cfg)
{
    audio_info_t ainfo;
    int gain = 128;
    int bsize = 32 * 1024;
    char *token;
    char **parm = cfg->parm;

    parm_init ();
    chkparm1 ("gain", gain = atoi (token));
    chkparm1 ("buffer", bsize = atoi (token));
    parm_end ();

    if (gain < AUDIO_MIN_GAIN)
	gain = AUDIO_MIN_GAIN;
    if (gain > AUDIO_MAX_GAIN)
	gain = AUDIO_MAX_GAIN;

    AUDIO_INITINFO (&ainfo);

    ainfo.play.sample_rate = cfg->rate;
    ainfo.play.channels = cfg->mode;
    ainfo.play.precision = cfg->fmt & MIX_16BIT ? 16 : 8;
    ainfo.play.encoding = cfg->fmt & MIX_16BIT ?
	AUDIO_ENCODING_SLINEAR : AUDIO_ENCODING_ULINEAR;
    ainfo2.play.gain = ainfo.play.gain = gain;
    ainfo2.play.buffer_size = ainfo.play.buffer_size = bsize;

    if (ioctl (audio_fd, AUDIO_SETINFO, &ainfo) == -1) {
	close (audio_fd);
	return 0;
    }

    return 1;
}


static int init (struct drv_config *cfg)
{
    audio_fd = open ("/dev/sound", O_WRONLY);

    if (audio_fd == -1)
	return 0;

    if (cfg->nbuf < 2)
	cfg->nbuf = 2;

    cfg->fmt &= 0xffff;

    if (!setaudio (cfg))
	return 0;

    smix_init (cfg);

    return 1;
}


/* Build and write one tick (one PAL frame or 1/50 s in standard vblank
 * timed mods) of audio data to the output device.
 */
static void bufdump ()
{
    register int i;

    for (i = smix_mixer (); i -= write (audio_fd, mixer_buffer, i); );
}


static void shutdown ()
{
    smix_close ();
    close (audio_fd);
}
