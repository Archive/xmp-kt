/* Extended Module Player - xm_save.c
 * Copyright (C) 1996,1997 Claudio Matsuoka and Hipolito Carraro Jr
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See docs/COPYING
 * for more information.
 */

#include "config.h"

#include "save.h"
#include "xm.h"


int xm_save (FILE * f)
{
    int i, j, r;
    int instr_no = 0;
    uint8 *patbuf, *p;
    uint8 compression;
    uint16 entries;
    struct xxm_event *event, e;
    struct xm_file_header xfh;
    struct xm_pattern_header xph;
    struct xm_instrument_header xih;
    struct xm_instrument xi;
    struct xm_sample_header xsh[16];

    SAVE_INIT ();

#if 0
    if (strncmp ((char *) xfh.id, "Extended Module: ", 17))
    return -1;
    strncpy (module_name, (char *) xfh.name, 20);
    strcpy (module_type, "Fast Tracker II module");

#endif

    strncpy(xfh.id, "Extended Module: ", 17);
    /*strncpy(xfh.name, xxh->title, 20);*/
    strncpy(xfh.name, module_name, 20);
    xfh.doseof = 0x1a;
    strncpy(xfh.tracker, "Kegtracker [xmp]", 20);
    xfh.version = 0x00;
    xfh.headersz = sizeof(xfh);
    xfh.songlen = xxh->len;
    xfh.restart = xxh->rst;
    xfh.channels = xxh->chn;
    xfh.patterns = xxh->pat;
    xfh.instruments = xxh->ins;
    xfh.tempo = xxh->tpo;
    xfh.bpm = xxh->bpm;
    xfh.flags = xxh->flg & 1;
    memcpy (xfh.order, xxo, xxh->len);

    L_ENDIAN16 (xfh.songlen);
    L_ENDIAN16 (xfh.restart);
    L_ENDIAN16 (xfh.channels);
    L_ENDIAN16 (xfh.patterns);
    L_ENDIAN16 (xfh.instruments);
    L_ENDIAN16 (xfh.tempo);
    L_ENDIAN16 (xfh.bpm);
    L_ENDIAN16 (xfh.flags);

    fwrite (&xfh, sizeof (xfh), 1, f);

#if 0
    MODULE_INFO ();
    if (opt.verbose)
        report ("Tracker name   : %-20.20s\n", xfh.tracker);

    PATTERN_INIT ();
#endif

    if (opt.verbose)
        report ("Saving patterns: %d ", xxh->pat);
    for (i = 0; i < xxh->pat; i++) {
	/*
        PATTERN_ALLOC (i);
        TRACK_ALLOC (i);
        */

        xph.length = sizeof(xph);
        xph.packing = 0;
        r = xph.rows = xxp[i]->rows;

        /* first pass to calc xph.datasize */
        xph.datasize = 0;
        for (j = 0; j < (xxh->chn * r); j++) {
            event = &EVENT (i, j % xxh->chn, j / xxh->chn);
            entries = ((event->note==0)?0:1) +
                ((event->ins==0)?0:1) +
                ((event->vol==0)?0:1) +
                ((event->fxt==0)?0:1) +
                 ((event->fxp==0)?0:1);
    
            if(entries < 4)
                xph.datasize += (entries + 1);
               else
                xph.datasize += 5;
        }

        L_ENDIAN16 (xph.rows);
        L_ENDIAN16 (xph.datasize);

        fwrite (&xph, sizeof (xph), 1, f);

        p = patbuf = calloc (1, xph.datasize);
        for (j = 0; j < (xxh->chn * r); j++) {
            /* this condition should not occur */
            if ((p - patbuf) > xph.datasize)
                break;
    
            event = &EVENT (i, j % xxh->chn, j / xxh->chn);
            memcpy(&e, event, sizeof(e));

            if (e.vol) {
                /* Volume set */
                if ((e.vol >= 0x01) && (e.vol <= 0x41)) {
                    e.vol += 0x0f;
                } else {
                    /* Volume column effects */
    
                /** ONLY UNAMBIGUOUS ONES ARE DONE PROPERLY YET **/
                /** cf. xm_load.c **/
                switch(e.f2t) {
                case FX_VOLSLIDE:
                    e.vol = e.f2p + 0x60;
                    e.vol |= 0x06 << 4;
                    break;
                /*
                case FX_VOLSLIDE:
                    e.vol = (e.f2p >> 4) + 0x70;
                    e.vol |= 0x07 << 4;
                    break;
                */
                case FX_EXTENDED:
                    /*e.f2p = (EX_F_VSLIDE_DN << 4) | (e.vol - 0x80);*/
                    e.vol |= 0x08 << 4;
                    break;
                /*
                case FX_EXTENDED:
                    e.f2p = (EX_F_VSLIDE_UP << 4) | (e.vol - 0x90);
                    e.vol |= 0x09 << 4;
                    break;
                */
                case FX_VIBRATO:
                    e.f2p = (e.vol - 0xa0) << 4;
                    e.vol |= 0x0a << 4;
                    break;
                /*
                case FX_VIBRATO:
                    e.f2p = e.vol - 0xb0;
                    e.vol |= 0x0b << 4;
                    break;
                */
                case FX_SETPAN:
                    e.vol = ((e.f2p - 8) >> 4) + 0xc0;
                    e.vol |= 0x0c << 4;
                    break;
                case FX_PANSLIDE:
                    e.f2p = (e.vol - 0xd0) << 4;
                    e.vol |= 0x0d << 4;
                    break;
                /*
                case FX_PANSLIDE:
                    e.f2t = FX_PANSLIDE;
                    e.f2p = e.vol - 0xe0;
                    e.vol |= 0x0e << 4;
                    break;
                */
                case FX_TONEPORTA:
                    e.vol = (e.f2p >> 4) + 0xf0;
                    e.vol |= 0x0f << 4;
                    break;
                }
            }
        }
    
        entries = ((e.note==0)?0:1) + ((e.ins==0)?0:1) +
            ((e.vol==0)?0:1) + ((e.fxt==0)?0:1) +
            ((e.fxp==0)?0:1);

        if(entries < 4) {
            compression = XM_EVENT_PACKING;
            if(e.note != 0)
                compression |= XM_EVENT_NOTE_FOLLOWS;
            if(e.ins != 0)
                compression |= XM_EVENT_INSTRUMENT_FOLLOWS;
                /* how to handle XM_END_OF_SONG ? */
            if(e.vol != 0)
                compression |= XM_EVENT_VOLUME_FOLLOWS;
            if(e.fxt != 0)
                compression |= XM_EVENT_FXTYPE_FOLLOWS;
            if(e.fxp != 0)
                compression |= XM_EVENT_FXPARM_FOLLOWS;
    
            *p++ = compression;
    
            if(compression & XM_EVENT_NOTE_FOLLOWS)
                *p++ = e.note;
            if(compression & XM_EVENT_INSTRUMENT_FOLLOWS)
                *p++ = e.ins;
            if(compression & XM_EVENT_VOLUME_FOLLOWS)
                *p++ = e.vol;
            if(compression & XM_EVENT_FXTYPE_FOLLOWS)
                *p++ = e.fxt;
            if(compression & XM_EVENT_FXPARM_FOLLOWS)
                *p++ = e.fxp;
        } else {
            *p++ = e.note;
            *p++ = e.ins;
            *p++ = e.vol;
            *p++ = e.fxt;
            *p++ = e.fxp;
        }
    }
    fwrite (patbuf, 1, xph.datasize, f);
    free (patbuf);
    if (opt.verbose)
        report (".");
    }

#if 0
    /*
    PATTERN_ALLOC (i);
    */

    xxp[i]->rows = 64;
    xxt[i * xxh->chn] = calloc (1, sizeof (struct xxm_track) +
    sizeof (struct xxm_event) * 64);
    xxt[i * xxh->chn]->rows = 64;
    for (j = 0; j < xxh->chn; j++)
    xxp[i]->info[j].index = i * xxh->chn;
#endif

    if (opt.verbose)
        report ("\nInstruments    : %d ", xxh->ins);
    if (opt.verbose > 1)
        report ("\n");

#if 0
    /* ESTIMATED value! We don't know the actual value at this point */
    xxh->smp = 255;

    INSTRUMENT_INIT ();
#endif

    for (i = 0; i < xxh->ins; i++) {
        /* need to determine size ? */
        xih.size=263;
        strncpy (xih.name, xxih[i].name, 22);
        xih.type = 0;
        xih.samples = xxih[i].nsm;
        xih.sh_size = sizeof(xsh);

        L_ENDIAN32 (xih.size);
        L_ENDIAN16 (xih.samples);

        fwrite (&xih, sizeof (xih), 1, f);
    
        if ((opt.verbose > 1) && (strlen ((char *)xxih[i].name) || xxih[i].nsm))
            report ("[%2X] %-22.22s %2d ", i, xxih[i].name, xxih[i].nsm);
    
        /*xxi[i] = calloc (sizeof (struct xxm_instrument), xxih[i].nsm);*/
    
        if (xxih[i].nsm) {
	fprintf(stderr, "$");
            /* Envelope */
            xi.v_fade = xxih[i].rls;
            xi.v_pts = xxih[i].aei.npt;
            xi.v_sus = xxih[i].aei.sus;
            xi.v_start = xxih[i].aei.lps;
            xi.v_end = xxih[i].aei.lpe;
            xi.v_type = xxih[i].aei.flg;
            xi.p_pts = xxih[i].pei.npt;
            xi.p_sus = xxih[i].pei.sus;
            xi.p_start = xxih[i].pei.lps;
            xi.p_end = xxih[i].pei.lpe;
            xi.p_type = xxih[i].pei.flg;
            /*
            xxae[i] = calloc (4, xxih[i].aei.npt);
            xxpe[i] = calloc (4, xxih[i].pei.npt);
            */
            memcpy (xi.v_env, xxae[i], xxih[i].aei.npt * 4);
            memcpy (xi.p_env, xxpe[i], xxih[i].pei.npt * 4);
    
            memcpy (xi.sample, &xxim[i], 96);
    
            /* moved up here from following loop */
            xi.y_wave = xxi[i][0].vwf;
            xi.y_depth = xxi[i][0].vde;
            xi.y_rate = xxi[i][0].vra;
            xi.y_sweep = xxi[i][0].vsw;
    
            for (j = 0; j < 24; j++) {
                L_ENDIAN16 (xi.v_env[j]);
                L_ENDIAN16 (xi.p_env[j]);
            }
            L_ENDIAN16 (xi.v_fade);

            fwrite (&xi, sizeof (xi), 1, f);
    
            for (j = 0; j < xxih[i].nsm; j++) {
                strncpy (xsh[j].name, xxs[instr_no].name, 22);
                xsh[j].volume = xxi[i][j].vol;
                xsh[j].pan = xxi[i][j].pan;
                xsh[j].relnote = xxi[i][j].xpo;
                xsh[j].finetune = xxi[i][j].fin;
                /*xxi[i][j].sid = instr_no;*/
                xsh[j].length = xxs[instr_no].len;
                xsh[j].loop_start = xxs[instr_no].lps;
                xsh[j].loop_length = xxs[instr_no].lpe - xsh[j].loop_start;
        
                xsh[j].type = xxs[instr_no].flg & WAVE_16_BITS ?
                    XM_SAMPLE_16BIT : 0;
                xsh[j].type |= xxs[instr_no].flg & WAVE_LOOPING ?
                    XM_LOOP_FORWARD : 0;
                xsh[j].type |= xxs[instr_no].flg &
                (WAVE_LOOPING | WAVE_BIDIR_LOOP) ?
                    XM_LOOP_PINGPONG : 0;
                instr_no++;

                L_ENDIAN32 (xsh[j].length);
                L_ENDIAN32 (xsh[j].loop_start);
                L_ENDIAN32 (xsh[j].loop_length);

                fwrite (&xsh[j], sizeof (xsh[j]), 1, f);
            }
            for (j = 0; j < xxih[i].nsm; j++) {
                if ((opt.verbose > 1) && xsh[j].length)
                    report ("%s[%1x] %05x%c%05x %05x %c "
                    "V%02x F%+04d P%02x R%+03d",
                    j ? "\n\t\t\t\t" : "\t", j,
                    xxs[xxi[i][j].sid].len,
                    xxs[xxi[i][j].sid].flg & WAVE_16_BITS ? '+' : ' ',
                    xxs[xxi[i][j].sid].lps,
                    xxs[xxi[i][j].sid].lpe,
                    xxs[xxi[i][j].sid].flg & WAVE_BIDIR_LOOP ? 'B' :
                    xxs[xxi[i][j].sid].flg & WAVE_LOOPING ? 'L' : ' ',
                    xsh[j].volume, xsh[j].finetune,
                    xsh[j].pan, xsh[j].relnote);

                drv_savepatch (f, xxi[i][j].sid, opt.c4rate,
                    XMP_SMP_DIFF, &xxs[xxi[i][j].sid], NULL);
            }
            if (opt.verbose == 1)
                report (".");

#if 0
    /* WE SHOULDN'T HAVE TO WORRY ABOUT THIS IN THE SAVER ? */
    } else {
        /* The sample size is a field of struct xm_instrument_header that
         * should be in struct xm_instrument according to the (official)
         * format description. xmp puts the field in the header because
         * Fasttracker writes the modules this way. BUT there's some
         * other tracker or conversor out there following the specs
         * verbatim and thus creating modules incompatible with those
         * created by Fasttracker. The following piece of code is a
         * workaround for this problem (allowing xmp to play "Braintomb"
         * by Jazztiz/ART).
         */

        if (xih.size == 29)
        fseek (f, -4, SEEK_CUR);
#endif
        }

        if ((opt.verbose > 1) && (strlen ((char *)xxih[i].name) || xih.samples))
            report ("\n");
    }
#if 0
    xxh->smp = instr_no;
    if (opt.verbose > 1)
        report ("Stored samples : %d", xxh->smp);
    if (opt.verbose)
        report ("\n");
#endif

#if 0
    /* If dynamic pan is disabled, XM modules will use the standard
     * MOD channel panning (LRRL).
     */

    for (i = 0; i < xxh->chn; i++)
        xxc[i].pan = opt.nopan ? (((i + 1) / 2) % 2) * 0xff : 0x80;
#endif

    return 0;
}
