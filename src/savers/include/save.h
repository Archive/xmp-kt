/* Extended Module Player - load.h
 * Copyright (C) 1997 Claudio Matsuoka and Hipolito Carraro Jr
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See docs/COPYING
 * for more information.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "xmpi.h"
#include "xxm.h"
#include "effects.h"
#include "driver.h"

extern char module_name[MODULE_NAME_MAXSIZE];
extern struct xmp_chctl xctl[32];

/*static char module_type[80];*/

#define SAVE_INIT() { \
    fseek (f, 0, SEEK_SET); \
}

#if 0
#define MODULE_INFO() { \
    if (opt.verbose) { \
	if (*module_name) report ("Module title   : %s\n", module_name); \
        report ("Module type    : %s\n", module_type); \
        if (xxh->len) report ("Module length  : %d patterns\n", xxh->len); \
    } \
}

#define INSTRUMENT_INIT() { \
    xxih=calloc (sizeof (struct xxm_instrument_header), xxh->ins); \
    xxim=calloc (sizeof (struct xxm_instrument_map), xxh->ins); \
    xxi=calloc (sizeof (struct xxm_instrument*), xxh->ins); \
    xxs=calloc (sizeof (struct xxm_sample), xxh->smp); \
    xxae=calloc (sizeof (uint16*), xxh->ins); \
    xxpe=calloc (sizeof (uint16*), xxh->ins); \
    /* xxfe=calloc (sizeof (uint16*), xxh->ins); */ \
}

/*
#define PATTERN_INIT() { \
    xxt=calloc (sizeof (struct xxm_track*), xxh->trk); \
    xxp=calloc (sizeof (struct xxm_pattern*), xxh->pat+1); \
}
*/
#define PATTERN_INIT() { \
    xxt=xmp_get_shared_mem(sizeof(struct xxm_track*) * xxh->trk); \
    xxp=xmp_get_shared_mem(sizeof(struct xxm_pattern*) * xxh->pat+1); \
}

/*
#define PATTERN_ALLOC(x) { \
    xxp[x]=calloc(1, sizeof (struct xxm_pattern)+\
    sizeof(struct xxm_trackinfo)*(xxh->chn-1)); \
}
*/
#define PATTERN_ALLOC(x) { \
    xxp[x]=xmp_get_shared_mem(sizeof(struct xxm_pattern)+\
                     sizeof(struct xxm_trackinfo)*(xxh->chn-1)); \
}

#define DETERMINE_PATTERN_SIZE(i) ( \
       sizeof(struct xxm_pattern) + \
       sizeof(struct xxm_trackinfo) * (xxh->chn-1) \
)

#define TRACK_ALLOC(i) { \
    int j; \
    for (j=0;j<xxh->chn;j++) { \
	xxp[i]->info[j].xpose=0; \
	xxp[i]->info[j].index=i*xxh->chn+j; \
	xxt[i*xxh->chn+j]=calloc (sizeof (struct xxm_track)+ \
	    sizeof (struct xxm_event)*(xxp[i]->rows?xxp[i]->rows:0x100), 1); \
	xxt[i*xxh->chn+j]->rows=xxp[i]->rows; \
    } \
}

#define DETERMINE_TRACK_SIZE(rows) ( \
       ( \
          sizeof(struct xxm_track) + \
          sizeof(struct xxm_event) * (rows?rows:0x100) \
       ) * xxh->chn \
)

#define ALLOC_MEM_POOL(size) { \
    mem_pool_ptr = xmp_get_shared_mem(size); \
}

#define PATTERN_ALLOC_FROM_POOL(x) { \
    xxp[x] = mem_pool_ptr; \
    mem_pool_ptr += sizeof(struct xxm_pattern) \
                  + sizeof(struct xxm_trackinfo) * (xxh->chn-1); \
}

#define TRACK_ALLOC_FROM_POOL(i) { \
    int j; \
    int s = sizeof(struct xxm_track)+ \
             sizeof(struct xxm_event)*(xxp[i]->rows?xxp[i]->rows:0x100); \
    xxt[i*xxh->chn] = mem_pool_ptr; \
    mem_pool_ptr += s * xxh->chn; \
    for (j=0;j<xxh->chn;j++) { \
      xxp[i]->info[j].xpose = 0; \
      xxp[i]->info[j].index = i*xxh->chn+j; \
      xxt[i*xxh->chn+j] = ((void *)(xxt[i*xxh->chn])) + s*j; \
      xxt[i*xxh->chn+j]->rows=xxp[i]->rows; \
    } \
}
#endif
