/* Extended Module Player - mod_save.c
 * Copyright (C) 1996,1997 Claudio Matsuoka and Hipolito Carraro Jr
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See docs/COPYING
 * for more information.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "save.h"
#include "mod.h"
#include "period.h"

static struct {
    char *magic;
    char *tracker;
    int ch;
} mod_magic[] = {
    { "M.K.", "Protracker", 4 },
    { "M!K!", "Protracker", 4 },
    { "M&K!", "Noisetracker", 4 },
    { "EMW3", "UNIC-Tracker", 4 },
    { "FLT4", "Startrekker", 4 },
    { "FLT8", "Startrekker", 8 },
    { "6CHN", "Fasttracker", 6 },
    { "8CHN", "Fasttracker", 8 },
    { "", 0 }
};

static int module_save (FILE *, int);


int m15_save (FILE * f)
{
    return module_save (f, 15);
}


int mod_save (FILE * f)
{
    return module_save (f, 31);
}


static int module_save (FILE * f, int nins)
{
    int i, j;
    int smp_size, pat_size, wow;
    /*int total_mem_size;*/
    struct xxm_event *event;
    struct stat st;
    union mod_header mh;
    uint8 mod_event[4];
    uint16 period;
    char *id = "";
    void *t;

    SAVE_INIT ();

#if 0
    fstat (fileno (f), &st);
    xxh->tpo = 6;
    xxh->bpm = 125;
    xxh->ins = nins;
    xxh->smp = xxh->ins;
    xxh->chn = 0;
    smp_size = 0;
    pat_size = 0;
#endif

    if (xxh->ins > 15) {
	for (i = 0; mod_magic[i].ch; i++) {
	    if (xxh->chn == mod_magic[i].ch) {
	        strncpy ((char *) mh.m31.magic, mod_magic[i].magic, 4);
		id = mod_magic[i].tracker;
		break;
	    }
	}

#if 0
	if (!xxh->chn) {
	    if (!strncmp ((char *) mh.m31.magic + 2, "CH", 2) &&
		isdigit (*mh.m31.magic) && isdigit (mh.m31.magic[1])) {
		if ((xxh->chn = (*mh.m31.magic - '0') *
			10 + mh.m31.magic[1] - '0') > 32)
		    return -1;
	    } else if (!strncmp ((char *) mh.m31.magic + 1, "CHN", 3) &&
		isdigit (*mh.m31.magic)) {
		if (!(xxh->chn = (*mh.m31.magic - '0')))
		    return -1;
	    } else
		return -1;
	    id = "Multichannel";
	}
#endif
    } else {
	id = "Soundtracker 15-instrument";
	xxh->chn = 4;
	if (mh.m15.ins[0].finetune > 0x0f)
	    return -1;
    }

    strncpy ((char *) mh.m31.name, module_name, 20);

    if(xxh->ins > 15) {
	mh.m31.len = xxh->len;
	mh.m31.restart = xxh->rst;
	memcpy (mh.m31.order, xxo, 128);
        for (i = 0; i < 128; i++) {
	    if(mh.m31.order[i] > 63) report("ORDER>63\n");
	    report("%02d: %02d\t", i, mh.m31.order[i]);
	}
    } else {
	mh.m15.len = xxh->len;
	mh.m15.restart = xxh->len;
	memcpy (mh.m15.order, xxo, 128);
        for (i = 0; i < 128; i++)
	    report("%02d: %02d\t", i, mh.m15.order[i]);
    }

#if 0
    /* Thunder's method to determine the number of patterns (shown below)
     * doesn't work for badly ripped mods. 
     *
     * xxh->pat=(st.st_size-smp_size-header_size)/(64*xxh->chn*4);
     */

    for (i = 0; i < 128; i++)
	if (xxo[i] > xxh->pat)
	    xxh->pat = xxo[i];
    xxh->pat++;

    if (xxh->pat > 0x7f || xxh->len == 0 || xxh->len > 0x7f)
	return -1;
#endif

    pat_size = 256 * xxh->chn * xxh->pat;

    /*INSTRUMENT_INIT ();*/

    for (i = 0; i < xxh->ins; i++) {
	mh.m31.ins[i].size = xxs[i].len / 2;
	mh.m31.ins[i].loop_start = xxs[i].lps / 2;
	mh.m31.ins[i].loop_size = (xxs[i].lpe - xxs[i].lps) / 2;
	mh.m31.ins[i].finetune = xxi[i][0].fin >> 4;
	mh.m31.ins[i].volume = xxi[i][0].vol;
	strncpy (mh.m31.ins[i].name, xxih[i].name, 22);
    }
    for (i = 0; i < xxh->ins; i++) {
	B_ENDIAN16 (mh.m31.ins[i].size);
	B_ENDIAN16 (mh.m31.ins[i].loop_start);
	B_ENDIAN16 (mh.m31.ins[i].loop_size);
#if 0
	if (mh.m31.ins[i].length > 0x8000 || mh.m31.ins[i].loop_start > 0x8000
		|| mh.m31.ins[i].loop_length > 0x8000)
	    return -1;
#endif
    }

#if 0
    /* Test for Mod's Grave WOW modules
     *
     * Stefan Danes <sdanes@marvels.hacktic.nl> said:
     * This weird format is identical to '8CHN' but still uses the 'M.K.' ID.
     * You can only test for WOW by calculating the size of the module for 8 
     * channels and comparing this to the actual module length. If it's equal, 
     * the module is an 8 channel WOW.
     */

    if ((wow = (!strncmp ((char *) mh.m31.magic, "M.K.", 4) &&
		(0x43c + xxh->pat * 8 * 4 * 0x40 + smp_size == st.st_size)))) {
	xxh->chn = 8;
	id = "Mod's Grave";
    }
#endif

    if(xxh->ins > 15) {
	fwrite (&mh, 1, sizeof (struct mod_header), f);
    } else {
	fwrite (&mh, 1, sizeof (struct m15_header), f);
    }

#if 0
    if (xxh->ins > 15)
	sprintf (module_type, "%s %4.4s module", id, mh.m31.magic);
    else
	sprintf (module_type, "%s module", id);

    MODULE_INFO ();

    for (i = 0; (opt.verbose > 1) && (i < xxh->ins); i++) {
	if ((strlen ((char *) xxih[i].name) || (xxs[i].len > 2)))
	    report ("[%2X] %-22.22s %04x %04x %04x %c V%02x %+d\n",
		i, xxih[i].name, xxs[i].len, xxs[i].lps,
		xxs[i].lpe, mh.m31.ins[i].looplength > 1 ? 'L' : ' ',
		xxi[i][0].vol, (char) xxi[i][0].fin >> 4);
    }

    PATTERN_INIT ();
#endif

    /* Load and convert patterns */
    if (opt.verbose)
	report ("Stored patterns: %d ", xxh->pat);

/*
    total_mem_size = (DETERMINE_PATTERN_SIZE(0)
                      + DETERMINE_TRACK_SIZE(64)) * xxh->pat;
    ALLOC_MEM_POOL(total_mem_size);
*/
    for (i = 0; i < xxh->pat; i++) {
#if 0
	/*PATTERN_ALLOC_FROM_POOL(i);*/
	PATTERN_ALLOC(i);
	xxp[i]->rows = 64;
	/*TRACK_ALLOC_FROM_POOL(i);*/
	TRACK_ALLOC(i);
#endif
	if(xxp[i]->rows != 64) return -1;

	for (j = 0; j < (64 * xxh->chn); j++) {
	    event = &EVENT (i, j % xxh->chn, j / xxh->chn);

	    period = amiga_note_to_period(event->note);

	    mod_event[0] = ((MSN(event->ins) << 4) + (period >> 8));
	    mod_event[1] = period & 0xff;
	    mod_event[2] = ((LSN(event->ins) << 4) + (event->fxt >> 8));
	    mod_event[3] = event->fxp & 0xff;

#if 0
	    if (!event->fxp) {
		switch (event->fxt) {
		case 0x05:
		    event->fxt = 0x03;
		    break;
		case 0x06:
		    event->fxt = 0x04;
		    break;
		case 0x01:
		case 0x02:
		case 0x0a:
		    event->fxt = 0x00;
		}
	    }

	    /* Special translation for e8 (set panning) effect.
	     * This is not an official Protracker effect but DMP uses
	     * it for panning, and a couple of modules follow this
	     * "standard".
	     */

	    if ((event->fxt == 0x0e) && ((event->fxp & 0xf0) == 0x80)) {
		event->fxt = FX_SETPAN;
		event->fxp <<= 4;
	    }
#endif
	    fwrite (mod_event, 1, 4, f);
	}
	if (opt.verbose)
	    report (".");
    }

    /* Save samples */

    if (opt.verbose)
	report ("\nSaving samples : %d ", xxh->smp);
    for (i = 0; i < xxh->smp; i++) {
	if (!xxs[i].len)
	    continue;
	drv_savepatch (f, xxi[i][0].sid, opt.c4rate, 0,
	    &xxs[xxi[i][0].sid], NULL);
	if (opt.verbose)
	    report (".");
    }
    if (opt.verbose)
	report ("\n");

    return 0;
}
