/* Extended Module Player - load.c
 * Copyright (C) 1996, 1997 Claudio Matsuoka and Hipolito Carraro Jr
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See docs/COPYING
 * for more information.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "xmpi.h"
#include "xxm.h"


extern struct xmp_drv_info *drv;
extern struct drv_config cfg;
extern char module_name[MODULE_NAME_MAXSIZE];
extern struct xmp_options opt;
extern struct xmp_fmt_info *__fmt_head;
extern struct xmp_ord_info xmpi_oinfo[256];
extern int __pause;

#if 0
static char *temp;


static int decompress (char **mod, char *suffix, char *decomp)
{
    char *line;

    if ((!strcasecmp (*mod + strlen (*mod) - strlen (suffix), suffix))) {
	line = malloc (strlen (decomp) + strlen (*mod) + strlen (temp) + 16);
	sprintf (line, "%s \"%s\" > %s", decomp, *mod, temp);
	if (opt.verbose > 2)
	    fprintf (stderr, "%s\n", line);
	if (system (line)) {
	    free (line);
	    return -1;
        } else
	    *mod = temp;

	free (line);
	return 0;
    }
    return 1;
}
#endif


int xmp_save_module (char *s)
{
    FILE *f;
    int i, t;
    struct xmp_fmt_info *fmt;

#if 0
    temp = tempnam (NULL, "xmp_");

    if ((decompress (&s, ".gz", "gzip -dc") *
		decompress (&s, ".Z", "uncompress -c") *
		decompress (&s, ".zip", "unzip -p") *
		decompress (&s, ".lha", "lha -pq") *
		decompress (&s, ".lzh", "lha -pq") *
		decompress (&s, ".bz2", "bzip2 -dc") *
		decompress (&s, ".mdz", "unzip -p 2>/dev/null")) < 0)
	return -1;
#endif

    if ((f = fopen (s, "w")) == NULL)
	return -3;

    /*drv->clearmem ();*/

    /*opt.filename = s;*/		/* For ALM */

    /* don't trash xxh */
    /*xxh = calloc (sizeof (struct xxm_header), 1);*/

    for (i = 0, fmt = __fmt_head; fmt; fmt = fmt->next)
	if (fmt->saver && ((i = fmt->saver (f)) != -1))
	    break;

    fclose (f);
    /*unlink (temp);*/

    if (i < 0)
	return i;

#if 0
    str_adj (module_name);
    if (!*module_name)
	strcpy (module_name, "(untitled)");

    strncpy (xxh->title, module_name, 32);

    if (opt.linear == 0)
	xxh->flg &= ~XXM_FLG_LINEAR;
    if (opt.linear == 1)
	xxh->flg |= XXM_FLG_LINEAR;

    if (opt.verbose > 1) {
	if (xxh->flg & XXM_FLG_MODRNG)
	    report ("Using Amiga period limits\n");
	report ("Module looping : %s\n", opt.loop ? "yes" : "no");
	report ("Period mode    : %s\n",
		xxh->flg & XXM_FLG_LINEAR ? "linear" : "Amiga");
	report ("Restart pos    : %d\n", xxh->rst);
    }

    if (opt.verbose > 2) {
	report ("Base volume    : %d\n", opt.vol_base);
	report ("C4 replay rate : %d\n", opt.c4rate);
	report ("Channel mixing : %d%% (dynamic pan %s)\n",
		opt.mix * opt.reverse,
		opt.nopan ? "disabled" : "enabled");
    }

    if (opt.verbose) {
	report ("Channels       : %d [ ", xxh->chn);
	for (i = 0; i < xxh->chn; i++) {
	    if (xxc[i].flg & XXM_CHANNEL_FM)
		report ("F ");
	    else
	        report ("%x ", xxc[i].pan >> 4);
	}
	report ("]\n");
    }

    t = xmpi_scan_module (opt.rrate);

    if (opt.verbose) {
        report ("Estimated time : %dmin%02ds\n",
            (t - xmpi_oinfo[opt.start].time) / 60000,
            ((t - xmpi_oinfo[opt.start].time) / 1000) % 60);
    }

    __pause = 0;
#endif

    return t;
}

