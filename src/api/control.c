/* Extended Module Player
 * Copyright (C) 1996-1998 Claudio Matsuoka and Hipolito Carraro Jr
 * $Id$
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See doc/COPYING
 * for more information.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/time.h>
#include <unistd.h>

#include "xmpi.h"
#include "xxm.h"
#include "driver.h"
#include "formats.h"

extern int __current_ord;
extern int __global_vol;
extern int __pause;
extern struct xmp_chctl __xctl[32];
extern struct xmp_drv_info *drv;
extern struct drv_config cfg;

static int drv_parm = 0;


void xmp_init (int argc, char **argv, struct xmp_options *xo)
{
    int i, norc = 0;

    drv_register ();
    fmt_register ();

    memset (xo, 0, sizeof (*xo));

    /* Set defaults */
#ifdef WORDS_BIGENDIAN
    xo->bsmp = 1;
#endif
    xo->freq = 22050;
    xo->interpolate = 1;
    xo->reverse = 1;
    xo->linear = -1;
    xo->start = 0;
    xo->mix = 75;

    xo->rrate = PAL_RATE;
    xo->c4rate = C4_PAL_RATE;
    xo->vol_base = 0x40;
    xo->vol_xlat = NULL;
    xo->vef = 0;

    /* Okay, this is kludgy. But I must parse this _before_ loading
     * the rc file.
     */
    for (i = 1; i < argc; i++) {
	if (!strcmp (argv[i], "--norc")) {
	    norc = 1;
	    break;
	}
    }

    if (!norc)
	xmpi_read_rc (xo);

    __event_callback = NULL;

    /*xmpi_tell_wait ();*/
}


inline int xmp_open_audio (struct xmp_options *o)
{
    memcpy (&opt, o, sizeof (struct xmp_options));
    return drv_open (opt.drv_id);
}


inline void xmp_close_audio ()
{
    drv->shutdown ();
}


void xmp_set_driver_parameter (char *s)
{
    cfg.parm[drv_parm] = s;
    while (isspace (*cfg.parm[drv_parm]))
	cfg.parm[drv_parm]++;
    drv_parm++;
}


inline void xmp_register_event_callback (void (*cb)())
{
    __event_callback = cb;
}

inline void xmp_unregister_event_callback (void)
{
    __event_callback = NULL;
}

void xmp_channel_mute (int from, int num, int on)
{
    int i;

    if (on < 0)
	for (i = 0; i < num; i++)
	    __xctl[from + i].mute = !__xctl[from + i].mute;
    else
	for (i = 0; i < num; i++)
	    __xctl[from + i].mute = on;
}


int xmp_player_ctl (int cmd, int arg)
{
    switch (cmd) {
    case XMP_ORD_PREV:
	if (__current_ord > 0)
	    __current_ord--;
	return __current_ord;
    case XMP_ORD_NEXT:
	if (__current_ord < xxh->len)
	    __current_ord++;
	return __current_ord;
    case XMP_ORD_SET:
	if ((arg < xxh->len) && (arg >= 0))
	    __current_ord = arg;
	return __current_ord;
    case XMP_MOD_STOP:
	__current_ord = -2;
	return 0;
    case XMP_MOD_PAUSE:
	__pause ^= 1;
	return __pause;
    case XMP_MOD_RESTART:
	__current_ord = -1;
	return 0;
    case XMP_GVOL_DEC:
	if (__global_vol > 0)
	    __global_vol--;
	return __global_vol;
    case XMP_GVOL_INC:
	if (__global_vol < 64)
	    __global_vol++;
	return __global_vol;
    case XMP_TIMER_STOP:
	xmpi_timer_stop ();
	break;
    case XMP_TIMER_RESTART:
	xmpi_timer_restart ();
	break;
    }

    return 0;
}


int xmp_play_module ()
{
    time_t t0, t1;
    int t, i;

    time (&t0);
    xmpi_play_module ();
    time (&t1);
    t = difftime (t1, t0);

    opt.start = 0;

    /*for (i = 0; i < xxh->trk; i++)
	free (xxt[i]);
    for (i = 0; i < xxh->pat; i++)
	free (xxp[i]);*/
    for (i = 0; i < xxh->ins; i++) {
	/* free (xxfe[i]); */
	free (xxpe[i]);
	free (xxae[i]);
	free (xxi[i]);
    }
    /*free (xxt);
    free (xxp);*/
    free (xxi);
    free (xxs);
    free (xxim);
    free (xxih);
    /* free (xxfe); */
    free (xxpe);
    free (xxae);
    free (xxh);

    return t;
}


void xmp_play_pattern(int pat)
{
  xmpi_play_pattern(pat);
}


void xmp_get_driver_cfg (int *srate, int *res, int *chn, int *itpt)
{
    *srate = cfg.rate;
    *res = 8 + 8 * ((cfg.fmt & MIX_16BIT) != 0);
    *chn = cfg.mode;
    *itpt = cfg.itpt;
}


int xmp_verbosity_level (int i)
{
    int tmp;

    tmp = opt.verbose;
    opt.verbose = i;

    return tmp;
}
