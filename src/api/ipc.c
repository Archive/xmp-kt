/* Extended Module Player
 * Copyright (C) 1996-1998 Claudio Matsuoka and Hipolito Carraro Jr
 * $Id$
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See doc/COPYING
 * for more information.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include "xmpi.h"

typedef struct _intlist {
  int data;
  struct _intlist *next;
} intlist;

intlist *idlisthead = 0, *idlisttail = 0;

void *next_block = 0;
int amount_left = 0;
int size_to_alloc = 1 << 20;

int alloc_shared_mem() {
  int shmid;
  char *p;
  intlist *item;
  int size = size_to_alloc;
  size_to_alloc <<= 1;

  if ((shmid = shmget(IPC_PRIVATE, size, IPC_CREAT | 0600)) == -1)
    return 1;

  /*printf("allocated %08x bytes with shmid %d\n", size, shmid);*/
  p = shmat(shmid, 0, 0);
  memset(p, 0, size);
  item = ((intlist *)p)++;
  item->data = shmid;
  item->next = 0;
  if (idlisttail == 0) {
    idlisthead = idlisttail = item;
  } else {
    idlisttail->next = item;
    idlisttail = item;
  }
  next_block = p;
    amount_left = size - sizeof(intlist);
    return 0;
}

int init_shared_mem() {
  return alloc_shared_mem();
}

void *saved_next_block = 0;
int saved_amount_left = 0;

void xmp_shared_mem_push_mark() {
  saved_next_block = next_block;
  saved_amount_left = amount_left;
}

void xmp_shared_mem_pop() {
  intlist *t = idlisthead->next;
  while(t != 0) {
    shmctl(t->data, IPC_RMID, NULL);
    /*printf("freed shmid %d\n", t->data);*/
    t = t->next;
  }
  idlisthead->next = 0;
  next_block = saved_next_block;
  amount_left = saved_amount_left;
  size_to_alloc = 1 << 21;
}

void *xmp_get_shared_mem(int n)
{
  char *p;

/*
   if (n > amount_left)
     alloc_shared_mem();
*/
  while ((amount_left -= n) < 0)
    if (alloc_shared_mem())
      return 0;
  p = next_block;
  next_block += n;
/*
  amount_left -= n;
*/
  memset(p, 0, n);
  return p;
}

void xmp_detach_shared_mem(void *p) {
/*
  shmctl(*--(int*)p, IPC_RMID, NULL);
  shmdt(p);
*/
}
        
void free_all_shm() {
  intlist *t = idlisthead;
  while(t != 0) {
    shmctl(t->data, IPC_RMID, NULL);
    /*printf("freed shmid %d\n", t->data);*/
    t = t->next;
  }
}


/* pipes for parent-child synchronization */
/*
static int pfd1[2], pfd2[2];

int xmpi_tell_wait ()
{
    return (pipe (pfd1) || pipe (pfd2));
}


int xmpi_select_read (int fd, int msec)
{
    fd_set rfds;
    struct timeval tv;

    tv.tv_sec = msec / 1000;
    tv.tv_usec = (msec % 1000) * 1000;
    FD_ZERO (&rfds);
    FD_SET (fd, &rfds);

    return select (fd + 1, &rfds, NULL, NULL, &tv);
}


int xmp_tell_parent ()
{
    return write (pfd2[1], "c", 1) != 1;
}


int xmp_wait_parent ()
{
    char c;

    return !((read (pfd1[0], &c, 1) == 1) && (c == 'p'));
}



int xmp_check_parent (int msec)
{
    return xmpi_select_read (pfd1[0], msec);
}


int xmp_tell_child ()
{
    return write (pfd1[1], "p", 1) != 1;
}


int xmp_wait_child ()
{
    char c;

    return !((read (pfd2[0], &c, 1) == 1) && (c == 'c'));
}


int xmp_check_child (int msec)
{
    return xmpi_select_read (pfd2[0], msec);
}

*/
