/* Extended Module Player
 * Copyright (C) 1996-1998 Claudio Matsuoka and Hipolito Carraro Jr
 * $Id$
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See doc/COPYING
 * for more information.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <string.h>
#include "xmpi.h"
#include "xxm.h"

extern struct xmp_drv_info *drv;
extern char module_name[MODULE_NAME_MAXSIZE];
extern struct drv_config cfg;
extern struct xmp_fmt_info *__fmt_head;
extern struct xmp_drv_info *__drv_head;


inline struct xmp_module_info *xmp_get_module_info (struct xmp_module_info *i)
{
    strncpy (i->title, module_name, 40);
    i->chn = xxh->chn;
    i->pat = xxh->pat;
    i->ins = xxh->ins;
    i->trk = xxh->trk;
    i->smp = xxh->smp;
    i->len = xxh->len;

    return i;
}


inline char *xmp_get_driver_description ()
{
    return drv->description;
}


inline struct xmp_fmt_info *xmp_get_fmt_info (struct xmp_fmt_info **x)
{
    return *x = __fmt_head;
}


inline struct xmp_drv_info *xmp_get_drv_info (struct xmp_drv_info **x)
{
    return *x = __drv_head;
}


inline void xmp_display_license ()
{
    xmpi_display_license ();
}

