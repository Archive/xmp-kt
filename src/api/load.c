/* Extended Module Player
 * Copyright (C) 1996-1998 Claudio Matsuoka and Hipolito Carraro Jr
 * $Id$.
 *
 * This file is part of the Extended Module Player and is distributed
 * under the terms of the GNU General Public License. See doc/COPYING
 * for more information.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include "xmpi.h"
#include "xxm.h"


extern struct xmp_drv_info *drv;
extern struct drv_config cfg;
extern char module_name[MODULE_NAME_MAXSIZE];
extern struct xmp_options opt;
extern struct xmp_fmt_info *__fmt_head;
extern struct xmp_ord_info xmpi_oinfo[256];
extern char ivalid[256];
extern int __pause;

int xmpi_decrunch_sqsh (FILE *, char *);
int xmpi_decrunch_pp (FILE *, char *);


#define BUILTIN_PP	0x01
#define BUILTIN_SQSH	0x02


static int decrunch (FILE **f, char *s, char *temp)
{
    unsigned char b[64];
    char *msg, *cmd, *line;
    FILE *t;
    int builtin, res;

    fread (b, 1, 64, *f);

    msg = cmd = NULL;
    builtin = res = 0;

    if (b[0] == 'P' && b[1] == 'K') {
	msg = "Unpacking ZIP file... ";
	cmd = "unzip -pqqC \"%s\" -x readme '*.diz' '*.nfo' '*.txt' "
		"'*.exe' '*.com' 2>/dev/null>%s";
    } else if (b[2] == '-' && b[3] == 'l' && b[4] == 'h') {
	msg = "Unpacking LHa file... ";
	cmd = "lha -pq \"%s\" > %s";
    } else if (b[0] == 31 && b[1] == 139) {
	msg = "Uncompressing gzipped file... ";
	cmd = "gzip -dc \"%s\" > %s";
    } else if (b[0] == 'B' && b[1] == 'Z' && b[2] == 'h') {
	msg = "Uncompressing bzipped file... ";
	cmd = "bzip2 -dc \"%s\" > %s";
    } else if (b[0] == 31 && b[1] == 157) {
	msg = "Uncompressing file... ";
	cmd = "uncompress -c \"%s\" > %s";
    } else if (b[0] == 'P' && b[1] == 'P' && b[2] == '2' && b[3] == '0') {
	msg = "Decrunching Powerpacked file... ";
	builtin = BUILTIN_PP;
    } else if (b[0] == 'X' && b[1] == 'P' && b[2] == 'K' && b[3] == 'F' &&
	b[8] == 'S' && b[9] == 'Q' && b[10] == 'S' && b[11] == 'H') {
	msg = "Decrunching XPK-SQSH file... ";
	builtin = BUILTIN_SQSH;
    }

    fseek (*f, 0, SEEK_SET);

    if (!msg)
	return 0;

    if (opt.verbose)
	report (msg);

    if (cmd) {
	line = malloc (strlen (cmd) + strlen (s) + strlen (temp) + 16);
	sprintf (line, cmd, s, temp);

	if (system (line)) {
	    if (opt.verbose)
		report ("failed\n");
	    free (line);
	    return 1;
	}
	free (line);
    } else switch (builtin) {
    case BUILTIN_PP:    
	res = xmpi_decrunch_pp (*f, temp);
	break;
    case BUILTIN_SQSH:    
	res = xmpi_decrunch_sqsh (*f, temp);
	break;
    }

    if (res < 0 || (t = fopen (temp, "r")) == NULL) {
	if (opt.verbose)
	    report ("failed\n");
	return 1;
    }

    if (opt.verbose)
	report ("done\n");

    fclose (*f);
    *f = t;
 
    return 1;
}


int xmp_load_module (char *s)
{
    FILE *f;
    int i, t;
    char *temp;
    struct xmp_fmt_info *fmt;
    struct stat st;

    if ((f = fopen (s, "r")) == NULL)
	return -3;

    if (fstat (fileno (f), &st) < 0)
	return -3;

    if (S_ISDIR (st.st_rdev))
	return -1;

    temp = tempnam (NULL, "xmp_");
    t = decrunch (&f, s, temp);

    drv->clearmem ();

    opt.filename = s;		/* For ALM */
    xxh = xmp_calloc (sizeof (struct xxm_header), 1);

    for (i = 0; i < 256; ivalid[i++] = 0);

    for (i = 0, fmt = __fmt_head; fmt; fmt = fmt->next) {
	if (fmt->loader && ((i = fmt->loader (f)) != -1))
	    break;
    }

    fclose (f);

    if (t)
	unlink (temp);

    if (i < 0)
	return i;

    str_adj (module_name);
    if (!*module_name)
	strcpy (module_name, "(untitled)");

    /* strncpy (xxh->title, module_name, 32); */

    if (opt.linear == 0)
	xxh->flg &= ~XXM_FLG_LINEAR;
    if (opt.linear == 1)
	xxh->flg |= XXM_FLG_LINEAR;

    if (opt.verbose > 1) {
	if (xxh->flg & XXM_FLG_MODRNG)
	    report ("Using Amiga period limits\n");
	report ("Module looping : %s\n", opt.loop ? "yes" : "no");
	report ("Period mode    : %s\n",
		xxh->flg & XXM_FLG_LINEAR ? "linear" : "Amiga");
    }

    if (opt.verbose > 2) {
	report ("Restart pos    : %d\n", xxh->rst);
	report ("Base volume    : %d\n", opt.vol_base);
	report ("C4 replay rate : %d\n", opt.c4rate);
	report ("Channel mixing : %d%% (dynamic pan %s)\n",
		opt.mix * opt.reverse,
		opt.nopan ? "disabled" : "enabled");
    }

    if (opt.verbose) {
	report ("Channels       : %d [ ", xxh->chn);
	for (i = 0; i < xxh->chn; i++) {
	    if (xxc[i].flg & XXM_CHANNEL_FM)
		report ("F ");
	    else
	        report ("%x ", xxc[i].pan >> 4);
	}
	report ("]\n");
    }

    t = xmpi_scan_module ();

    if (opt.verbose) {
	report ("Estimated time : %dmin%02ds\n",
	    (t + 500) / 60000, ((t + 500) / 1000) % 60);
    }

    __pause = 0;

    return t;
}

