Summary: Extended Module Player
Name: xmp
Version: 0.4.4
Release: 1
Packager: Claudio Matsuoka <claudio@pos.inf.ufpr.br>
URL: http://xmp.home.ml.org
Group: Applications/Sound
Source: http://xmp.home.ml.org/pkg/0.4.4/xmp-0.4.4.tar.gz
Copyright: GPL

%description
xmp is an open source module player for UNIX distributed under the
terms of the GNU GPL. On PC class machines with GUS or AWE cards
xmp takes advantage of the OSS sequencer to play modules with
virtually no system load. Using software mixing, xmp plays at sampling
rates up to 48 kHz in mono or stereo, 8 or 16 bits, signed or unsigned,
little or big endian samples with 32 bit linear interpolation.

%prep
%setup

%build
./configure --prefix=/usr
make

%install
make install

%clean
rm -rf $RPM_BUILD_ROOT

%post

%files
%doc README etc/magic doc/Changes doc/FAQ.html doc/FAQ.txt
/etc/xmprc
/usr/bin/xmp
/usr/bin/xxmp
/usr/man/man1/xmp.1
/usr/man/man1/xxmp.1

