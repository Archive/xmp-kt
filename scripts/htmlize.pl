
my $itemize = 0;
my $prevline = "";

sub prologue
{
    my $title = shift;
    my $body = shift;

    print <<EOF;
<html>
<head>
<title>$title</title>
</head>
<body $body>
EOF
}

sub epilogue
{
    print $prevline;
    $itemize && print "</ul>";

    print <<EOF;
<p>
</body>
</html>
EOF
}

sub htmlize
{
    if (/^\-\-+$/) {
	 $prevline =~ /^(.*>)?([^<>]+)(<.*)?$/;
	 $prevline = "$1<p>\n<hr><p>\n<h1>$2</h1>$3"; 
	 $_ = "";
    }

    print $prevline;

    if (!$itemize && /^\s*- /) {
	s/^\s*- /<ul>\n<li>/;
	$itemize = 1;
	$prevline = $_;
	next;
    }

    if ($itemize && /^\w/) {
	$itemize = 0;
	$prevline = "</ul>\n$_";
	next;
    }

    s/^\s*- /<li>/;
    $itemize || /^\s*$/ && ($_ = "<p>");
    $prevline = $_;
}

1;
