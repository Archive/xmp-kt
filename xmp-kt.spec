# Note that this is NOT a relocatable package
%define ver      0.4.4
%define rel      1
%define prefix   /usr/local

Summary: Extended Module Player 1.1.6 (for KT %{ver})
Name: xmp-kt
Version: %ver
Release: %rel
Packager: Widget <pdr@pdr.ml.org>
URL: http://pdr.ml.org/kt
Group: Applications/Sound
Source: ftp://pdr.ml.org/pub/kt/xmp-kt-%{ver}.tar.gz
Copyright: GPL

%description
xmp is an open source module player for UNIX distributed under the
terms of the GNU GPL. On PC class machines with GUS or AWE cards
xmp takes advantage of the OSS sequencer to play modules with
virtually no system load. Using software mixing, xmp plays at sampling
rates up to 48 kHz in mono or stereo, 8 or 16 bits, signed or unsigned,
little or big endian samples with 32 bit linear interpolation.
XMP is written and maintained by Claudio Matsuoka <claudio@pos.inf.ufpr.br>.
This version is a hacked around version by Widget and K for use with
KegTracker.  It only installs the xmp library and headers.  The original
distribution can be found at http://xmp.home.ml.org and contains the xmp
and xxmp programs.  This distribution is based on the 1.1.6 version of xmp.

%prep
%setup

%build
CFLAGS="${RPM_OPT_FLAGS}"
./configure --prefix=%prefix
make

%install
rm -rf $RPM_BUILD_ROOT

make prefix=$RPM_BUILD_ROOT%{prefix} install

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-, root, root)
%doc README etc/magic doc/Changes doc/FAQ.html doc/FAQ.txt
/etc/xmprc
%{prefix}/lib/libxmp-kt.a
%{prefix}/include/xmp-kt/xmp.h
%{prefix}/include/xmp-kt/xmpi.h
%{prefix}/include/xmp-kt/xxm.h

%changelog

* Sun Nov 15 1998 Widget <pdr@pdr.ml.org>

- wrote .spec file
